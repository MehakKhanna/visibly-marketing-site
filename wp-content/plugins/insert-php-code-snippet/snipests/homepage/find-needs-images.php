<?php
$regular_size = get_post_meta(get_the_ID(), 'wpcf-dn-image-regular-size', true);
$retina_size = get_post_meta(get_the_ID(), 'wpcf-dn-image-retina-size', true);
$mobile_size = get_post_meta(get_the_ID(), 'wpcf-dn-image-mobile-size', true);
$alt_text = get_post_meta(get_the_ID(), 'wpcf-dn-alt-text', true);
include($rootPath.'/snipests/picture.php');
 ?>