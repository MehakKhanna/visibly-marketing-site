<?php
$regular_size = get_post_meta(get_the_ID(), 'wpcf-crv-image-for-regular-size', true);
$retina_size = get_post_meta(get_the_ID(), 'wpcf-crv-image-for-ratina-size', true);
$mobile_size = get_post_meta(get_the_ID(), 'wpcf-crv-image-for-mobile', true);
$alt_text = get_post_meta(get_the_ID(), 'wpcf-crv-alt-text', true);
include($rootPath.'/snipests/picture.php');
 ?>