<?php
$regular_size = get_post_meta(get_the_ID(), 'wpcf-lo-image-for-regular-size', true);
$retina_size = get_post_meta(get_the_ID(), 'wpcf-lo-image-for-retina-size', true);
$mobile_size = get_post_meta(get_the_ID(), 'wpcf-lo-image-for-mobile-size', true);
$alt_text = get_post_meta(get_the_ID(), 'wpcf-lo-alt-text', true);
include($rootPath.'/snipests/picture.php');
 ?>