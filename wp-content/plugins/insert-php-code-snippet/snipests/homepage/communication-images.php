<?php
$regular_size = get_post_meta(get_the_ID(), 'wpcf-ci-image-regular-size', true);
$retina_size = get_post_meta(get_the_ID(), 'wpcf-ci-image-retina-size', true);
$mobile_size = get_post_meta(get_the_ID(), 'wpcf-ci-image-mobile-size', true);
$alt_text = get_post_meta(get_the_ID(), 'wpcf-ci-alt-text', true);
include($rootPath.'/snipests/picture.php');
 ?>
