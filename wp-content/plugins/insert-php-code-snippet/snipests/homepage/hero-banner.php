<?php
$regular_size = get_post_meta(get_the_ID(), 'wpcf-hero-image', true);
$retina_size = get_post_meta(get_the_ID(), 'wpcf-hero-image-ratina-size', true);
$mobile_size = get_post_meta(get_the_ID(), 'wpcf-hero-image-mobile-size', true);
$alt_text = get_post_meta(get_the_ID(), 'wpcf-hero-image-alt-text', true);
include($rootPath.'/snipests/picture.php');
 ?>

