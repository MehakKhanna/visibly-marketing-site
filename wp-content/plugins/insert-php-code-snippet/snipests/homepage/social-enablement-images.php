<?php
$regular_size = get_post_meta(get_the_ID(), 'wpcf-se-image-regular-size', true);
$retina_size = get_post_meta(get_the_ID(), 'wpcf-se-image-retina-size', true);
$mobile_size = get_post_meta(get_the_ID(), 'wpcf-se-image-mobile-size', true);
$alt_text = get_post_meta(get_the_ID(), 'wpcf-se-alt-text', true);
include($rootPath.'/snipests/picture.php');
 ?>