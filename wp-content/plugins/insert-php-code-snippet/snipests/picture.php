<?php 
if($mobile_size==''){
	$mobile_size = $regular_size;
}
if($retina_size==''){
	$retina_size = $regular_size;
}

 ?>

<picture>
    <source srcset="<?php echo $mobile_size; ?>" media="(max-width: 767px)" />
    <source srcset="<?php echo $retina_size; ?>" media="(-webkit-min-device-pixel-ratio: 1.5)" />
    <source srcset="<?php echo $regular_size; ?>" media="(min-width: 768px)" />
    <?php if($alt_text=='') {
        $alt_text = get_the_title();
    } ?>
    <img src="<?php echo $regular_size; ?>" alt="<?php echo $alt_text; ?>" title="<?php echo $alt_text; ?>" />
</picture>