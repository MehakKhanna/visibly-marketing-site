<?php
$regular_size = get_post_meta(get_the_ID(), 'wpcf-features-icons-gamification-for-regular-size', true);
$retina_size = get_post_meta(get_the_ID(), 'wpcf-features-icons-gamification-for-retina-size', true);
$mobile_size = get_post_meta(get_the_ID(), 'wpcf-features-icons-gamification-for-regular-size', true);
$alt_text = get_post_meta(get_the_ID(), 'wpcf-features-icons-gamification-alt-text', true);
include($rootPath.'/snipests/picture.php');
 ?>
