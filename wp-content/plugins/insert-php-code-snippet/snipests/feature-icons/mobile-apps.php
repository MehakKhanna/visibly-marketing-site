<?php
$regular_size = get_post_meta(get_the_ID(), 'wpcf-features-icons-mobile-apps-for-regular-size', true);
$retina_size = get_post_meta(get_the_ID(), 'wpcf-features-icons-mobile-apps-for-retina-size', true);
$mobile_size = get_post_meta(get_the_ID(), 'wpcf-features-icons-mobile-apps-for-regular-size', true);
$alt_text = get_post_meta(get_the_ID(), 'wpcf-features-icons-mobile-apps-alt-text', true);
include($rootPath.'/snipests/picture.php');
 ?>
