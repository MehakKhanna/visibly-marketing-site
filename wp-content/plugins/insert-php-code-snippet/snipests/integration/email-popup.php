<div id="integration-contact-form" style="display:none;">
	<h2 class="form-main-title">Request information</h2>
	<div class="form-sub-title"><?php echo get_post_meta(get_the_ID(), 'wpcf-title-for-integration-popup', true); ?></div>
	<div class="form-wrapper">
		<?php echo do_shortcode('[contact-form-7 id="2873" title="Integration popup"]') ?>
	</div>
</div>
<div id="integration-popup-description" style="display:none;"><?php echo get_post_meta(get_the_ID(), 'wpcf-description-for-integration-popup', true); ?></div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#integration-contact-form .wpcf7-textarea').text($('#integration-popup-description').html());
		document.addEventListener( 'wpcf7mailsent', function( event ) {
			setTimeout(() => {
				$.fancybox.close();       
			}, 2000);
			
		}, false );
	});
</script>