<picture>
    <source srcset="<?php echo $mobile_size; ?>" media="(max-width: 990px)" />
    <source srcset="<?php echo $retina_size; ?>" media="(-webkit-min-device-pixel-ratio: 1.5)" />
    <source srcset="<?php echo $regular_size; ?>" media="(min-width: 991px)" />
    <?php if($alt_text=='') {
        $alt_text = get_the_title();
    } ?>
    <img src="<?php echo $regular_size; ?>" alt="<?php echo $alt_text; ?>" title="<?php echo $alt_text; ?>" />
</picture>