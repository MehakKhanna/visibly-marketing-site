<?php
/**
 * Template Name: about us
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();

while ( have_posts() ) : the_post();?>
<h1 id="h1_title"><?php the_title(); ?></h1>
<div class="about-top-content">
<?php the_content(); ?>
</div>
<div class="about-csr-section">
            <div class="about-csr-banner">
                <img src="<?php echo get_template_directory_uri() ?>/images/about-csr-image.jpg" alt="about-csr-image">
                <div class="container">
                    <div class="csr-quote-block">
                        <div class="csr-quote-text">Anything I can do to help these children feel more positive about the world around them has got to be worth it.</div>
                        <div class="csr-quote-meta clearfix">
                            <div class="csr-quote-author">Asma Pathek</div>
                            <div class="csr-read-more-link">
                                <a class="link_with_arrow" href="<?php echo get_home_url() ?>/blog-my-charity-day/" title="Read now">Read now 
                                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                  viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                                  <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
                                  <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
                              </svg></a>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
            <div class="container">
                <div class="about-csr-col-wrapper clearfix">
                    <div class="about-csr-col">
                        <div class="about-csr-col-inner">
                            <h2>Diversity</h2>
                            <div class="about-csr-content">
                                <p>Every year we have a ‘Women in Technology’ open day to showcase careers in Visibly and the wider technology market. The last Women in technology open day attracted nearly 120 female participants. Also, Visibly’s HR team has a target of ensuring that 50% of all employees are female. </p>
                            </div>
                        </div>
                    </div>
                    <div class="about-csr-col">
                        <div class="about-csr-col-inner">
                            <h2>CSR</h2>
                            <div class="about-csr-content">
                                <p>We have a mature CSR program that includes community work, tree planting and constructive engagement around operational activities. We  encourage our people to participate in discussions and decisions around social, environmental and labour policies.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
<?php endwhile;  
get_footer(); ?>