<?php
/**
 * Template Name: Testimonial page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */


?>


<?php $args = array(
	'posts_per_page'   => 1,
	'orderby'          => 'order',
	'post_type'        => 'testimonials',
	'post_status'      => 'publish',
	'order' => 'ASC',
	);
$posts_array = get_posts( $args );

if( sizeof($posts_array) != 0 )
{    
	foreach ($posts_array as $testimonial) {
		$image = get_the_post_thumbnail($testimonial,'thumbnail');
		$name = get_post_meta($testimonial->ID, 'wpcf-testimonial-name', true).',';
		$desigation = get_post_meta($testimonial->ID, 'wpcf-testimonial-designation', true);
		$description = '"'.$testimonial->post_title.'"';
		if($pricingPage){
			?>
			<div class = "testimonialMainWrapper clearfix">
				<div class = "avatar-image">  
					<div class = "avtart-img">
						<?php  if ( $image ) {
								echo $image;
						}  ?>
					</div>
					<div  class="avtart-sign"><?php the_field('name') ?></div>
					<div class="avtart-sign"><?php the_field('designation') ?></div>
				</div>
				<div class = "testimonialText"><?php echo $description; ?></div>
			</div>
			
			<?php }
			else{ ?>
				 <div class = "testimonialMainWrapper">

                       <div class = "testimonialText"><?php echo $description; ?></div>
                       <div class = "avtart-sign"><?php echo $name;  ?> <?php the_field('designation') ?></div>
                        <div class = "avatar-image">  
                             <div class = "avtart-img"> 
                               <?php  if ( $image ) {
								echo $image;
						}  ?>
                             </div>
                       </div>
                 </div>
			<?php } }
		}  ?>