<div class="features_list">
							<ul class="flex_view feature_ul">

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
												<path class="culture-st0" d="M7,10c-2.2,0-4,1.8-4,4s1.8,4,4,4s4-1.8,4-4S9.2,10,7,10z M15,15c-1.6,0-3,1.4-3,3s1.4,3,3,3s3-1.4,3-3
												S16.7,15,15,15z M16.5,3c-3,0-5.5,2.5-5.5,5.5s2.5,5.5,5.5,5.5S22,11.5,22,8.5S19.5,3,16.5,3z"/>
												<path class="culture-st1" d="M0,0h24v24H0V0z"/>
												<path class="culture-st2" d="M7,10c-2.2,0-4,1.8-4,4s1.8,4,4,4s4-1.8,4-4S9.2,10,7,10z M7,16c-1.1,0-2-0.9-2-2s0.9-2,2-2s2,0.9,2,2
												S8.1,16,7,16z M15,15c-1.6,0-3,1.4-3,3s1.4,3,3,3s3-1.4,3-3S16.7,15,15,15z M15,19c-0.6,0-1-0.5-1-1s0.4-1,1-1s1,0.5,1,1
												S15.6,19,15,19z M16.5,3c-3,0-5.5,2.5-5.5,5.5s2.5,5.5,5.5,5.5S22,11.5,22,8.5S19.5,3,16.5,3z M16.5,12c-1.9,0-3.5-1.6-3.5-3.5
												S14.6,5,16.5,5S20,6.6,20,8.5S18.4,12,16.5,12z"/>
											</svg> 
											</span>
											Culture & ENGAGEMENT
										</h3>
										<p class="feature_descr">Pulse employee engagement surveys.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/culture-engagement" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/features_advocacy.svg');?> 
											</span>
											Experience & NPS
										</h3>
										<p class="feature_descr">Measure customer and job seeker experience.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/experience-nps" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
												<path class="social-analytics-st0" d="M19.2,3h-14c-1.1,0-2,0.9-2,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21.2,3.9,20.3,3,19.2,3z"/>
												<path class="social-analytics-st1" d="M0,0h24v24H0V0z"/>
												<path class="social-analytics-st2" d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M19,19H5V5h14V19z M7,10
												h2v7H7V10z M11,7h2v10h-2V7z M15,13h2v4h-2V13z"/>
											</svg>

											</span>
											Social analytics
										</h3>
										<p class="feature_descr">Social media analytics for the enterprise </p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/social-analytics" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<!-- <li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/Reputation-Managment-Final.svg');?> 
											</span>
											Brand reputation
										</h3>
										<p class="feature_descr">Monitor your brand on social and review sites.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/brand-reputation" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li> -->

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/ic-values.svg');?> 

											</span>
											AI values
										</h3>
										<p class="feature_descr">Measure your internal value alignment.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/ai-values" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>


								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31 32" style="enable-background:new 0 0 31 32;" xml:space="preserve"> <path class="analytic-st0" d="M-2-2h34v34H-2V-2z"></path> <circle class="analytic-st1" cx="16" cy="16" r="14.2"></circle> <path class="analytic-st2" d="M27.2,14.6h-9.8V4.8C22.5,5.4,26.6,9.5,27.2,14.6z"></path> <path class="analytic-st2" d="M4.7,16c0-5.8,4.3-10.5,9.9-11.2v22.5C9,26.5,4.7,21.8,4.7,16z"></path> <path class="analytic-st2" d="M17.4,27.2v-9.8h9.8C26.6,22.5,22.5,26.6,17.4,27.2z"></path> </svg> 
											</span>
											IC analytics
										</h3>
										<p class="feature_descr">Measure IC and content resonance.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/internal-communication-analytics" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								
								

							</ul>
						</div>