<!-- Survey Section Starts -->
<?php wp_reset_query(); ?>
  <div class="take-servey-section">
    <div class="container cf">
      <div class="take-servey-left">
        <h2 class="take-servey-content">Take our survey, find out if you could benefit from an employee-led culture transformation. </h2>
        <p class="take-servey">
          <span class="servey-link">
            <a class="link_with_arrow" href="https://visibly.io/survey/9" title="Let's go">Let's go
              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
              <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
              <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
            </svg>
          </a>
        </span>
      </p>
      </div>
          

        <?php
$general_page_id = get_the_ID();
  
      if(get_field('footer_book_section_field_title')!=''){
$general_page_id = get_the_ID();
  
      }else{
        
      $general_page = get_page_by_path( 'general-blocks' );
      $general_page_id = $general_page->ID;
      }
      $post_type = get_post_type($general_page_id);
      $args = array(
            'post_type'        => $post_type,
            'post_status'      => 'publish',
            'order' => 'DESC',
            'posts_per_page' => 1,
            'include'          => array($general_page_id),
        );

                 
        $podcast_posts = get_posts( $args );
        
           
        foreach ($podcast_posts as $post) {
         ?>
<div class="take-servey-right" style="background-image:url(<?php the_field('footer_book_section_field_image') ?>)">
            <div class="take-servey-right-inner">

                <h2><?php the_field('footer_book_section_field_title') ?></h2>
                <p class="take-servey-wrapper">
                  <a class="link_with_arrow" href="<?php the_field('footer_book_section_field_book') ?>" title="Download our eBook">Download our eBook
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                    <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
                    <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
                </svg>
            </a>
        </p>
        
    </div>
    </div>
    <?php } ?>
    </div>
  </div>
  <!-- Survey Section Starts -->