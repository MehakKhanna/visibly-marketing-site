<div class="features_list">
							<ul class="flex_view feature_ul">

								<!-- <li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">

												<path class="customer-adovocacy-st0" d="M21.4,11.6l-9-9C12.1,2.2,11.6,2,11,2H4C2.9,2,2,2.9,2,4v7c0,0.6,0.2,1.1,0.6,1.4l9,9c0.4,0.4,0.9,0.6,1.4,0.6
												s1.1-0.2,1.4-0.6l7-7c0.4-0.4,0.6-0.9,0.6-1.4S21.8,11.9,21.4,11.6z"/>
												<path class="customer-adovocacy-st1" d="M0,0h24v24H0V0z"/>
												<path class="customer-adovocacy-st2" d="M21.4,11.6l-9-9C12.1,2.2,11.6,2,11,2H4C2.9,2,2,2.9,2,4v7c0,0.6,0.2,1.1,0.6,1.4l9,9c0.4,0.4,0.9,0.6,1.4,0.6
												s1.1-0.2,1.4-0.6l7-7c0.4-0.4,0.6-0.9,0.6-1.4S21.8,11.9,21.4,11.6z M13,20l-9-9V4h7v0l9,9L13,20z"/>
												<circle class="customer-adovocacy-st2" cx="6.5" cy="6.5" r="1.5"/>
												<path class="customer-adovocacy-st2" d="M8.9,12.6c0,0.6,0.2,1.1,0.6,1.4l3.5,3.5l3.5-3.5c0.4-0.4,0.6-0.9,0.6-1.4c0-1.1-0.9-2.1-2.1-2.1
												c-0.6,0-1.1,0.2-1.4,0.6L13,11.7l-0.6-0.6c-0.4-0.4-0.9-0.6-1.4-0.6C9.8,10.5,8.9,11.4,8.9,12.6z"/>
											</svg>

											</span>
											Customer advocacy
										</h3>
										<p class="feature_descr">Leverage customer brand advocates.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/customer-advocacy" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li> -->

								<!-- <li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
												<path class="employee-advocacy-st0" d="M22,12c0,5.5-4.5,10-10,10S2,17.5,2,12S6.5,2,12,2S22,6.5,22,12z"/>
												<path class="employee-advocacy-st1" d="M0,0h24v24H0V0z"/>
												<path class="employee-advocacy-st2" d="M10.2,13c0,0.7-0.6,1.2-1.2,1.2S7.8,13.7,7.8,13s0.6-1.2,1.2-1.2S10.2,12.3,10.2,13z M15,11.8
												c-0.7,0-1.2,0.6-1.2,1.2s0.6,1.2,1.2,1.2s1.2-0.6,1.2-1.2S15.7,11.8,15,11.8z M22,12c0,5.5-4.5,10-10,10S2,17.5,2,12S6.5,2,12,2
												S22,6.5,22,12z M10.7,4.1C12.1,6.4,14.6,8,17.5,8c0.5,0,0.9-0.1,1.3-0.1C17.4,5.6,14.9,4,12,4C11.5,4,11.1,4.1,10.7,4.1z M4.4,9.5
												c1.7-1,3-2.6,3.7-4.4C6.4,6,5.1,7.6,4.4,9.5z M20,12c0-0.8-0.1-1.5-0.3-2.2C19,9.9,18.2,10,17.5,10c-3.1,0-5.9-1.4-7.8-3.7
												C8.7,8.9,6.6,10.9,4,11.9c0,0,0,0.1,0,0.1c0,4.4,3.6,8,8,8S20,16.4,20,12z"/>
											</svg> 
											</span>
											Employee advocacy
										</h3>
										<p class="feature_descr">Activate employee brand advocates. </p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/employee-advocacy" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li> -->

								

								<?php /*
								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/Employee-Referrals.svg');?>
											</span>
											Employee referrals
										</h3>
										<p class="feature_descr">Drive more employee referrals.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/employee-referrals" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>



								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/widgets.svg');?> 
											</span>
											Social Widgets
										</h3>
										<p class="feature_descr">Embed social widgets into web assets.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/social-widgets" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>
							*/ ?>

							</ul>
						</div>

					
