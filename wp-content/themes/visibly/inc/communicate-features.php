<div class="features_list">
	<ul class="flex_view feature_ul">
<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<?php include($rootPath.'/images/features_feeds.svg');?> 
					</span>
					FEEDS
				</h3>
				<p class="feature_descr">Tailored feeds for all communication needs.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/feeds" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>
		<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<?php include($rootPath.'/images/features_campaign.svg');?> 
					</span>
					CAMPAIGN MANAGEMENT
				</h3>
				<p class="feature_descr">Enterprise project management workflow.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/campaign-management" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>

		<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<?php include($rootPath.'/images/features_printmedia.svg');?> 
					</span>
					PRINT
				</h3>
				<p class="feature_descr">Design, print and ship employee magazines.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/print" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>




		<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<?php include($rootPath.'/images/features_mobile.svg');?> 
					</span>
					SMS & Push
				</h3>
				<p class="feature_descr">Create and send bespoke SMS notifications.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/sms-push" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>

		<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<?php include($rootPath.'/images/icons/email.svg');?>  
					</span>
					Email
				</h3>
				<p class="feature_descr">Create and send beautiful HTML emails.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/email" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>

<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<?php include($rootPath.'/images/icons/chat.svg');?>
					</span>
					Chat
				</h3>
				<p class="feature_descr">Utilise both 1-2-1 and group instant chat.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/chat" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>

		<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/features_media.svg');?> 
											</span>
											Media editors
										</h3>
										<p class="feature_descr">Edit and optimise photos and videos.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/media-editors" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/features_feeds.svg');?> 
											</span>
											Asset Management
										</h3>
										<p class="feature_descr">End to end media asset management.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/asset-management" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/moderation.svg');?>  
											</span>
											Moderation
										</h3>
										<p class="feature_descr">Brand governance and compliance.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/moderation" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>


	</ul>
</div>