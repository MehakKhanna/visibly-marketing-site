<div class="features_list">
							<ul class="flex_view feature_ul">

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/construction.svg');?> 
											</span>
											CONSTRUCTION
										</h3>
										<p class="feature_descr">Integrations with over 1000 enterprise systems.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/why-visibly/construction" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/technology.svg');?> 
											</span>
											TECHNOLOGY
										</h3>
										<p class="feature_descr">Tailored feeds to satisfy every communication scenario.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/why-visibly/technology" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/retail.svg');?> 
											</span>
											RETAIL
										</h3>
										<p class="feature_descr">Scientifically researched advocacy diagnostics model.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/why-visibly/retail" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/manufacturing.svg');?> 
											</span>
											Manufacturing
										</h3>
										<p class="feature_descr">Remote locations or non-technical workers</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/why-visibly/manufacturing" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/healthcare.svg');?> 
											</span>
											Healthcare
										</h3>
										<p class="feature_descr">Engaged workers means satisfied patients</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/why-visibly/healthcare" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/hospitality.svg');?> 
											</span>
											Hospitality
										</h3>
										<p class="feature_descr">De-centralised and non-desk workers</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/why-visibly/hospitality" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/science.svg'); ?>
											</span>
											SCIENCE
										</h3>
										<p class="feature_descr">Remote locations or non-technical workers</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/why-visibly/science/" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>


							
								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/financial.svg');?> 
											</span>
											FINANCIAL
										</h3>
										<p class="feature_descr">Brand governance and fails safe systems for brand compliance.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/why-visibly/financial" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>


								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/events.svg');?> 
											</span>
											EVENTS
										</h3>
										<p class="feature_descr">Design, print and ship employee magazines.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/why-visibly/events" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								
								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/logistics.svg');?> 
											</span>
											Logistics
										</h3>
										<p class="feature_descr">Remote locations or non-technical workers</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/why-visibly/logistics/" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/education.svg');?> 
											</span>
											EDUCATION
										</h3>
										<p class="feature_descr">Analytics for adoption, social media and more.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/why-visibly/education" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include($rootPath.'/images/icons/non-profit.svg');?> 
											</span>
											NON PROFIT
										</h3>
										<p class="feature_descr">Value polls and pulse surveys to measure engagement.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/why-visibly/non-profit" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

							</ul>
						</div>