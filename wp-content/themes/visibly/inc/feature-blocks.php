<!-- Category Main Section Starts -->
<section class="all_categories">
  <div id="category_main_section">
    <div class="container">
      <div class="inner_wrap">

        <div id="all_category">
          <div class="all_cat_wrap">

            <div class="title">
              <div class="title_txt"><?php dynamic_sidebar('feature-block-title') ?></div>
            </div>


            <div class="category_div flex_view_xs space-between center">

              <?php
                                    $args1 = array(
                                        'post_type' => 'features_blocks',
                                        'post_status' => 'publish',
                                        'orderby' => 'menu_order',
                                        'order' => 'ASC',
                                        'posts_per_page' => -1,
                                    );
                                    $posts_array = get_posts( $args1 );
                                        if( sizeof($posts_array) != 0 ){  
                                            foreach ($posts_array as $post) { 
                                              
                                              
                                        ?>

                                        <div class="category <?php the_field('feature_block_class') ?>">

                <div class="cat_wrap">
                  <div class="cat_header flex_view_xs middle">
                    <div class="icon_div">
                      <img class="feature-normal-icon" src="<?php the_field('feature_block_icon'); ?>">
                      <img class="feature-active-icon" src="<?php the_field('feature_block_active_icon'); ?>">
                      
                    </div>
                    <div class="title_div">
                      <p class="cat_title"><?php the_title();?></p>
                    </div>
                  </div>

                  <div class="cat_body">
                    <div class="cat_descr"><p><?php echo $post->post_content;?></p></div>
                    <div class="cat_link"><a class="link_with_arrow" href="<?php the_field('feature_block_link'); ?>" title="Learn more">Learn more 
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
                <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
              </svg></a></div>
                  </div>
                  
                </div>

              </div>

                                  <?php
                                        }
                                    }
                                    wp_reset_query();
                                     ?>

              
            </div>

          </div>
        </div>
        

      </div>
    </div>
  </div>
</section>
  <!-- Category Main Section Ends -->