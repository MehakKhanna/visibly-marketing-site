<div class="features_list">
	<ul class="flex_view feature_ul">

		<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<?php include($rootPath.'/images/features_media.svg');?> 
					</span>
					Media editors
				</h3>
				<p class="feature_descr">Edit and optimise photos and videos.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/advocacy/media-editors" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>

		<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<?php include($rootPath.'/images/icons/moderation.svg');?> 
					</span>
					Moderation
				</h3>
				<p class="feature_descr">Brand governance and compliance.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/advocacy/moderation" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>

		<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<?php include($rootPath.'/images/features_printmedia.svg');?> 
					</span>
					PRINT
				</h3>
				<p class="feature_descr">Design, print and ship employee magazines.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/print" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>

		<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<?php include($rootPath.'/images/features_gamification.svg');?> 
					</span>
					GAMIFICATION
				</h3>
				<p class="feature_descr">Create competition through points and leaderboards.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/engage/gamification" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>

		<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31 32" style="enable-background:new 0 0 31 32;" xml:space="preserve"> <path class="analytic-st0" d="M-2-2h34v34H-2V-2z"></path> <circle class="analytic-st1" cx="16" cy="16" r="14.2"></circle> <path class="analytic-st2" d="M27.2,14.6h-9.8V4.8C22.5,5.4,26.6,9.5,27.2,14.6z"></path> <path class="analytic-st2" d="M4.7,16c0-5.8,4.3-10.5,9.9-11.2v22.5C9,26.5,4.7,21.8,4.7,16z"></path> <path class="analytic-st2" d="M17.4,27.2v-9.8h9.8C26.6,22.5,22.5,26.6,17.4,27.2z"></path> </svg> 
					</span>
					IC analytics
				</h3>
				<p class="feature_descr">Measure IC and content resonance.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/monitor/internal-communication-analytics" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>

		<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<?php 
						include($rootPath.'/images/features_pollsnpulse.svg');?> 
					</span>
					POLLS AND Recognition
				</h3>
				<p class="feature_descr">Send polls and recognition messages.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/engage/polls-and-recognition" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>
	</ul>
</div>