<div class="features_list">
	<ul class="flex_view feature_ul">

		<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<?php include($rootPath.'/images/features_gamification.svg');?> 
					</span>
					GAMIFICATION
				</h3>
				<p class="feature_descr">Create competition through points and leaderboards.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/gamification" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>


		<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<?php include($rootPath.'/images/features_pollsnpulse.svg');?> 
					</span>
					POLLS AND Recognition
				</h3>
				<p class="feature_descr">Send polls and recognition messages.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/polls-recognition" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>

		<li class="feature_li">
			<div class="feature">
				<h3 class="feature_title">
					<span class="icon">
						<?php include($rootPath.'/images/icons/curation.svg');?>  
					</span>
					Curation
				</h3>
				<p class="feature_descr">Personal and global content curation.</p>
				<div class="learn_more">
					<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/curation" title="Learn more">Learn more 
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
						<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
						<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
					</svg></a>
				</div>
			</div>
		</li>
	</ul>
</div>