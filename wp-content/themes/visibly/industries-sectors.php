<?php
/**
 * Template Name: industries and sectors
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>

<div id="industries_sectors">
	<div id="new_grid">

		<!-- Banner Section Starts -->
		<section class="banner_section">
			<div class="container">

				<div class="inner_wrap">
					<div class="parent_div flex_view_xs middle center">

						<div class="image_div">
							<div class="image img">
								<picture>
									<source srcset="<?php echo get_template_directory_uri(); ?>/images/industries_banner.png" media="(max-width: 767px)">

									<source srcset="<?php echo get_template_directory_uri(); ?>/images/industries_banner.png" media="(-webkit-min-device-pixel-ratio: 1.5)">

									<source srcset="<?php echo get_template_directory_uri(); ?>/images/industries_banner.png" media="(min-width: 768px)">

									<img src="<?php echo get_template_directory_uri(); ?>/images/industries_banner.png" alt="Banner image">

								</picture>
							</div>
						</div>

						<div class="text_div">
							<div class="text_wrap">
								<ul class="top_ul">
									<li class="top_li">
										<div class="descr">
											<h3 class="header">EMPLOYEES
												<span class="icon">
													<?php include_once('images/employee_svg.svg');?> 
												</span>
											</h3>

											<p class="descr">One centralised platform for all of your online and offline employee communications.</p>
										</div>

									</li>


									<li class="top_li">
										<div class="descr">
											<h3 class="header">CUSTOMERS
												<span class="icon">
													<?php include_once('images/customer_svg.svg');?> 
												</span>
											</h3>

											<p class="descr">Leverage NPS and employee advocacy to improve the customer experience.</p>
										</div>

									</li>


									<li class="top_li">
										<div class="descr">
											<h3 class="header">JOB SEEKERS
												<span class="icon">
													<?php include_once('images/jobseeker_svg.svg');?> 
												</span>
											</h3>

											<p class="descr">Promote a living, breathing and activated employer brand. Use NPS to improve job seeker experience.</p>
										</div>

									</li>


									<li class="top_li">
										<div class="descr">
											<h3 class="header">GUESTS
												<span class="icon">
													<?php include_once('images/guests_svg.svg');?> 
												</span>
											</h3>

											<p class="descr">Manage reseller, partner, vendor, supply chain, shareholder, candidate communications from one platform.</p>
										</div>

									</li>
								</ul>
							</div>
						</div>
						
					</div>
				</div>
				
			</div>
		</section>
		<!-- Banner Section Ends -->

		<!-- Overview Section Starts -->
		<section class="overview_section">
			<div class="inner_wrap">
				<div class="container">

					<div class="section_title text-center">
						<h3 class="title"><span class="square">Industries and sectors<span class="square_two"></span></span></h3>
					</div>
					
					<div class="container">
						
						<?php include($rootPath.'/inc/why-visibly-features.php');?> 

					</div>
				</div>
			</div>
		</section>
		<!-- Overview Section Ends -->


		<!-- Bottom Section Starts -->
		<section class="bottom_section">
			<div class="container">
				<div class="inner_wrap">
					<h3 class="healine text-center">Get everyone on the same page</h3>
					<p class="subtxt text-center">Break down department siloes and get everyone pulling in the same direction</p>

					<div class="category_div flex_view_xs middle space-between">
						<a href="<?php echo get_home_url(); ?>/department/hr" class="cat_div cat_hr flex_view_xs middle center">
							<p class="cat_txt">HR</p>
						</a>

						<a href="<?php echo get_home_url(); ?>/department/communications" class="cat_div cat_communications flex_view_xs middle center">
							<p class="cat_txt">Communications</p>
						</a>

						<a href="<?php echo get_home_url(); ?>/department/marketing" class="cat_div cat_marketing flex_view_xs middle center">
							<p class="cat_txt">Marketing</p>
						</a>
					</div>
				</div>
			</div>
		</section>
		<!-- Bottom Section Ends -->


	</div>
</div>

<?php
get_footer(); ?>