<div class="emailform-wrapper">
	<div class="error-wraning-wrapper"><span class="icon"></span><span class="email-popup-text"></span></div>
	<div class="email-suceess-message"><div class="icon-wrapper"><i class="fas fa-check"></i></div>Thank you for your registration.<br>Please check your email for next step instructions.</div>
	<form class="emailform cf">
		<div class="signupInputWrapper">
			<input class="email" type="email" placeholder="Email Address">
			<input type="hidden" value="<?php echo $_SESSION['plan_data']['data'][0]['identity'] ?>" name="planID" id="planID">
		</div>
		<div class="signupSubmit">
		<div class="email-submit-div">Launch Free Account</div>
		<input type="submit" value="Get started" class="email-submit">
	
			<div class="signup-loader-wrapper"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/visibly-loader.gif" alt="visibly-loader" class="signup-loader" ></div>
		</div>
	</form>
</div>