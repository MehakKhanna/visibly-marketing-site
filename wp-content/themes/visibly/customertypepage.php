<?php
/**
 * Template Name: Customer Type page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
while ( have_posts() ) : the_post(); 
the_content(); 
endwhile; 
include('inc/how-visibly-help.php'); 
get_footer(); ?>
