<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
?>
<div id="single_article_page">
    <div id="new_grid">
    
        <section class="title_section">
            <div class="container">
                <div class="top_txt text-center">
                    <h3 class="txt"><span class="line">BLOG</span></h3>
                     <?php
                        $term_list = wp_get_post_terms($post->ID, 'category', ['fields' => 'all']);
                        $primaryCategory='';
                        foreach($term_list as $term) {
                         if( get_post_meta($post->ID, '_yoast_wpseo_primary_category',true) == $term->term_id ) {
                            $primaryCategory = $term->name;
                            $primaryCategoryID = $term->term_id;
                            $currentPostId = $post->ID;
                        }
                    }

                    ?>
                    <p class="subtxt"><?php echo $primaryCategory; ?></p>
                    <h1 class="article_title"><?php the_title() ?></h1>
                    <?php if(get_post_type() == 'post') { ?>
                        <p class="article_subtxt"><?php the_field('post_subtitle') ?></p>
                    <?php } ?>
                </div>

            </div>
        </section>
<div class="single-article-section section">
        <section class="article_banner">
            <div class="container">
                
                <div class="nxt_article xs-hide">
                    <div class="next_article_url">

                        <?php 
                        the_post_navigation(
                            array(
                                'next_text' => '<span class="meta-nav link_with_arrow" aria-hidden="true">' . __( 'Next article <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                                  <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"></path>
                                  <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"></circle>
                                  </svg>', 'twentynineteen' ) . '</span> ' ,
                            )
                        );
                        ?>

                    
              </div>
                </div>

                

                <div class="article_header clearfix">
                    <a href="<?php echo get_home_url() ?>/blog" class="article_cat">All Articles</a>
                    
                   <?php 
                        the_post_navigation(
                            array(
                                'next_text' => '<span class="meta-nav link_with_arrow" aria-hidden="true">' . __( 'Next article <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                                  <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"></path>
                                  <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"></circle>
                                  </svg>', 'twentynineteen' ) . '</span> ' .
                                '<span class="screen-reader-text">' . __( 'Next post:', 'twentynineteen' ) . '</span> <br/>' .
                                '<span class="next_article_title">%title</span>',
                                
                            )
                        );
                        ?>
                </div>
                
                <?php  if ( has_post_thumbnail() ) {?>
                    <div class="article_banner_img">
                        <div class="img">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="wp-post-image" title="<?php the_title() ?>">
                        </div>
                    </div>
                    <?php 
                } ?>
                
            </div>
        </section>

        <section class="article_detail">
            <div class="container">
                <div class="article_parent flex_view_xs">
                    <div class="author_detail">
                        <div class="author_img img">
                            <?php
                            if (function_exists ( 'mt_profile_img'  ) ) {
                                $author_id=$post->post_author;
                                mt_profile_img( $author_id, array(
                                    'size' => 'full' ,
                                    'attr' => array( 'alt' => 'Alternative Text' ),
                                    'echo' => true )
                            );
                            }
                            ?>
                        </div>
                        <p class="author_name"><?php the_author() ?></p>
                        <p class="publish_date"><?php echo get_the_date(); ?></p>
                    </div>
                    <div class="article_txt"><?php the_content(); ?></div>

                </div>
                <div class="article_share">
                    <?php echo sharethis_inline_buttons(); ?>
                </div>
            </div>
        </section>
        <div class="footer-blog-nav">
            <section class="next_article text-right">
                <div class="container">
                     <?php 
                            the_post_navigation(
                                array(
                                    'next_text' => '<div class="next_blog_url"><span class="meta-nav link_with_arrow" aria-hidden="true">' . __( 'Next article <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                                      <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"></path>
                                      <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"></circle>
                                      </svg>', 'twentynineteen' ) . '</span></div>' .
                                    '<span class="screen-reader-text">' . __( 'Next post:', 'twentynineteen' ) . '</span> <br/>' .
                                    '<span class="next_blog_title">%title</span>',
                                    
                                )
                            );
                            wp_reset_query();
                            ?>
                
            </section>
        </div>
    </div>

        <?php include($rootPath.'/inc/take-survey.php'); ?>

        <?php
            $args1 = array(
                'post_type'        => 'post',
                'post_status'      => 'publish',
                'category' => $primaryCategoryID,
                'post__not_in' =>array($currentPostId),
                'orderby' => 'date',
                'order' => 'DESC',
                'posts_per_page' => 2,
                );


            $posts_array = get_posts( $args1 );

            if( sizeof($posts_array) != 0 )
            { ?>

        <!-- Related Articles Section Starts -->
        <section class="related_articles">
            <div class="section_title text-center">
                <h3 class="title"><span class="line">RELATED ARTICLES</span></h3>
            </div>

            <div class="article_list">
                <div class="inner_wrap">

                    <div class="article_row flex_view">

                    
               <?php foreach ($posts_array as $post) {
                     ?>

<?php
                        $related_post_term_list = wp_get_post_terms($post->ID, 'category', ['fields' => 'all']);
                        $related_post_primaryCategory='';
                        foreach($related_post_term_list as $term) {
                         if( get_post_meta($post->ID, '_yoast_wpseo_primary_category',true) == $term->term_id ) {
                            $related_post_primaryCategory = $term->name;
                        }
                    }

                    ?>
                

                        <div class="article select-all employer-branding">
                            <div class="article_url flex_view middle">
                                <span class="border sm-hide"></span>
                                <div class="article_image_wrap">
                                    <div class="image">
                                         <?php  if ( has_post_thumbnail() ) {?>
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="wp-post-image">
                                            <?php 
                                        } ?>
                                    </div>
                                </div>
                                <div class="article_text_wrap">



                                    <h3 class="article_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <div class="article_descr"><?php the_excerpt(); ?></div>
                                    <p class="article_category"><?php echo $related_post_primaryCategory; ?></p>
                                </div>

                            </div>
                        </div>
<?php  }  ?>


        
                    </div>


                </div>

            </div>
        </section>

        <!-- Related Articles Section Ends -->
<?php  }  ?>
    

    </div>
</div>


<?php get_footer();
?>
