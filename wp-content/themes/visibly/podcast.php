<?php
/**
 * Template Name: Podcast
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>

<div id="podcast_page">
	<div id="new_grid">
		<!-- Banner Section Starts -->
		<section class="banner_section">
			<div class="container">
				<div class="banner_wrap">
					
					<div class="blog_txt">
						<a href="<?php echo get_home_url(); ?>/blog" class="blog_title">BLOGS</a>
					</div>



					<div class="banner_txt">
						<h1 class="banner_title">The culture and <br class="sm-hide">comms podcast</h1>
						<p class="banner_descr">Listen to our interviews with culture <br class="sm-hide">and communications top experts.</p>
					</div>


					<ul class="icon_ul flex_view_xs middle space-between">
						<li class="icon_li img">
							<a target="_blank" href="https://podcasts.apple.com/gb/podcast/culture-and-communications/id1495789175" class="icon_url">
								<img src="<?php echo get_template_directory_uri(); ?>/images/apple_podcast.png" alt="Apple Podcast">
							</a>
						</li>

						<li class="icon_li img">
							<a target="_blank" href="https://open.spotify.com/show/5BDR0UDftOzMtvMY2EwEz4" class="icon_url">
								<img src="<?php echo get_template_directory_uri(); ?>/images/spotify_icon.png" alt="Spotify Icon">
							</a>
						</li>

						<li class="icon_li img">
							<a target="_blank" href="https://soundcloud.com/user-486275042" class="icon_url">
								<img src="<?php echo get_template_directory_uri(); ?>/images/soundcloud_icon.png" alt="Soundcloud Podcast">
							</a>
						</li>
					</ul>

					<div class="banner_image img">
						<img src="<?php echo get_template_directory_uri(); ?>/images/podast_banner_img.png" alt="banner_image">
					</div>
				</div>
			</div>
			<div class="overlay"></div>
		</section>
		<!-- Banner Section Ends -->

		<!-- Podcast Section Starts -->
		<section class="podcast_section">
			<div class="container">

				<div class="title text-center">
					<h3 class="section_title"><span class="line_txt">PODCASTS</span></h3>
				</div>

				<?php

$args = array(
            'post_type'        => 'podcast',
            'post_status'      => 'publish',
            'order' => 'DESC',
            'posts_per_page' => -1,
        );

                 
 $podcast_posts = get_posts( $args );
        
           
        foreach ($podcast_posts as $post) {
         ?>

				<div class="podcast">
					<div class="podcast_parent flex_view_xs">

						<div class="image_div">
							<div class="img">
								<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo $post->post_title; ?>">
							</div>
						</div>

						<div class="text_div">
							<h3 class="podcast_title"><?php echo $post->post_title; ?></h3>
							<h4 class="podcast_subtitle"><?php the_field('podcast_subtitle') ?></h4>
							<p class="podcast_descr">
								<?php echo $post->post_content; ?>
							</p>
							<a title="Listen Now" href="<?php the_field('podcast_url'); ?>" class="request-demo-btn">Listen Now</a>
						</div>

					</div>
				</div>

				<?php 
        }
        wp_reset_query();
         ?>
				
			</div>
		</section>
		<!-- Podcast Section Ends -->
		
	</div>
</div>


<?php
get_footer(); ?>