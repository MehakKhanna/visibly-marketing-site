<?php
/**
 * Template Name: Event
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();

?>
<h1 id="h1_title"><?php the_title(); ?></h1>

<!-- Main wrapper starts  -->
<div class='mainWrapper blogMainWrapper'>

    <!-- blogHeroSection starts -->


    <?php

    $args = array(
        'post_type'        => 'events',
        'post_status'      => 'publish',
        'order' => 'ASC',
        'posts_per_page' => 1,
        'post__in'  => get_option( 'sticky_posts' ),
        'ignore_sticky_posts' => 1
    );


    $sticky_posts_array = get_posts( $args );

    if( sizeof($sticky_posts_array) != 0 )
    {    
        foreach ($sticky_posts_array as $post) { ?>




            <!-- HERO SECTION STARTS -->
            <section class="hero_section">
                <div class="container">



                    <div class="hero">
                        <div class="flex_view middle">
                            <div class="blog_hero">
                                <div class="hero_banner">
                                    <div class="image">
                                        <?php  if ( has_post_thumbnail() ) {?>
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="wp-post-image" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
                                            <?php 
                                        } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="blog_txt">
                                <div class="text_wrap">
                                    <h3 class="blog_title"><?php echo $post->post_title; ?></h3>
                                    <p class="blog_excerpt"><?php the_excerpt(); ?></p>
                                    <div class="read_article_url"> <a class="link_with_arrow" href="<?php the_permalink(); ?>" title="Read article">Read article
                                      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                      viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                                      <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
                                      <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
                                  </svg></a></div>

                              </div>
                          </div>
                      </div>
                  </div>


              <?php }
          } ?>

      </div>
  </section>  
  <!-- HERO SECTION ENDS -->




  <!-- blogHeroSection ends -->



  <!--blogListSection starts-->
  <div class="blogListSection">
    <div id="new_grid">
        <div class="container">

            <!-- bloglistWrapper starts -->
            <div class="bloglistWrapper">
<!-- ARTICLE SECTION STARTS -->
                    <section class="articles_section">
                        <div class="container">
                            <div class="title_section text-center">
                                <h3 class="title_txt"><span class="line_txt">All events</span></h3>
                            </div>

                            <div class="cat_select_div text-center">
                                <div class="category_div custom_select flex_view">

                                    <div class="custom_select_trigger flex_view middle center">
                                        <span class="current_cat">Choose Option</span>
                                    </div>
                                    
                                    <div class="custom_option_box">
                                      <span class="custom_option" data-value="attending">Attending</span>
                                       <span class="custom_option" data-value="attended">Attended</span>
                                       <span class="custom_option" data-value="select-all">Select All</span>
                                   </div>
                               </div>  
                           </div>
                           
                       </div>
                        <div class="article_list">
                        <div class="inner_wrap">

                            <div class="article_row flex_view">
                <?php

                $args1 = array(
                    'post_type'        => 'events',
                    'post_status'      => 'publish',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'posts_per_page' => -1,
                );


                $posts_array = get_posts( $args1 );

                if( sizeof($posts_array) != 0 )
                {    
                    foreach ($posts_array as $post) { 
                        $categories = get_the_category();
                        $cls = '';

                        if ( ! empty( $categories ) ) {
                          foreach ( $categories as $cat ) {
                            $cls .= $cat->slug . ' ';
                        }
                    }

                    ?>


                                <div class="article select-all <?php echo $cls;?>">
                                     <?php
                                            $term_list = wp_get_post_terms($post->ID, 'category', ['fields' => 'all']);
                                            $primaryCategory='';
                                            foreach($term_list as $term) {
                                             if( get_post_meta($post->ID, '_yoast_wpseo_primary_category',true) == $term->term_id ) {
                                                $primaryCategory = $term->name;
                                            }
                                        }

                                        ?>
                                    <div  class="article_url flex_view middle">
                                        <span class="border sm-hide"></span>
                                        <div class="article_image_wrap">
                                            <div class="image">
                                               <?php  if ( has_post_thumbnail() ) {?>
                                                <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="wp-post-image">
                                                <?php 
                                            } ?>
                                        </div>
                                    </div>
                                    <div class="article_text_wrap">
                                        <h3 class="article_title"><a href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></h3>
                                        <div class="article_descr"><?php the_excerpt(); ?></div>
                                        <p class="article_category"><?php echo $primaryCategory; ?></p>
                                    </div>

                                </div>
                            </div>



                      


            <?php

        }
    } ?>
  </div>


                    </div>

                </div>
    </section>
            <!-- ARTICLE SECTION ENDS -->




    



</div>
<!-- bloglistWrapper ends -->
</div>
</div>
</div>
<!--blogListSection ends-->
</div>
<!-- Main wrapper ends-->







<?php get_footer(); ?>