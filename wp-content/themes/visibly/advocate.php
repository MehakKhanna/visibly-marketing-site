<?php
/**
 * Template Name: Advocate
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>

<div id="advocacy" class="main-feature-page">
	<div id="new_grid">
		<!-- Banner Section Starts -->
		<section class="banner_section">
			<div class="banner_wrap">

				<div class="banner_img img">
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/images/advocate_banner.png" media="(max-width: 767px)">

						<source srcset="<?php echo get_template_directory_uri(); ?>/images/advocate_banner.png" media="(-webkit-min-device-pixel-ratio: 1.5)">

						<source srcset="<?php echo get_template_directory_uri(); ?>/images/advocate_banner.png" media="(min-width: 768px)">

						<img src="<?php echo get_template_directory_uri(); ?>/images/advocate_banner.png" alt="Banner image">
					</picture>
				</div>

				<div class="banner_txt">
					<h1 class="banner_title"><?php the_field('hero_title') ?></h1>
					<div class="hero-sub-title"><?php the_field('hero_subtitle') ?></div>
					<a title="Request Demo" href="http://localhost/visibly/request-a-demo-by-mail/" class="request-demo-btn white-btn">Request Demo</a>
				</div>

				<!-- <div class="overlay"></div> -->

			</div>
		</section>
		<!-- Banner Section Ends -->

		<!-- Features overview section starts -->
		<section class="engage_features_overview">
			<div class="inner_wrap">
				<div class="container">

					<div class="section_title text-center">
						<h3 class="title"><span class="square"><span class="square_two"></span>Advocacy Features</span></h3>
					</div>
					
					<div class="container">
						<?php include($rootPath.'/inc/advocate-features.php'); ?>
					</div>
				</div>
			</div>
		</section>
		<!--  Features overview section ends -->


		<?php include($rootPath.'/inc/feature-blocks.php') ?>

	</div>
</div>


<?php
get_footer(); ?>