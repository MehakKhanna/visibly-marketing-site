<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
</div>
</div>

<!-- #content -->
<!-- Footer Starts-->
<div id="footer">
 <?php 
 if(!is_404() && !is_page_template('homepage.php') && !is_page_template('podcast.php') && !is_page_template('engage-features.php') && !is_page_template('monitor.php') && !is_page_template('advocate.php') && !is_page_template('communicate.php') && !is_page_template('pricingpage.php') ){
    global $post;
    if($post_slug=$post->post_name!='signup' && $post_slug=$post->post_name!='request-a-demo-by-mail' && $post_slug=$post->post_name!='request-trial'){
        ?>

        
        <div class="signup-form-wrapper">
            <div class="cf container">
                <div class="free-trial-block">
                    <?php include('blocks/footer-free-trial.php'); ?>
                </div>
            </div>
        </div>
    <?php } 
}
wp_reset_query();
?>
<?php if(!is_page_template('homepage.php') && !is_single() && !is_page_template('pricingpage.php')) { 
 include('inc/take-survey.php');
 } ?>

<div class='footer-quick-links-section'>
    <div class="container cf">
        <div class="footer-description-col">
            <div class="footer-column-inner">
                <?php $editLink = get_admin_url().'/widgets.php'; include('inc/edit-link.php'); ?>
                <div class="footer-logo-wrapper">
                    <div class="footer-logo">
                     <a class="footer-logo-link" href="<?php echo get_home_url() ?>"> <svg xmlns="http://www.w3.org/2000/svg" class="nav__image image" viewBox="0 0 374 95">
                        <defs>
                            <style>
                                .footer-logo-wrapper .cls-1 {
                                    fill: #fff;
                                }

                                .footer-logo-wrapper .cls-2 {
                                    fill: #ffbf00;
                                }

                                .footer-logo-wrapper .cls-3 {
                                    fill: #ff595e;
                                }
                            </style>
                        </defs>
                        <g data-name="Layer 2">
                            <g data-name="Layer 1">
                                <path class="cls-2" d="M17.21,22V49.66H40.52V72.71H67.85V22ZM57.73,62.58H50.64V39.52H27.33v-7.4h30.4Z" />
                                <path class="cls-3" d="M50.64,90.25V62.57H27.33V39.52H0V90.25ZM10.12,49.66h7.09V72.71H40.52v7.4H10.12Z" />
                            </g>
                        </g>
                    </svg></a>
                    <div class="become-partner-text">Become a <a href="<?php echo get_home_url();?>/contact/">partner</a></div>
                </div>
            </div>
            <!-- footer-logo-wrapper ends -->

            <div class="footer-description">
                <?php dynamic_sidebar('footer-text') ?>
            </div>
        </div>
    </div>
    <div class="footer-column-wrapper">
        <div class="footer-column-wrapper-inner clearfix">
            <?php $editLink = get_admin_url().'/nav-menus.php'; include('inc/edit-link.php'); ?>
            <?php dynamic_sidebar('footer-menus') ?>
        </div>
    </div>
</div>
</div>
<!--Footer quicklink section ends-->
<div id="footer-bottom-section">
    <div class="container">
        <div class="footerWrapper clearfix">
            <div class="footer-bottom-left-links">
                <?php $editLink = get_admin_url().'/nav-menus.php'; include('inc/edit-link.php'); ?>
                <?php wp_nav_menu( array(
                    'menu'           => 'footer-policy-menu',
                    'menu_class'     => 'cf'
                ) ); ?>
            </div>
            <ul class="footer-social-links-wrapper">
                <?php $editLink = get_admin_url().'/widgets.php'; include('inc/edit-link.php'); ?>
                <?php dynamic_sidebar('footer-social-links') ?>
            </ul>
            <div class="copy-right-sec">
                <p>Copyright @ 2020 All Rights Reserved</p>
            </div>
        </div>

    </div>
</div>
</div>
<!-- Footer ends-->

<!-- .site-content-contain -->
<!-- #page -->
<?php wp_footer(); ?>

<!-- Start of HubSpot Embed Code -->
<script   id="hs-script-loader" async defer src="//js.hs-scripts.com/3817805.js"></script>
<!-- End of HubSpot Embed Code -->

</div>


</body>
</html>
