<?php
/**
 * Template Name: Pricing page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>

<div class="mainWrapper pricingMainWrapper">
    <div class="pricingHeroSection container">
        <div class="pricingHeroRightIllustration pricingHeroIllustration"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/beanbag.svg" alt="pricingHeroRightIllustration"></div>
        <div class="a-center pagemainTitleWrapper">
            <h1 class="pagemainTitle"><?php the_title(); ?></h1>
            <div class="pageInnerText"><p><?php the_field('pricing_hero_sub_title') ?></p></div>                                
        </div>
    </div>
    <div class="container">
        <div class="covid-block">
            <div class="covid-block-inner">
                <div class="covid-block-image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/Waived.svg" alt="waived"></div>
                <p><?php the_field('pricing_covid_text') ?></p>
            </div>
        </div>
    </div>
    <!-- pricingHeroSection Ends -->
    <div class="pricingTableWrapper">
        <div class="container">
            <div class="no-free-trial-block">
                <div class="pricingColWrapper">
                    <div class="pricingColWrapper">

                        <!-- First col starts -->
                        <?php 
                        $pricing_columns = CFS()->get('pricing_page_columns');
                        foreach ($pricing_columns as $pricing_column){ ?>


                            <div class="pricingCol">
                                <div class="price-col-title"><?php echo $pricing_column['pricing_column_main_title']; ?></div>
                                <div class="pricingColInner">
                                    <p class="below-title-para"><?php echo $pricing_column['pricing_column_below_title_text']; ?></p>
                                    <div class="price-amount"><?php echo $pricing_column['pricing_column_price']; ?></div>
                                    <div class="price-per"><?php echo $pricing_column['pricing_column_per_user_text']; ?></div>
                                    <div class="price-billing-type"><?php echo $pricing_column['pricing_column_billed_text']; ?></div>
                                    <div class="price-request-demo-btn-wrapper">
                                        <?php
                                        if(isset($pricing_column['pricing_select_button']['Request trial'])){?>
                                            <a href="<?php echo get_home_url(); ?>/request-trial/" class="request-demo-btn">Launch free trial</a>
                                        <?php }
                                        else if(isset($pricing_column['pricing_select_button']['Request demo'])) { ?>
                                            <a href="<?php echo get_home_url(); ?>/request-a-demo-by-mail/" class="request-demo-btn">Request Demo</a>
                                        <?php }
                                        /* if($pricing_column['pricing_column_main_title']=='Lite') { ?>
                                            <a href="#" class="request-demo-btn primary-btn">14 Day Free Trial</a>
                                        <?php }
                                        else if($pricing_column['pricing_column_main_title']=='Standard') { ?>
                                            <a href="<?php echo get_home_url(); ?>/request-a-demo-by-mail/" class="request-demo-btn theme-button">Request Demo</a>
                                      <?php  } else {?>
                                            <a href="<?php echo get_home_url(); ?>/request-a-demo-by-mail/" class="request-demo-btn green-button">Request Demo</a>
                                        <?php } */ ?>
                                    </div>
                                    <div class="price-service-block">
                                        <?php if($pricing_column['pricing_column_main_title']=='Standard') { ?>
                                            <div class="covid-block-image"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/Waived.svg" alt="waived"></div>
                                        <?php } ?>
                                        <p><?php echo $pricing_column['pricing_column_service_text']; ?></p>
                                        <div class="service-supoort-type"><?php echo $pricing_column['pricing_column_service_second_line']; ?></div>
                                    </div>
                                    <div class="price-col-feature-block">
                                        <div class="price-col-feature-title"><?php echo $pricing_column['pricing_column_plan_type']; ?> </div>
                                        <div class="price-col-feature-list">
                                            <div class="price-col-feature-item"><?php echo $pricing_column['pricing_column_plan_features']; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                        ?>

                        <!-- First col ends -->


                        <!--pricingCol ends-->

                    </div>
                    <!--pricingCol ends--></div>
                </div>
            </div>
        </div>
        <!--pricingTableWrapper ends-->

        <!-- Compare section starts -->
        <div class="compare-plan-section">
            <div class="compare-plan-title"><?php the_field('pricing_plans_main_title') ?></div>
            <div class="compare-plan-table">
                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Lite</th>
                            <th>Standard</th>
                            <th>Enterprise</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                       $pricing_plans = CFS()->get('pricing_plans');
                       foreach ($pricing_plans as $pricing_plan){ ?>
                        <tr>
                            <td><?php echo $pricing_plan['pricing_plan_name']; ?></td>
                            <td class="essential-col">
                                <?php if($pricing_plan['pricing_lite']==1){?>
                                    <svg class="pricing-check-icon" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"/></svg>
                                <?php } else {
                                    echo '-';
                                } ?>
                            </td>
                            <td class="standard-col">
                                <?php if($pricing_plan['pricing_standard']==1){?>
                                    <svg class="pricing-check-icon" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"/></svg>
                                <?php } else {
                                    echo '-';
                                } ?>
                            </td>
                            <td class="advanced-col">
                                <?php if($pricing_plan['pricing_enterprise']==1){?>
                                    <svg class="pricing-check-icon" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"/></svg>
                                <?php } else {
                                    echo '-';
                                } ?>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr class="no-bg">
                        <td></td>
                        <td class="essential-col">
                            <a href="#" class="request-demo-btn primary-btn">14 Day Free Trial</a>
                        </td>
                        <td class="standard-col">
                            <a href="#" class="request-demo-btn theme-button">Request Demo</a>
                        </td>
                        <td class="advanced-col">
                            <a href="#" class="request-demo-btn green-button">Request Demo</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Compare section ends -->

    <div class="honourablePricingSection">
        <div class="container a-center">
            <div class="pageInnerTitle"><?php the_field('pricing_lets_help_title') ?></div>
            <div class="honourablePricingContent"><?php the_field('pricing_lets_help_content') ?></div>

        </div>

        <div class="book-demobutton"><a class="request-demo-btn primary-btn" title="Start Now" href="/request-a-demo-by-mail/">Start Now</a></div>

    </div>
    <!-- honourablePricingSection ends -->

    <!-- More than software section starts -->

    <div class="more-than-soft-section section">
        <div class="container clearfix">
            <div class="more-than-soft-image-col">
                <img src="<?php the_field('pricing_more_than_soft_image') ?>" alt="more-than-software.png">
            </div>
            <div class="more-than-soft-content-col">
                <h2><?php the_field('pricing_more_than_soft_title') ?></h2>
                <div class="more-than-soft-content">
                    <p><?php the_field('pricing_more_than_soft_content') ?></p>
                </div>
            </div>
        </div>
    </div>


    <!-- More than software section ends -->

    <!-- Faq section starts -->

    <div class="no-free-trial-block">
        <div class="pricingFaqSection">
            <div class="container">
                <div class="pageInnerTitle a-center"><?php the_field('pricing_faqs_main_title') ?></div>
                <div class="pricingFaqWrapper">
                    <?php 
                    $faqs = CFS()->get('pricing_faqs'); 
                    foreach($faqs as $faq) {
                        ?>
                        <div class="faqBlock">
                            <h2 class="faqTitle"><?php echo $faq['pricing_faqs_question']; ?><span class="faqToggleHandle"><i class="material-icons">add</i></span></h2>
                            <div class="faqContent">
                                <div class="faqContentInner">
                                    <?php echo $faq['pricing_faqs_answers']; ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
                <!-- pricingFaqWrapper ends-->
                <div class="moreQustionSection">Any more questions? <a href="http://www.visibly.io/support">Visit our resource area</a></div>
            </div>
        </div>
        <!--pricingFaqSection ends--></div>
    </div>
    <!-- Main wrapper ends-->

    <?php get_footer(); ?>
