<?php
/**
 * Template Name: Signup page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
?>
<div class="form-page">
	<div class="mainWrapper signupformMainWrapper">
		<div class="signupformHeroSection container">

			<!-- Main wrapper starts  -->
			<div class="mainSignupForm cf">
				<div class="signupFormBox">
					<h1 class="form-main-title">Let’s get you started..</h1>
					<div class="form-sub-title">Please fill out the form below</div>
					<div class="form-wrapper">
						<?php include('signupform.php') ?>
						<div class="form-image-wrapper">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/contact-bg.jpg" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
</div>
<?php get_footer();
