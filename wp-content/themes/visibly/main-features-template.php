<?php
/**
 * Template Name: Feature main page(Listen, Communicate, Engage)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>

<?php 
    global $post;
    $post_slug = $post->post_name;
?>
<div class="mainWrapper feature-detail-page <?php the_field('theme_color') ?>-theme">
		<main id="main" class="site-main" role="main">
        	<?php
			while ( have_posts() ) : the_post(); ?>
				

<div class="feature-main-page feature-advocacy-diagnostics-page feature-<?php echo $post_slug; ?>-page">
<div class="fds edit-media-section alt centralised-background feature-detail-description-page feature-first-section">
<div class="container clearfix">
<div class="fds-detail-description">
<h1><?php the_field('feature_detail_first_title') ?></h1>
<div><?php the_field('feature_detail_first_content') ?></div>

<div class="free-trial-block">
<div class="btn_div"><a class="request-demo-btn" title="Request Demo" href="<?php echo get_home_url(); ?>/request-a-demo-by-mail/">Request Demo</a> <a class="free-trial-link" href="<?php echo get_home_url(); ?>/request-trial/">Launch free trial</a></div>
</div>

</div>
<div class="fds-image"><img src="<?php the_field('feature_detail_first_image') ?>"></div>
</div>
</div>
<div class="fds alt bg-white organise-content fun-content-creation-section feature-second-section">
<div class="container clearfix">
<div class="fds-image"><img src="<?php the_field('feature_detail_second_image') ?>"></div>
<div class="fds-description fds-inner-detail-description">
<h3><?php the_field('feature_detail_second_title') ?></h3>
<div><?php the_field('feature_detail_second_content') ?></div>
</div>
</div>
</div>
<div class="fds personalise-every-piece connect-sec-background feature-third-section">
<div class="container clearfix">
<div class="fds-image"><img src="<?php the_field('feature_detail_third_image') ?>"></div>
<div class="fds-description fds-inner-detail-description ">
<h3><?php the_field('feature_detail_third_title') ?></h3>
<div><?php the_field('feature_detail_third_content') ?></div>
</div>
</div>
</div>
<div class="fds alt section feature-fourth-section">
<div class="container clearfix">
<div class="fds-image"><img src="<?php the_field('feature_detail_fourth_image') ?>"></div>
<div class="fds-description fds-inner-detail-description ">
<h3><?php the_field('feature_detail_fourth_title') ?></h3>
<div><?php the_field('feature_detail_fourth_content') ?></div>
</div>
</div>
</div>
<div class="fds theme-background feature-fifth-section section">
<div class="container clearfix">
<div class="fds-image"><img src="<?php the_field('feature_detail_fifth_image') ?>"></div>
<div class="fds-description fds-inner-detail-description ">
<h3><?php the_field('feature_detail_fifth_title') ?></h3>
<div><?php the_field('feature_detail_fifth_content') ?></div>
</div>
</div>
</div>
</div>

			<?php endwhile; // End of the loop.
			?>
		

		</main><!-- #main -->
	
</div><!-- .wrap -->

  <?php include($rootPath.'/inc/feature-blocks.php') ?>


<?php
get_footer(); ?>