<?php
/**
 * Template Name: blog
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
$number_of_posts = get_field('number_of_posts_to_display');
?>







<div id="blog_listing_page">
    <div id="new_grid">
        
<h1 id="h1_title">Blog</h1>
        <!-- FORM SECTION STARTS -->
        <section class="join_section">

            <div class="container">
                
                <div class="title text-center">
                    <h3 class="section_title"><span class="line_txt"><?php the_field('blogpage_title') ?></span></h3>
                    <p class="title_descr"><?php the_field('blogpage_subtitle') ?></p>
                </div>

                <div class="form_wrapper">
                    <!-- Main wrapper starts  -->
                    <!--[if lte IE 8]>
                    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                    <![endif]-->
                    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                    <script>
                        hbspt.forms.create({
                            portalId: "3817805",
                            formId: "7021049f-325f-4ec1-837c-4a8da6f167fd"
                        });
                    </script>
                    <p class="policy_txt">Read our <a href="<?php echo get_home_url(); ?>/privacy-policy">Privacy Policy</a></p>
                
                </div>
            </div> 

        </section>
        <!-- FORM SECTION ENDS -->


        <!-- HERO SECTION STARTS -->
        <section class="hero_section">
            <div class="container">

                 <?php

    $args = array(
        'post_type'        => 'post',
        'post_status'      => 'publish',
        'order' => 'ASC',
        'posts_per_page' => 1,
        'post__in'  => get_option( 'sticky_posts' ),
        'ignore_sticky_posts' => 1
        );


    $sticky_posts_array = get_posts( $args );

    if( sizeof($sticky_posts_array) != 0 )
    {    
        foreach ($sticky_posts_array as $post) { ?>
        
          
        <?php
                        $term_list = wp_get_post_terms($post->ID, 'category', ['fields' => 'all']);
                        $primaryCategory='';
                        foreach($term_list as $term) {
                         if( get_post_meta($post->ID, '_yoast_wpseo_primary_category',true) == $term->term_id ) {
                            $primaryCategory = $term->name;
                        }
                    }

                    ?>



                <div class="category_title">
                    <p class="cat_title"><?php echo $primaryCategory; ?></p>
                </div>

                <div class="hero">
                    <div class="flex_view middle">
                        <div class="blog_hero">
                            <div class="hero_banner">
                                <div class="image">
                                    <?php  if ( has_post_thumbnail() ) {?>
                                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="wp-post-image">
                                        <?php 
                                    } ?>
                                </div>
                            </div>
                        </div>
                        <div class="blog_txt">
                            <div class="text_wrap">
                                <h3 class="blog_title"><?php echo $post->post_title; ?></h3>
                                <p class="blog_excerpt"><?php the_excerpt(); ?></p>
                                <div class="read_article_url"> <a class="link_with_arrow" href="<?php the_permalink(); ?>" title="Read article">Read article
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                  <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
                  <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
                </svg></a></div>

                            </div>
                        </div>
                    </div>
                </div>


<?php }
} ?>
                
            </div>
        </section>  
        <!-- HERO SECTION ENDS -->

        <!-- ARTICLE SECTION STARTS -->
        <section class="articles_section">
            <div class="container">
                <div class="title_section text-center">
                    <h3 class="title_txt"><span class="line_txt">ALL ARTICLES</span></h3>
                </div>

                <div class="cat_select_div text-center">
                    <div class="category_div custom_select flex_view">

                        <div class="custom_select_trigger flex_view middle center">
                            <span class="current_cat">Choose Topic</span>
                        </div>
              
                        <div class="custom_option_box">
               
                           <?php 
                           $categories = get_categories( array(
                              'orderby' => 'name',
                              'parent'  => 0,
                              'hide_empty' => 1,
                          ) );
                           foreach ( $categories as $category ) { 
                            if($category->slug!='uncategorized'){
                                ?>
                                <span class="custom_option" data-value="<?php echo $category->slug ?>"><?php echo $category->name ?></span>
                            <?php } 
                        } ?>
                        <span class="custom_option" data-value="select-all">Select All</span>
                     
                        
                        </div>
                      
                    </div>  
                </div>
                
            </div>

            <div class="article_list">
                <div class="inner_wrap">

                    <div class="article_row ">


<!--  -->
<?php 

  //get the current page
  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

  //pagination fixes prior to loop
 // $temp =  $query;
  $query = null;

  //custom loop using WP_Query
  $query = new WP_Query( array( 
    'post_type'        => 'post',
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
     'posts_per_page' => -1,
     'paged'=>$paged,
     'post__not_in'  => get_option( 'sticky_posts' ),
  ) ); 

 //set our query's pagination to $paged
 //$query -> query('post_type=post&posts_per_page=5'.'&paged='.$paged);

 if ( $query->have_posts() ) : ?>
    <div class="flex_view">
   <?php while ( $query->have_posts() ) : $query->the_post();
    ?>
    <?php  $posts_array = get_posts( $query );

   
    
        $categories = get_the_category();
        $cls = '';

        if ( ! empty( $categories ) ) {
          foreach ( $categories as $cat ) {
            $cls .= $cat->slug . ' ';
        }
    }
    

    ?>
     <div class="article select-all <?php echo $cls;?>">
                              <?php
                        $term_list = wp_get_post_terms($post->ID, 'category', ['fields' => 'all']);
                        $primaryCategory='';
                        foreach($term_list as $term) {
                         if( get_post_meta($post->ID, '_yoast_wpseo_primary_category',true) == $term->term_id ) {
                            $primaryCategory = $term->name;
                        }
                    }

                    ?>
                            <div  class="article_url flex_view middle">
                                <span class="border sm-hide"></span>
                                <div class="article_image_wrap">
                                    <div class="image">
                                         <?php  if ( has_post_thumbnail() ) {?>
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="wp-post-image">
                                            <?php 
                                        } ?>
                                    </div>
                                </div>
                                <div class="article_text_wrap">
                                    <h3 class="article_title"><a href="<?php the_permalink(); ?>"><?php echo $post->post_title; ?></a></h3>
                                    <div class="article_descr"><?php the_excerpt(); ?></div>
                                    <p class="article_category"><?php echo $primaryCategory; ?></p>
                                </div>
                                
                            </div>
                        </div>
                        
  <?php endwhile;?>
</div>


<?php endif; ?>

<?php //reset the following that was set above prior to loop
$query = null; 
//$query = $temp; ?>
<!--  -->



 
                    </div>

                   
                </div>

            </div>
        </section>
        <!-- ARTICLE SECTION ENDS -->

      

    </div>

</div>

<?php get_footer(); ?>