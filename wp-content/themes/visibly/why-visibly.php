<?php
/**
 * Template Name: Why visibly
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>

 <div class="mainWrapper why-visibly-page">
        
        <div class="pageTitleWrapper page-container">
        	<?php $sectors = CFS()->get('why_visibly_sector');
        	$sector_cnt = 1;
        	foreach ($sectors as $sector) {?>
        	
            <div class="innerPageTitleWrapper <?php if($sector_cnt%2==0) {echo 'alt';} ?>">
                <div class="bannerTitleImage">
                    <div class="image-container"><img src="<?php echo $sector['why_visibly_image'] ?>" alt="<?php echo $sector['why_visibly_title'] ?>"></div>
                </div>
                <div class="bannerTitle">
                    <div class="text-description">
                        <h1 class="PageTitle"><?php echo $sector['why_visibly_title'] ?></h1>
                        <div class="mainTitle"><?php echo $sector['why_visibly_subtitle'] ?></div>
                        <div class="Paragraph"><?php echo $sector['why_visibly_content'] ?></div>
                        <div class="full-width-buttons">
                            <div class="signupForm">
                                <div class="free-trial-block">
                                    <div class="book-demobutton"><a class="free-trial-link" title="Request Demo" href="<?php echo get_home_url(); ?>/request-a-demo-by-mail/">Request Demo</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

           <?php $sector_cnt++; }
        	 ?>
            
            
        </div>
        
    </div>


<?php get_footer(); ?>