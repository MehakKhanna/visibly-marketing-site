<div class="postAuthorImg"><?php
if (function_exists ( 'mt_profile_img'  ) ) {
    $author_id=$post->post_author;
    mt_profile_img( $author_id, array(
        'size' => 'full' ,
        'attr' => array( 'alt' => 'Alternative Text' ),
        'echo' => true )
    );
}
?></div>