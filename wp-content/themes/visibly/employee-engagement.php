<?php
/*
 * Template Name: Employee engagement
 */
get_header();


 ?>

<div class="mainWrapper employee-engagement-page">
		<main id="main" class="site-main" role="main">
        
			<?php
			while ( have_posts() ) : the_post();
                
				the_content(); ?>

		<?php endwhile; // End of the loop.
		include('inc/complete-platform.php'); 
			?>
		</main><!-- #main -->
	
</div><!-- .wrap -->

<?php get_footer();
