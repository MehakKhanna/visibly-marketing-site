<?php
/**
 * Template Name: Customer success
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>
 <div id="customerSuccessPage" class="mainWrapper ">
        <!-- Banner section starts -->
        <section class="banner_section">
            <div class="banner_background" style="background:url(<?php the_field('customer_success_hero_section_background_image') ?>)"></div>
            <div class="banner_wrap container clearfix">

                <div class="banner_txt">
                    <h1 class="banner_title"><?php the_field('customer_success_hero_section_title') ?></h1>
                    <p><?php the_field('customer_success_hero_section_subtitle') ?></p>

                </div>

                <div class="banner_img img"> <img src="<?php the_field('customer_success_hero_section_image') ?>" alt="<?php the_field('customer_success_hero_section_title') ?>">
                </div>

            </div>
        </section>
        <!-- Banner section ends -->

        <!-- Exploration section starts -->
        <section class="exploration_section section">
            <div class="container">
                <h2><?php the_field('customer_success_exploration_section_title') ?></h2>
                <div class="exploration_content"><?php the_field('customer_success_exploration_section_paragraph') ?>
                </div>
            </div>
        </section>

        <!-- Exploration section ends -->

        <!-- Planning section starts -->
        <section class="planning_section section">
            <img class="planning-bg" src="<?php the_field('customer_success_planning_section_background_image') ?>" alt="Planning">
            <div class="container clearfix">
                <div class="planning_description">
                    <h2><?php the_field('customer_success_planning_section_title') ?></h2>
                    <div class="planning_desc_content"><?php the_field('customer_success_planning_section_paragraph') ?></div>
                </div>
                <div class="planning_image">
                    <img src="<?php the_field('customer_success_planning_section_image') ?>" alt="Planning">
                </div>
            </div>
        </section>
        <!-- Planning section ends -->

        <!-- Training section starts -->
        <section class="training_section section">
            <div class="container">
                <div class="training-top-part">
                <h2><?php the_field('customer_success_training_section_title') ?></h2>
                <div class="training-top-part-content"><?php the_field('customer_success_training_section_paragraph_1') ?></div>
            </div>

            <div class="clearfix">
                 <div class="training_image_wrapper clearfix">
                    <div  class="training_image_inner_wrapper">
                       <div class="training_image_col"> 
                            <img src="<?php the_field('customer_success_training_section_image_1') ?>" alt="Training">
                        </div>
                        <div class="training_image_col"> 
                            <img src="<?php the_field('customer_success_training_section_image_2') ?>" alt="Training">
                        </div>
                    </div>
                </div>
                <div class="training_description">
                    <div class="training_desc_content"><?php the_field('customer_success_training_section_paragraph_2') ?></div>
                </div>
               
            </div>
            </div>
        </section>
        <!-- Training section ends -->

        <!-- Exploration section starts -->
        <section class="launch_support_section section">
            <div class="container">
                <h2><?php the_field('customer_success_launch_section_title') ?></h2>
                <div class="launch_support_content"><?php the_field('customer_success_launch_section_paragraph') ?></div>
            </div>
        </section>

        <!-- Exploration section ends -->



    </div>
    <!-- Main wrapper ends-->    
<?php
get_footer(); ?>