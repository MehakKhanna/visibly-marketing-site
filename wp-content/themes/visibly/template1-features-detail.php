<?php
/*
 * Template Name: Template1 features detail
 */
get_header();


 ?>

<div class="mainWrapper feature-detail-page">
		<main id="main" class="site-main" role="main">
        <h1 class="feartures-detail-title"><?php the_title() ?></h1>
			<?php
			while ( have_posts() ) : the_post();
				the_content();
			endwhile; // End of the loop.
			?>
			<div class="more-feature-section features_overview">
				<div class="inner_wrap">
					<div class="container">
						<div class="section_title text-center">
							<h3 class="title"><span class="square"><span class="square_two"></span>More Features</span></h3>
						</div>
							<?php 
							$slug = get_field('feature_category');
							if($slug=='engage'){
							include($rootPath.'/inc/engage-features.php');
							}
							else if($slug=='communicate'){
							include($rootPath.'/inc/communicate-features.php');	
							}
							else if($slug=='monitor'){
							include($rootPath.'/inc/monitor-features.php');	
							}
							else{
							include($rootPath.'/inc/advocate-features.php');	
							}
							//include($rootPath.'/inc/more-features.php');
							//dynamic_sidebar('Feature-detail-common-section') ?>
					</div>
				</div>
			</div>

		</main><!-- #main -->
	
</div><!-- .wrap -->

<?php get_footer();
