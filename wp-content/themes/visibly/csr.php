<?php
/*
 * Template Name: CSR
 */
get_header();


 ?>

<div class="mainWrapper csr-page">
		<main id="main" class="site-main" role="main">
        	<?php
			while ( have_posts() ) : the_post();
                
				the_content(); ?>

		<?php endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	
</div><!-- .wrap -->

<?php get_footer();
