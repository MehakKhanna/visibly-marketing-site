<?php
/**
 * Template Name: Platformpage
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();


$platform_create_message_title = get_post_meta($post->ID, 'wpcf-platform-create-message-title', true);
$platform_create_message_description = get_post_meta($post->ID, 'wpcf-platform-create-message-description', true);
$platform_create_message_video_link = get_post_meta($post->ID, 'wpcf-platform-create-message-video-link', true);


$platform_moderate_title = get_post_meta($post->ID, 'wpcf-platform-moderate-title', true);
$platform_moderate_description = get_post_meta($post->ID, 'wpcf-platform-moderate-description', true);
$platform_moderate_video_link = get_post_meta($post->ID, 'wpcf-platform-moderate-video-link', true);


$platform_publish_and_share_title = get_post_meta($post->ID, 'wpcf-platform-publish-and-share-title', true);
$platform_publish_and_share_description = get_post_meta($post->ID, 'wpcf-platform-publish-and-share-description', true);
$platform_publish_and_share_video_link = get_post_meta($post->ID, 'wpcf-platform-publish-and-share-video-link', true);


$platform_feedback_title = get_post_meta($post->ID, 'wpcf-platform-feedback-title', true);
$platform_feedback_description = get_post_meta($post->ID, 'wpcf-platform-feedback-description', true);
$platform_feedback_video_link = get_post_meta($post->ID, 'wpcf-platform-feedback-video-link', true);


$platform_measure_title = get_post_meta($post->ID, 'wpcf-platform-measure-title', true);
$platform_measure_description = get_post_meta($post->ID, 'wpcf-platform-measure-description', true);
$platform_measure_video_link = get_post_meta($post->ID, 'wpcf-platform-measure-video-link', true);

?>
<!-- Main wrapper starts  -->
    
    <div class='mainWrapper platformMainWrapper'>
        <!-- Platform section starts-->
        <div class="platform-page">
                <div class="cloud cloud1"></div>
                <div class="cloud cloud2"></div>
                <div class="cloud cloud3"></div>
                <div class="plane-wrapper"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/platform-plane-image.png" alt="platform-plane-image" class="platform-plane-image"></div>
                 <div class="a-center platform-title-wrapper">
                                    <div class="pagemainTitle"><h1 class = "pageTitleForH1Tag"><?php the_title() ?> </h1></div>
                                    <div class="pageInnerText"><?php echo get_post_meta($post->ID, 'wpcf-subtitle', true); ?></div>
                                    <div class="down-arrow-wrapper a-center">
                                        <div class="down-arrow"></div>
                                    </div>
                                </div>
                    <div  id="create-message-section">
                            <div class="container">
                                <div class="create-message-section selected cf animate_parent">
                                        <div class="chatboxWrapper">
                                            <div class="chatbox">
                                                <div class="user-thumbnail">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/user-thumbnail.jpg" alt="user-thumbnail" />
                                                </div>
                                                <div class="chatboxText">
                                                    <div class="chatboxText1">Charity skydive photos.</div>
                                                    <div class="chatboxText2">Sent for moderation!</div>
                                                </div>
                                                <div class="chatbox-smile-icon-wrapper">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/smily-image.png" alt="smily-image">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="platformMessageWrapper">
                                            <div class="platformMessage">
                                                <div class="platformMessageTitle"><?php echo $platform_create_message_title ; ?></div>
                                                <div class="platformMessageDescription"><?php echo $platform_create_message_description; ?></div>
                                                <div class="watchVideoLink" data-url="<?php echo $platform_create_message_video_link; ?>"><span  class="platform-title-icon"><i class="fa fa-play" aria-hidden="true"></i></span> Watch video</div>
                                            </div>
                                        </div>
                                        <div class="platform-arrow platform-arrow1"></div>
                                    </div>
                                    <!-- create-message-section ends -->
                                </div>
                    </div>

                <div class="moderate-message-section  section cf alt animate_parent">
                        <div class="moderate-message-inner">
                        <div class="container">
                    <div class="chatboxWrapper moderatorChatbox">
                        <div class="chatbox">
                            <div class="user-thumbnail">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/user-thumbnail-1.jpg" alt="user-thumbnail" />
                            </div>
                            <div class="chatboxText">
                                <div class="chatboxText1">@EricC we&apos;ve approved this post.</div>
                                <div class="chatboxText2">Best of luck with the donations!</div>
                            </div>
                        </div>
                    </div>
                    <div class="platformMessageWrapper">
                        <div class="platformMessage">
                            <div class="platformMessageTitle"><?php echo $platform_moderate_title; ?></div>
                            <div class="platformMessageDescription"><?php echo$platform_moderate_description; ?></div>
                            <div class="watchVideoLink" data-url="<?php echo $platform_moderate_video_link; ?>"><span  class="platform-title-icon"><i class="fa fa-play" aria-hidden="true"></i></span> Watch video</div>
                        </div>
                    </div>
                    <div class="platform-arrow platform-arrow2"></div>
                    </div>
                    </div>
                </div>
                <!-- moderate-message-section ends -->

                <div class="thanks-message-section   cf alt animate_parent">
                    <div class="thanks-message-inner">
                            
                    <div class="chatboxWrapper">
                        <div class="chatbox">
                            <div class="user-thumbnail">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/user-thumbnail.jpg" alt="user-thumbnail" />
                            </div>
                            <div class="chatboxText">
                                <div class="chatboxText1">Thanks!!</div>
                            </div>
                            <div class="chatbox-smile-icon-wrapper">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/smily-image.png" alt="smily-image">
                            </div>
                        </div>
                    </div>
                    <div class="platform-arrow platform-arrow4"></div>
                    </div>
                </div>
                <!-- thanks-message-section ends -->

                <div class="publish-message-section   alt animate_parent">
                        <div class="publish-message-inner">
                    <div class="container">
                    <div class="cf">
                    <div class="platformMessageWrapper">
                        <div class="platformMessage">
                            <div class="platformMessageTitle"><?php echo $platform_publish_and_share_title; ?></div>
                            <div class="platformMessageDescription"><?php echo $platform_publish_and_share_description; ?></div>
                            <div class="watchVideoLink" data-url="<?php echo $platform_publish_and_share_video_link; ?>"><span  class="platform-title-icon"><i class="fa fa-play" aria-hidden="true"></i></span> Watch video</div>
                        </div>
                    </div>
                    <div class="platform-arrow platform-arrow3"></div>
                    <div class="platform-twitter-icon">
                        <i class="fab fa-twitter"></i>
                    </div>
                    </div>
                </div>
                    </div>
                    <!-- platform-twitter-icon ends -->
                </div>
                <!-- publish-message-section ends -->

                <div class="about-photo-block  ">
                    <div class="about-photo-inner">
                        <div class="container">
                    <div class="about-photo-block-inner">
                    <div class="chatboxWrapper">
                        <div class="chatbox">
                            <div class="user-thumbnail">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/user-thumbnail-2.jpg" alt="user-thumbnail-2" />
                            </div>
                            <div class="chatboxText">
                                <div class="chatboxText1">Heh @Comms_Jim,</div>
                                <div class="chatboxText2">how about my photos?</div>
                            </div>
                        </div>
                    </div>
                    <div class="platform-arrow5 platform-arrow"></div>

                    <div class="platform-internal-icon">
                            <div class="internal-comment-count">25 Comments</div>
                        </div>
                        <!-- platform-internal-icon ends -->
    
                        <div class="platform-facebook-icon">
                            <i class="fab fa-facebook-f"></i>
                        </div>
                        <!-- platform-facebook-icon ends -->
    
                        <div class="platform-linkedin-icon">
                            <i class="fab fa-linkedin-in"></i>
                            <div class="linkedin-like-count">
                                <i class="fa fa-thumbs-up" aria-hidden="true"></i> 25</div>
                        </div>
                        <!-- platform-linkedin-icon ends -->
                        </div>
                        </div>
                        </div>
                </div>
                <!-- about-photo-block ends -->

                <div class="feedback-section animate_parent   ">
                        <div class="container">
                    <div class="feedback-section-inner cf">
                        <div class="platformMessageWrapper">
                            <div class="platformMessage">
                                <div class="platformMessageTitle"><?php echo $platform_feedback_title; ?></div>
                                <div class="platformMessageDescription"><?php echo $platform_feedback_description; ?></div>
                                <div class="watchVideoLink" data-url="<?php echo $platform_feedback_video_link; ?>"><span  class="platform-title-icon"><i class="fa fa-play" aria-hidden="true"></i></span> Watch video</div>
                            </div>
                        </div>

                        <div class="chatboxWrapper moderatorChatbox">
                                <div class="chatbox">
                                    <div class="user-thumbnail">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/user-thumbnail-1.jpg" alt="user-thumbnail" />
                                    </div>
                                    <div class="chatboxText">
                                        <div class="chatboxText1">@SarahH all good except</div>
                                        <div class="chatboxText2">the 1st one. you&apos;re wearing</div>
                                        <div class="chatboxText3">t-shirt with our old brand!</div>
                                    </div>
                                    <div class="chatbox-smile-icon-wrapper">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/grinmacing_emoji.png" alt="grinmacing_emoji">
                                    </div>
                                </div>
                            </div>
                                <!-- feedback-chatbox-block ends -->
                        <div class="platform-arrow platform-arrow10"></div>
                        </div>
                        </div>
                    </div>
                    <!-- feedback-section ends -->


                <div class="measure-section cf animate_parent">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/platform-plane-image.png" alt="platform-plane-image" class="platform-plane-image-2">
                        <div class="container">
                    <div class="measure-block">
                        <div class="platformMessageTitle"><?php echo $platform_measure_title; ?></div>
                        <div class="platformMessageDescription"><?php echo $platform_measure_description; ?></div>
                        <div class="watchVideoLink" data-url="<?php echo $platform_measure_video_link; ?>"><span  class="platform-title-icon"><i class="fa fa-play" aria-hidden="true"></i></span> Watch video</div>
                        <div class="platform-arrow platform-arrow12"></div>
                    </div>
                    </div>
                </div>
                <!-- measure-section ends -->

                <div class="platform-social-wrapper animate_parent">
                    <div class="platform-social-inner cf">
                    <div class="platform-social-col facebook-col">
                        <div class="platform-social-col-inner">
                            <div class="platform-social-icon">
                                <i class="fab fa-facebook-f"></i>
                            </div>
                            <div class="platform-social-row">
                                <div class="platform-social-count">20</div>
                                <div class="platform-icon">
                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="platform-social-row">
                                <div class="platform-social-count">8K</div>
                                <div class="platform-icon">
                                    <i class="fa fa-share" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="platform-social-row">
                                <div class="platform-social-count">1</div>
                                <div class="platform-icon">
                                    <i class="fa fa-comment" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="platform-social-col twitter-col">
                        <div class="platform-social-col-inner">
                            <div class="platform-social-icon">
                                <i class="fab fa-twitter"></i>
                            </div>
                            <div class="platform-social-row">
                                <div class="platform-social-count">18</div>
                                <div class="platform-icon">
                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="platform-social-row">
                                <div class="platform-social-count">4K</div>
                                <div class="platform-icon">
                                    <i class="fa fa-share" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="platform-social-row">
                                <div class="platform-social-count">10</div>
                                <div class="platform-icon">
                                    <i class="fa fa-comment" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="platform-social-col linkedin-col">
                        <div class="platform-social-col-inner">
                            <div class="platform-social-icon">
                                <i class="fab fa-linkedin-in"></i>
                            </div>
                            <div class="platform-social-row">
                                <div class="platform-social-count">35</div>
                                <div class="platform-icon">
                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="platform-social-row">
                                <div class="platform-social-count">2K</div>
                                <div class="platform-icon">
                                    <i class="fa fa-share" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="platform-social-row">
                                <div class="platform-social-count">99</div>
                                <div class="platform-icon">
                                    <i class="fa fa-comment" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="platform-arrow13 platform-arrow"></div>
                </div>
                <!-- platform-social-wrapper ends -->

                <div class="no-sweat-block animate_parent">
                    <div class="chatboxWrapper">
                        <div class="chatbox">
                            <div class="user-thumbnail">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/user-thumbnail-2.jpg" alt="user-thumbnail-2" />
                            </div>
                            <div class="chatboxText">
                                <div class="chatboxText1">Ok no sweat </div>
                            </div>
                            <div class="chatbox-smile-icon-wrapper">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/smiling-emoji.png" alt="grinmacing_emoji">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- no-sweat-block ends -->
           
                <div class="platform-no-sweat-text-block a-center animate_parent  ">
                        <div class="earth-view"></div>
                    <div class="platform-no-sweat-text-block-inner">
                    <h2>There you have it, no sweat!</h2>
                    <p>FRICTIONLESS EMPLOYEE generated content</p>
                    <div class="platform-arrow platform-arrow14"></div>
                    </div>
                </div>
            

           
           
        
        </div>
    </div>
    <div class="videoPopupWrapper">
    <div class="videoOverlay"></div>
    <div class="videoPopup">
        <a href="javascript:void(0);" title="Close popup" class="closeVideoPopup"><svg xmlns="http://www.w3.org/2000/svg" viewBox="-255 347 100 100" width="24" aria-label="Close Navigation" shape-rendering="geometricPrecision">
                                        <path class="close" d="M-160.4 434.2l-37.2-37.2 37.1-37.1-7-7-37.1 37.1-37.1-37.1-7 7 37.1 37.1-37.2 37.2 7.1 7 37.1-37.2 37.2 37.2"></path>
                                    </svg></a>
        <iframe src="https://player.vimeo.com/video/283185927" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> 
        
    </div>
    </div>
    <!-- Main wrapper ends-->    

<?php get_footer();
