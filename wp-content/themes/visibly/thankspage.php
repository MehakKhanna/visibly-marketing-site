<?php /* Template Name: Thanks page */
get_header(); 
while ( have_posts() ) : the_post(); ?>
<div class="thanks-signup-page">
    <h2><?php  the_title() ?></h2>
      <div class="thanks-content"><?php the_content(); ?></div>
      <div class="thanks-image">
      <img src="<?php the_field('thankyou_image') ?>">
      </div>
    </div>
    <?php endwhile; 
get_footer();
?>