<?php
/**
 * Template Name:Featurepage
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>



<div id="feature_page">
	<div id="new_grid">

		<!-- Banner section starts -->
		<section class="banner_section">
			<div class="inner_wrap">
				<div class="container">

					<!-- <div class="banner_title text-center">
						<h3 class="title_txt"><span class="square">One platform for all of<br> your culture needs</span></h3>
					</div> -->

					<div class="banner_image">
						<div class="image img">
							<img src="<?php echo get_template_directory_uri(); ?>/images/featured_banner.png" alt="banner_image">
						</div>
						<div class="banner_txt">
							<h3 class="quote_txt">"It's like Hubspot but<br>for people culture."</h3>
						<!-- 	<h3 class="author_name">Andrew Platt-Higgins</h3>
							<h3 class="author_desgn">Global Communications Lead</h3> -->
						</div>
					</div>
					
				</div>
			</div>
		</section>
		<!-- Banner section ends -->


		 <!-- Features overview section starts -->

		<section class="features_overview">
			<div class="inner_wrap">
				<div class="container">

					<div class="section_title text-center">
						<h3 class="title"><span class="square"><span class="square_two"></span>Features overview</span></h3>
					</div>
					
						
						<div class="features_list">
							<ul class="flex_view feature_ul">

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31 32" style="enable-background:new 0 0 31 32;" xml:space="preserve"> <path class="feeds-st0" d="M1,7.5v19.7C1,28.7,2.3,30,3.8,30h19.7v-2.8H3.8V7.5H1z"></path> <rect x="8.5" y="3.7" class="feeds-st1" width="18.6" height="18.6"></rect> <path class="feeds-st0" d="M26.2,2H9.6C8,2,6.8,3.3,6.8,4.8v16.7c0,1.5,1.2,2.8,2.8,2.8h16.7c1.5,0,2.8-1.3,2.8-2.8V4.8 C29,3.3,27.8,2,26.2,2z M26.4,21.6h-17V4.7h17V21.6z"></path> <polygon class="feeds-st2" points="14.4,15.6 16.8,18.7 20.2,14.5 24.8,20.2 11,20.2 "></polygon> </svg>
											</span>
											FEEDS
										</h3>
										<p class="feature_descr">Tailored feeds for all communication needs.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/feeds" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/features_gamification.svg');?> 
											</span>
											GAMIFICATION
										</h3>
										<p class="feature_descr">Create competition through points and leaderboards.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/gamification" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								
								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/features_pollsnpulse.svg');?> 
											</span>
											POLLS AND Recognition
										</h3>
										<p class="feature_descr">Send polls and recognition messages.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/polls-recognition" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/icons/curation.svg');?> 
											</span>
											Curation
										</h3>
										<p class="feature_descr">Personal and global content curation.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/curation" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>



								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31 32" style="enable-background:new 0 0 31 32;" xml:space="preserve"> <rect x="3.7" y="5.1" class="campaign-st0" width="24.1" height="24.6"></rect> <path class="campaign-st1" d="M26.9,3.6h-1.6V0.4h-3.1v3.1H9.8V0.4H6.7v3.1H5.1C3.4,3.6,2,5,2,6.7v21.8c0,1.7,1.4,3.1,3.1,3.1h21.8 c1.7,0,3.1-1.4,3.1-3.1V6.7C30,5,28.6,3.6,26.9,3.6z M26.9,28.4H5.1V12.9h21.8V28.4z M26.9,9.8H5.1V6.7h21.8V9.8z"></path> <rect x="8.2" y="16" class="campaign-st2" width="7.8" height="7.8"></rect> </svg>
											</span>
											CAMPAIGN MANAGEMENT
										</h3>
										<p class="feature_descr">Enterprise project management workflow.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/campaign-management" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/features_printmedia.svg');?> 
											</span>
											PRINT
										</h3>
										<p class="feature_descr">Design, print and ship employee magazines.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/print" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								

								<!-- <li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/icons/Project-Manage.svg');?> 
											</span>
											Project management
										</h3>
										<p class="feature_descr">End to end workflow tools.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/communicate/project-management" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li> -->


								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/features_mobile.svg');?> 
											</span>
											SMS & Push
										</h3>
										<p class="feature_descr">Create and send bespoke SMS notifications.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/sms-push" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<!-- <li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php //include('images/features_advocacy.svg');?> 
											</span>
											Push notifications
										</h3>
										<p class="feature_descr">Keep the workforce up to date with urgent company news via push.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php //echo get_home_url() ?>/features/push-notifications" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li> -->

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/icons/email.svg');?> 
											</span>
											Email
										</h3>
										<p class="feature_descr">Create and send beautiful HTML emails. 
</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/email" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/icons/chat.svg');?> 
											</span>
											Chat
										</h3>
										<p class="feature_descr">Utilise both 1-2-1 and group instant chat.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/chat" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
												<path class="culture-st0" d="M7,10c-2.2,0-4,1.8-4,4s1.8,4,4,4s4-1.8,4-4S9.2,10,7,10z M15,15c-1.6,0-3,1.4-3,3s1.4,3,3,3s3-1.4,3-3
												S16.7,15,15,15z M16.5,3c-3,0-5.5,2.5-5.5,5.5s2.5,5.5,5.5,5.5S22,11.5,22,8.5S19.5,3,16.5,3z"/>
												<path class="culture-st1" d="M0,0h24v24H0V0z"/>
												<path class="culture-st2" d="M7,10c-2.2,0-4,1.8-4,4s1.8,4,4,4s4-1.8,4-4S9.2,10,7,10z M7,16c-1.1,0-2-0.9-2-2s0.9-2,2-2s2,0.9,2,2
												S8.1,16,7,16z M15,15c-1.6,0-3,1.4-3,3s1.4,3,3,3s3-1.4,3-3S16.7,15,15,15z M15,19c-0.6,0-1-0.5-1-1s0.4-1,1-1s1,0.5,1,1
												S15.6,19,15,19z M16.5,3c-3,0-5.5,2.5-5.5,5.5s2.5,5.5,5.5,5.5S22,11.5,22,8.5S19.5,3,16.5,3z M16.5,12c-1.9,0-3.5-1.6-3.5-3.5
												S14.6,5,16.5,5S20,6.6,20,8.5S18.4,12,16.5,12z"/>
											</svg> 
											</span>
											Culture & ENGAGEMENT
										</h3>
										<p class="feature_descr">Pulse employee engagement surveys.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/culture-engagement" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/features_advocacy.svg');?> 
											</span>
											Experience & NPS
										</h3>
										<p class="feature_descr">Measure customer and job seeker experience.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/experience-nps" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
												<path class="social-analytics-st0" d="M19.2,3h-14c-1.1,0-2,0.9-2,2v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21.2,3.9,20.3,3,19.2,3z"/>
												<path class="social-analytics-st1" d="M0,0h24v24H0V0z"/>
												<path class="social-analytics-st2" d="M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M19,19H5V5h14V19z M7,10
												h2v7H7V10z M11,7h2v10h-2V7z M15,13h2v4h-2V13z"/>
											</svg>

											</span>
											Social analytics
										</h3>
										<p class="feature_descr">Social media analytics for the enterprise </p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/social-analytics" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<!-- <li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/icons/Reputation-Managment-Final.svg');?> 
											</span>
											Brand reputation
										</h3>
										<p class="feature_descr">Monitor your brand on social and review sites.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/brand-reputation" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li> -->

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/icons/ic-values.svg');?> 
											</span>
											AI values
										</h3>
										<p class="feature_descr">Measure your internal value alignment.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/ai-values" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>


								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31 32" style="enable-background:new 0 0 31 32;" xml:space="preserve"> <path class="analytic-st0" d="M-2-2h34v34H-2V-2z"></path> <circle class="analytic-st1" cx="16" cy="16" r="14.2"></circle> <path class="analytic-st2" d="M27.2,14.6h-9.8V4.8C22.5,5.4,26.6,9.5,27.2,14.6z"></path> <path class="analytic-st2" d="M4.7,16c0-5.8,4.3-10.5,9.9-11.2v22.5C9,26.5,4.7,21.8,4.7,16z"></path> <path class="analytic-st2" d="M17.4,27.2v-9.8h9.8C26.6,22.5,22.5,26.6,17.4,27.2z"></path> </svg> 
											</span>
											IC analytics
										</h3>
										<p class="feature_descr">Measure IC and content resonance.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/internal-communication-analytics" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<!-- <li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">

												<path class="customer-adovocacy-st0" d="M21.4,11.6l-9-9C12.1,2.2,11.6,2,11,2H4C2.9,2,2,2.9,2,4v7c0,0.6,0.2,1.1,0.6,1.4l9,9c0.4,0.4,0.9,0.6,1.4,0.6
												s1.1-0.2,1.4-0.6l7-7c0.4-0.4,0.6-0.9,0.6-1.4S21.8,11.9,21.4,11.6z"/>
												<path class="customer-adovocacy-st1" d="M0,0h24v24H0V0z"/>
												<path class="customer-adovocacy-st2" d="M21.4,11.6l-9-9C12.1,2.2,11.6,2,11,2H4C2.9,2,2,2.9,2,4v7c0,0.6,0.2,1.1,0.6,1.4l9,9c0.4,0.4,0.9,0.6,1.4,0.6
												s1.1-0.2,1.4-0.6l7-7c0.4-0.4,0.6-0.9,0.6-1.4S21.8,11.9,21.4,11.6z M13,20l-9-9V4h7v0l9,9L13,20z"/>
												<circle class="customer-adovocacy-st2" cx="6.5" cy="6.5" r="1.5"/>
												<path class="customer-adovocacy-st2" d="M8.9,12.6c0,0.6,0.2,1.1,0.6,1.4l3.5,3.5l3.5-3.5c0.4-0.4,0.6-0.9,0.6-1.4c0-1.1-0.9-2.1-2.1-2.1
												c-0.6,0-1.1,0.2-1.4,0.6L13,11.7l-0.6-0.6c-0.4-0.4-0.9-0.6-1.4-0.6C9.8,10.5,8.9,11.4,8.9,12.6z"/>
											</svg>

											</span>
											Customer advocacy
										</h3>
										<p class="feature_descr">Leverage customer brand advocates.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/customer-advocacy" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li> -->

								<!-- <li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
												<path class="employee-advocacy-st0" d="M22,12c0,5.5-4.5,10-10,10S2,17.5,2,12S6.5,2,12,2S22,6.5,22,12z"/>
												<path class="employee-advocacy-st1" d="M0,0h24v24H0V0z"/>
												<path class="employee-advocacy-st2" d="M10.2,13c0,0.7-0.6,1.2-1.2,1.2S7.8,13.7,7.8,13s0.6-1.2,1.2-1.2S10.2,12.3,10.2,13z M15,11.8
												c-0.7,0-1.2,0.6-1.2,1.2s0.6,1.2,1.2,1.2s1.2-0.6,1.2-1.2S15.7,11.8,15,11.8z M22,12c0,5.5-4.5,10-10,10S2,17.5,2,12S6.5,2,12,2
												S22,6.5,22,12z M10.7,4.1C12.1,6.4,14.6,8,17.5,8c0.5,0,0.9-0.1,1.3-0.1C17.4,5.6,14.9,4,12,4C11.5,4,11.1,4.1,10.7,4.1z M4.4,9.5
												c1.7-1,3-2.6,3.7-4.4C6.4,6,5.1,7.6,4.4,9.5z M20,12c0-0.8-0.1-1.5-0.3-2.2C19,9.9,18.2,10,17.5,10c-3.1,0-5.9-1.4-7.8-3.7
												C8.7,8.9,6.6,10.9,4,11.9c0,0,0,0.1,0,0.1c0,4.4,3.6,8,8,8S20,16.4,20,12z"/>
											</svg> 
											</span>
											Employee advocacy
										</h3>
										<p class="feature_descr">Activate employee brand advocates. </p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/employee-advocacy" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li> -->

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/features_media.svg');?> 
											</span>
											Media editors
										</h3>
										<p class="feature_descr">Edit and optimise photos and videos.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/media-editors" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/features_feeds.svg');?> 
											</span>
											Asset Management
										</h3>
										<p class="feature_descr">End to end media asset management.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/asset-management" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/icons/moderation.svg');?> 
											</span>
											Moderation
										</h3>
										<p class="feature_descr">Brand governance and compliance.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/moderation" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>
								<?php /*
								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
											
												<?php include('images/icons/Employee-Referrals.svg');?>
											</span>
											Employee referrals
										</h3>
										<p class="feature_descr">Drive more employee referrals.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/employee-referrals" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>
								

								<li class="feature_li">
									<div class="feature">
										<h3 class="feature_title">
											<span class="icon">
												<?php include('images/icons/widgets.svg');?> 
											</span>
											Social Widgets
										</h3>
										<p class="feature_descr">Embed social widgets into web assets.</p>
										<div class="learn_more">
											<a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/social-widgets" title="Learn more">Learn more 
												<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
												<path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
												<circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
											</svg></a>
										</div>
									</div>
								</li>

								*/ ?>

							</ul>
						</div>
				</div>
			</div>
		</section>
		<!--  Features overview section ends -->

	<?php include($rootPath.'/inc/feature-blocks.php') ?>
		
	</div>
</div>

<?php
get_footer(); ?>
