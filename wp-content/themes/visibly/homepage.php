<?php
  /**
  * Template Name: Homepage
  *
  * @package WordPress
  * @subpackage Twenty_Fourteen
  * @since Twenty Fourteen 1.0
  */
  /**
  * Template part for displaying page content in page.php
  *
  * @link https://codex.wordpress.org/Template_Hierarchy
  *
  * @package WordPress
  * @subpackage Twenty_Seventeen
  * @since 1.0
  * @version 1.0
  */
  get_header();
   
  //   while ( have_posts() ) : the_post();
  //     the_content();
  //  endwhile;
?>
<div class="homepageWrapper">
  
  <div class="homePageBanner">
    <div class="container">
      <!-- Banner Starts - ->
      <div class="mainBannerCaptionWrapper">
        <div class="shape green shape1"></div>
        <div class="shape yellow shape2"></div>
        <div class="shape primary shape3"></div>
        <div class="mainBannerCaptionInner">
          <h1 class="hero-banner-title"><span class="first-line"><span class="square_two"></span>Say 'goodbye'... to disjointed culture programs</span></h1>
          <p class="sub_txt">Visibly’s all-in-one culture alignment platform supports HR, IC and Marketing
in nurturing workforce behaviours for competitive advantage.</p>
          <a href="http://localhost/visibly/request-a-demo-by-mail/" class="request-demo-btn" title="Request Demo">Request Demo</a>
        </div>

      </div>
      <!- - Banner Ends -->

      


      <!-- Video Section Starts - ->

      <div class="video_section video_section_top">
        <div class="video_inner flex_view_xs middle center">
          <div class="img_section">
            <div class="top_img">
              <img src="<?php //echo get_template_directory_uri(); ?>/images/video_top_img.png" alt="video_top_image">
            </div>



            <script src="https://fast.wistia.com/embed/medias/ezdejx7cwg.jsonp" async></script>
            <script src="https://fast.wistia.com/assets/external/E-v1.js" async></script>

            <div class="video_frame wistia_responsive_padding">
        

<script src="https://fast.wistia.com/embed/medias/ezdejx7cwg.jsonp" async></script>
              <script src="https://fast.wistia.com/assets/external/E-v1.js" async></script>
              <div class="wistia_responsive_padding" style="padding: 57.25% 0 0 0; position: relative;">
                <div class="wistia_responsive_wrapper" style="height: 100%; left: 0; position: absolute; top: 0; width: 100%;"><span class="wistia_embed wistia_async_ezdejx7cwg popover=true popoverAnimateThumbnail=true videoFoam=true" style="display: inline-block; height: 100%; position: relative; width: 100%;">.</span>
                </div>
              </div>
              
            </div>
          </div>


          <div class="text_section">
            <div class="text_wrapper">
              <p class="para para_one">
                In a hyper-connected and rapidly changing world, where most people consume based on word of mouth, organisations need joined up data and tools to align themselves with today’s consumer expectations.
              </p>
              <p class="para para_two">
                Visibly consolidates <span class="hl">management, employee</span> and <span class="hl">customer</span> data, providing businesses with a unique set of <span class="hl">insights</span> and <span class="hl">tools</span> to tackle today's unique business challenges.
              </p>

              <div class="btn_div">
                <a href="http://localhost/visibly/request-a-demo-by-mail/" class="request-demo-btn" title="Request Demo">Request Demo</a>

              </div>
            </div>
          </div>
        </div>
      </div>

      <! -- Video Section Ends -->

<!-- New hero section starts -->
  <div class="animated-hero-section clearfix">
    <div class="animate-hero-content">
        <div class="type-wrap">
          <?php 
          $main_title_sentence = CFS()->get( 'hero_main_title_sentence' );
          $main_title_words = CFS()->get( 'hero_main_title_words' );
          $hero_sub_title = CFS()->get( 'hero_sub_title' );
          $main_title_string = '';
          if(count($main_title_words) > 0){
          $string_count = 1;
             foreach ( $main_title_words as $main_title_word ) { 
              if($string_count < count($main_title_words)){
              $main_title_string .= '"'.$main_title_word['hero_main_title_word'].'",';
              }
              else{
               $main_title_string .= '"'.$main_title_word['hero_main_title_word'].'"'; 
              }
              $string_count++;
            }
          }
            ?>
            
          <!-- Keep your home<br>workforce -->
        <h1><?php echo $main_title_sentence; ?></h1>
        <div class="text_section">
          <div class="text_wrapper">
           <p class="para para_one"><?php echo $hero_sub_title; ?></p>
        </div>
      </div>
        <div class="btn_div">
          <a href="<?php echo get_home_url(); ?>/request-a-demo-by-mail/" class="request-demo-btn" title="Request Demo">Request Demo</a>
          <a href="<?php echo get_home_url(); ?>/request-trial/" class="free-trial-link">Launch free trial</a>

        </div>
        </div>
      </div>
      <?php if(CFS()->get( 'hero_main_image' ) !== '') { ?>
      <div class="animated-hero-image-col">
      <img src="<?php echo CFS()->get( 'hero_main_image' ); ?>" alt="Visibly">
    </div>
    <?php } ?>
  </div>
</div>



  </div>


  <div class="unique-challenges-section section">
    <h2><?php the_field('homepage_banner_bottom_title'); ?></h2>
    <div><?php the_field('homepage_banner_bottom_description'); ?></div>
    <div class="homepage-banner_bottom">
       <?php /* <img src="<?php the_field('homepage_banner_bottom_image') ?>" alt="<?php the_field('homepage_banner_bottom_title'); ?>"> 

      <script src="https://fast.wistia.com/embed/medias/<?php the_field('homepage_banner_bottom_video_id'); ?>.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><span class="wistia_embed wistia_async_<?php the_field('homepage_banner_bottom_video_id'); ?> popover=true popoverAnimateThumbnail=true"> </span> */ ?>
      <iframe src="https://player.vimeo.com/video/<?php the_field('homepage_banner_bottom_video_id'); ?>" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
    </div>
  </div>

<!-- New hero section ends -->

    
<?php include($rootPath.'/inc/feature-blocks.php') ?>
<div class="container">
  <div class="home-why-visibly-block">
    <div class="home-why-visibly-title"><?php the_field('home-why-visibly-title') ?></div>
    <div class="home-why-visibly-content"><?php the_field('home-why-visibly-content') ?></div>
  </div>
</div>
  <!-- Measure Section Starts -->
  <div id="measure_section" class="section">
    <div class="container">

      <div class="inner_wrap">
        <div class="measure flex_parent flex_view_xs middle">
          <div class="image_section">
            <div class="image img clearfix">
               <!-- <video width="320" height="240" muted loop="true" autoplay="true">
                <source src="<?php //echo CFS()->get('homepage_monitor_video'); ?>" type="video/mp4">
                Your browser does not support the video tag.
              </video> -->
              <div class="three-screenshot-col heatmap-image"><img src="<?php echo CFS()->get('homepage_monitor_image'); ?>">
                <div id="green-box" class="green-box">7.5</div>
              </div>
              
              <div class="three-screenshot-col survey-image"><img src="<?php echo CFS()->get('homepage_monitor_image_2'); ?>"></div>
              <div class="three-screenshot-col pulse-image">
                <div class="pulse-box">
                  <div class="variation-box-wrapper"><span id="variation-box">7.5</span>%</div>
                  <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 183.68 78"><title>path</title><polyline points="0.61 48.61 4.4 51.51 34.4 3.2 36.29 34.46 43.24 27.36 53.34 41.72 67.56 44.25 95.03 14.57 107.03 25.3 122.82 15.2 131.98 32.25 164.09 21.06" /><line x1="162.85" y1="20.43" x2="182.73" y2="77.67"/></svg>
                  <div class="pulse-date">
                    <div>Change since</div>
                    <div>Apr 14th</div>
                  </div>
               </div>
              </div>
            </div>
          </div>

          <div class="text_section">

            <div class="text_wrap">
              
              <div class="icon_div">
                <span class="icon"><img src="<?php echo CFS()->get('homepage_monitor_icon'); ?>"></span>
              </div>

              <h3 class="section_title"><?php echo CFS()->get('homepage_monitor_title'); ?></h3>

              <p class="section_descr"><?php echo CFS()->get('homepage_monitor_description'); ?>
              </p>

              <div class="btn_div">
                <a href="<?php echo get_home_url() ?>/request-a-demo-by-mail/" class="btn_link request-demo-btn green-button">Request Demo</a>
                <div class="learn_more"> 
                  <a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/listen" title="Learn more">Learn more 
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                    <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
                    <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
                  </svg>
                </a>
              </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>

  </div>
  <!-- Measure Section Ends -->

  <!-- Implementation Section Starts -->
  <!-- <div id="implementation_section_main">

    <div class="container">

      <div class="inner_wrap">
    <div class="implement flex_parent flex_view_xs middle">
      <div class="image_section">
          <div class="image_div img">
              <img src="<?php echo CFS()->get('homepage_implementation_image'); ?>" alt="implementation_image">
            </div>
        </div>
      <div class="text_section">
        <div class="text_wrap">
          <div class="icon_div">
            <span class="icon"><img src="<?php echo CFS()->get('homepage_implementation_icon'); ?>"></span>
          </div>

          <h3 class="section_title"><?php echo CFS()->get( 'homepage_implementation_title' ); ?></h3>

          <div class="descr_div">
            <p class="section_descr"><?php echo CFS()->get( 'homepage_implementation_description' ); ?></p>

            

          </div>
          <div class="btn_div">
            <a href="<?php echo get_home_url(); ?>/request-a-demo-by-mail/" class="request-demo-btn" title="Request Demo">Request Demo</a>
          </div>
        </div>
      </div>
      
</div>

<div class="step_column_wrapper">
        <div class="steps_grid flex_view_xs middle center">

          <div class="step_column step_one">

            <div class="step_parent flex_view_xs middle center">
              <div class="logo_section flex_view_xs middle center">
                <?php include_once('images/svg_icon_strategy.svg');?>
              </div>

              <div class="text_section">
                <h3 class="step_title">Strategy</h3>
                <p class="step_descr">Define strategic objectives for culture transformation.</p>
              </div>
            </div>    
           
          </div>

          <div class="step_column step_two">

            <div class="step_parent flex_view_xs middle center">
              <div class="logo_section flex_view_xs middle center">
                <?php include_once('images/svg_icon_brand.svg');?>
              </div>

              <div class="text_section">
                <h3 class="step_title">Brand</h3>
                <p class="step_descr">We create brand templates to ensure total consistency.</p>
              </div>
            </div>    
           
          </div>

          <div class="step_column step_three">

            <div class="step_parent flex_view_xs middle center">
              <div class="logo_section flex_view_xs middle center">
                <?php include_once('images/svg_icon_values.svg');?>
              </div>

              <div class="text_section">
                <h3 class="step_title">Values</h3>
                <p class="step_descr">We use AI to suggest and measure your company values.</p>
              </div>
            </div>    
           
          </div>

          <div class="step_column step_four">

            <div class="step_parent flex_view_xs middle center">
              <div class="logo_section flex_view_xs middle center">
                <?php include_once('images/svg_icon_consultancy.svg');?>
              </div>

              <div class="text_section">
                <h3 class="step_title">Consultancy</h3>
                <p class="step_descr">Our consultancy arm works with you every step of the way.</p>
              </div>
            </div>    
           
          </div>

        </div>  
</div>
      </div>
      
    </div>
  </div> -->
  <!-- Implementation Section Ends-->

  <!-- Communicate Section Starts -->
  <div id="commmunicate_section">
    <div class="container">

      <div class="inner_wrap">
        <div class="communicate flex_parent flex_view_xs middle">
          <div class="image_section">
            <div class="image img clearfix">
              <!-- <video width="320" height="240" muted loop="true" autoplay="true">
              <source src="<?php echo CFS()->get('homepage_communicate_video'); ?>" type="video/mp4">
              Your browser does not support the video tag.
            </video> -->
            <div class="chat-anim-block">
            <div class="chat-anim-chat-area"></div>
            <div class="chat-other-area"></div>
        </div>    
            </div>
            <div class="hcepImageWrapper">
              <picture>
                <source srcset="<?php echo get_template_directory_uri() ?>/videos/shadow-mockup-new.png" media="(max-width: 767px)">
                <source srcset="<?php echo get_template_directory_uri() ?>/videos/shadow-mockup-new@2x.png" media="(-webkit-min-device-pixel-ratio: 1.5)">
                <source srcset="<?php echo get_template_directory_uri() ?>/videos/shadow-mockup-new.png" media="(min-width: 768px)">
                    <img src="<?php echo get_template_directory_uri() ?>/videos/shadow-mockup-new.png" alt="Internal communications that reach and engage every employee" title="Internal communications that reach and engage every employee">
              </picture>
              <video autoplay="autoplay" loop="loop" muted="" width="304" height="540">
                <source src="<?php echo CFS()->get('homepage_communicate_video'); ?>" type="video/mp4"><source src="movie.ogg" type="video/ogg">Your browser does not support the video tag.</video>
            </div>
          </div>

          <div class="text_section">

            <div class="text_wrap">
              
              <div class="icon_div">
                <span class="icon"><img src="<?php echo CFS()->get('homepage_communicate_icon'); ?>"></span>
              </div>

              <h3 class="section_title"><?php echo CFS()->get('homepage_communicate_title'); ?></h3>

              <p class="section_descr"><?php echo CFS()->get('homepage_communicate_description'); ?>
              </p>

              <div class="btn_div">
                <a href="<?php echo get_home_url() ?>/why-visibly" class="btn_link request-demo-btn">Why Visibly</a>
                <div class="learn_more"> 
                  <a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/communicate" title="Learn more">Learn more 
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                    <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
                    <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
                  </svg>
                </a>
              </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>

  </div>
  <!-- Communicate Section Ends -->
  

  <!-- Expert Video Section Starts -->
  <!-- <div id="expert_video_section_main">
    <div class="container">
      <div class="inner_wrap">

        <div class="parent_div flex_view middle space-between">

          <div class="text_div">
            <h3 class="section_title">Dedicated experts giving best practice advice.</h3>
            <p class="section_descr">Our culture and communication Consultants support you all the way through the program.</p>

            <div class="section_link"><a class="link_with_arrow" href="<?php echo get_home_url(); ?>/customer-success" title="Learn more">Learn more 
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
                <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
              </svg></a></div>
          </div>

          <div class="video_section">
            <div id="visbly-web-video" class="banner-video-wrapper">
              <script src="https://fast.wistia.com/embed/medias/f8bn2yefa5.jsonp" async></script>
              <script src="https://fast.wistia.com/assets/external/E-v1.js" async></script>
              <div class="wistia_responsive_padding" style="padding: 57.25% 0 0 0; position: relative;">
                <div class="wistia_responsive_wrapper" style="height: 100%; left: 0; position: absolute; top: 0; width: 100%;"><span class="wistia_embed wistia_async_f8bn2yefa5 popover=true popoverAnimateThumbnail=true videoFoam=true" style="display: inline-block; height: 100%; position: relative; width: 100%;">.</span>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div> -->
  <!-- Expert Video Section Ends -->


  


  <!-- Engage Section Starts -->
  <div id="engage_section" class="section">
    <div class="container">

      <div class="inner_wrap">
        <div class="engage flex_parent flex_view_xs middle">
          <div class="image_section">
            <div class="image img clearfix">
             <?php /* <video width="320" height="240" muted loop="true" autoplay="true">
              <source src="<?php echo CFS()->get('homepage_engage_video'); ?>" type="video/mp4">
              Your browser does not support the video tag.
            </video> */ ?>
              <div class="three-screenshot-col"><img src="<?php echo CFS()->get('homepage_engage_image'); ?>"></div>
              <div class="three-screenshot-col"><img src="<?php echo CFS()->get('homepage_engage_image_2'); ?>"></div>
              <div class="three-screenshot-col"><img src="<?php echo CFS()->get('homepage_engage_image_3'); ?>"></div>
              
            </div>
          </div>

          <div class="text_section">

            <div class="text_wrap">
              
              <div class="icon_div">
                <span class="icon"><img src="<?php echo CFS()->get('homepage_engage_icon'); ?>"></span>
              </div>

              <h3 class="section_title"><?php echo CFS()->get('homepage_engage_title'); ?></h3>

              <p class="section_descr"><?php echo CFS()->get('homepage_engage_description'); ?>
              </p>

              <div class="btn_div">
                <a href="<?php echo get_home_url() ?>/request-a-demo-by-mail" class="btn_link request-demo-btn theme-button">Let's Go</a>
                <div class="learn_more"> 
                  <a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/engage" title="Learn more">Learn more 
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                    <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
                    <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
                  </svg>
                </a>
              </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>

  </div>
  <!-- Engage Section Ends -->


  

  

  <!-- Advocate Section Starts -->
  <!-- <div id="advocate_section">
    <div class="container">

      <div class="inner_wrap">
        <div class="advocate flex_parent flex_view_xs middle">

          <div class="image_section">
            <div class="image img clearfix">

            <div class="hcepImageWrapper">
              <video autoplay="autoplay" loop="loop" muted="" width="304" height="540">
                <source src="<?php echo CFS()->get('homepage_advocate_video'); ?>" type="video/mp4"><source src="movie.ogg" type="video/ogg">Your browser does not support the video tag.</video>
                <picture>
                <source srcset="<?php echo get_template_directory_uri() ?>/videos/shadow-mockup-new.png" media="(max-width: 767px)">
                <source srcset="<?php echo get_template_directory_uri() ?>/videos/shadow-mockup-new.png" media="(-webkit-min-device-pixel-ratio: 1.5)">
                <source srcset="<?php echo get_template_directory_uri() ?>/videos/shadow-mockup-new.png" media="(min-width: 768px)">
                    <img src="<?php echo get_template_directory_uri() ?>/videos/shadow-mockup-new.png" alt="Internal communications that reach and engage every employee" title="Internal communications that reach and engage every employee">
              </picture></div>
            </div>
          </div>

          <div class="text_section">

            <div class="text_wrap">
              
              <div class="icon_div">
                <span class="icon"><img src="<?php echo CFS()->get('homepage_advocate_icon'); ?>"></span>
              </div>

              <h3 class="section_title"><?php echo CFS()->get('homepage_advocate_title'); ?></h3>

              <p class="section_descr"><?php echo CFS()->get('homepage_advocate_description'); ?></p>

              <div class="btn_div">
                <a href="<?php echo get_home_url() ?>/features" class="btn_link request-demo-btn yellow-button">See Platform</a>
                <div class="learn_more"> 
                  <a class="link_with_arrow" href="<?php echo get_home_url() ?>/features/advocacy" title="Learn more">Learn more 
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                    <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
                    <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
                  </svg>
                </a>
              </div>
              </div>
            </div>

          </div>


        </div>

        <div class="bottom_txt">
          <p class="txt">Culture transformation is really hard, that’s why 75%+ of programs fail. Research shows that this is mostly due to communication, collaboration or people issues.
          </p>
          <div class="learn_more"> 
                  <a class="link_with_arrow" href="<?php echo get_home_url() ?>/objectives-key-results-how-to-connect-employee-goals-to-business-outcomes/" title="Learn more">Learn more 
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                    <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
                    <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
                  </svg>
                </a>
              </div>
        </div>
      </div>

    </div>

  </div> -->
  <!-- Advocate Section Ends -->

  <!--Expert Section Starts-->
  <div id="expert_section_main">

    <div class="expertsection">

      <div class="container">
        <div class="subcontainer">
          <h2><?php the_field('expert_main_title') ?></h2>
          <div class="subheader"><?php the_field('expert_sub_title') ?></div>

          <div class="col-container clearfix">
           

              
            <div class="col">
              <div class="expertsectionSVG">
                <img src="<?php the_field('expert_podcast_icon') ?>">
              </div>
              <div class="colheader"><h3><?php echo the_field('expert_podcast_title'); ?></h3></div>
              <div class="expert-section-content"><?php the_field('expert_podcast_content') ?></div>
              
              <div class="expertsectionlink">
                <a class="link_with_arrow" href="<?php the_field('expert_podcast_post'); ?>">Listen now
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                  <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
                  <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
                </svg></a>
              </div>
            </div>

            <div class="col">
              <div class="expertsectionSVG">
                <img src="<?php the_field('expert_feature_blog_icon') ?>">
              </div>
              
              <div class="colheader"><h3><?php  the_field('expert_feature_blog_title'); ?></h3></div>
              <div class="expert-section-content"><?php the_field('expert_featured_blog_content') ?></div>
              <div class="expertsectionlink">
                <a class="link_with_arrow" href="<?php the_field('expert_feature_blog'); ?>">Read now
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                  <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
                  <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
                </svg></a>
              </div>
            </div>

            <div class="col">
              <div class="expertsectionSVG">
                <img src="<?php the_field('expert_download_ebook_icon') ?>">
              </div>
              
              <div class="colheader"><h3><?php echo the_field('expert_download_ebook_title'); ?></h3></div>
              <div class="expert-section-content"><?php the_field('expert_download_ebook_content') ?></div>
              
              <div class="expertsectionlink">
                <a target="_blank" class="link_with_arrow" href="<?php the_field('expert_download_ebook') ?>">Read now
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
                  <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
                  <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
                </svg></a>
              </div>
            </div>

           
           
            

          </div>
        </div>
      </div>

    </div>
    
  </div>
  <!--Expert Section Ends-->
  

  <?php //include($rootPath.'/inc/take-survey.php'); ?>


  <!-- Contact Section Starts -->
  <div id="contact_section" class="form-section">
    <div class="container">

      <div class="inner_wrap">
        <div class="title">
          <p class="title_txt"><?php echo CFS()->get('homepage_contact_title') ?></p>
        </div>

        <div class="contact_form form-wrapper">
          <?php dynamic_sidebar('contact-section') ?>
          <?php //echo do_shortcode('[contact-form-7 id="2807" title="Footer Contact Form"]') ?>
        </div>  
      </div>
      
    </div>
    
  </div>
  <!-- Contact Section Ends -->



</div>

<?php get_footer();