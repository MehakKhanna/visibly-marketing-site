<?php
/**
 * Template Name: Press
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
 
?>
<h1 id="h1_title"><?php the_title(); ?></h1>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/isotope.pkgd.min.js"></script>
   <!-- Main wrapper starts  -->
        <div class='mainWrapper blogMainWrapper'>
           
            <!-- blogHeroSection starts -->

           
 <?php

$args = array(
            'post_type'        => 'press-feed',
            'post_status'      => 'publish',
            'order' => 'ASC',
            'posts_per_page' => 1,
        );

                 
 $sticky_posts_array = get_posts( $args );
        
           
        foreach ($sticky_posts_array as $post) {
            if(get_post_meta($post->ID, 'wpcf-sticky-press', true))
        { 
         ?>
        <div class="blogHeroSection">
       <?php  if ( has_post_thumbnail() ) {?>
        <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="wp-post-image" alt="<?php echo $post->post_title; ?>" title="<?php echo $post->post_title; ?>">
                                        <?php 
                                    } ?>
                <div class="blogHeroCaption">
                    <div class="featuredArticleTitle"><span>Featured Article</span></div>
                    <div class="blogHeroPostTitle"><span><?php echo $post->post_title; ?></span></div>
                </div>
            </div>
                         
            <?php }
        } ?>

            <!-- blogHeroSection ends -->
           
           
            <!--blogListSection starts-->
            <div class="blogListSection">
            <div class="container">

                <!-- bloglistWrapper starts -->
                <div class="bloglistWrapper">

<?php

$args1 = array(
            'post_type'        => 'press-feed',
            'post_status'      => 'publish',
             'orderby' => 'date',
            'order' => 'DESC',
            'posts_per_page' => -1,
        );

                 
 $posts_array = get_posts( $args1 );
        
        if( sizeof($posts_array) != 0 )
        {    
        foreach ($posts_array as $post) { 
                $categories = get_the_category();
         ?>
       


            <!-- postBlock starts -->
                <div class="postBlock all">
                    <div class="postBlockInner">
                        <div class="postImage">
                            
                            <?php 
                                $external_link = get_post_meta($post->ID, 'wpcf-press-external-link', true); 
                                $pdf_link = get_post_meta($post->ID, 'wpcf-press-upload-pdf-file', true); 
                            ?>
                            <a <?php if($external_link!='') { echo 'target="_blank"'; } ?> href="<?php if($external_link=='') { echo get_permalink($post); } else {echo $external_link;} ?>"><?php  if ( has_post_thumbnail() ) {?>
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/blog-thumbnail-holder.png" alt="<?php echo $post->post_title; ?>" title="<?php echo $post->post_title; ?>" class="blog-thumbnail-holder">
                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="wp-post-image" alt="<?php echo $post->post_title; ?>" title="<?php echo $post->post_title; ?>">
                                        <?php 
                                    } ?></a>
                        </div>
                        <div class="postTextWrapper">
                            <?php $editLink = get_edit_post_link(); include('inc/edit-link.php'); ?>
                            <div class="pressTitleWrapper <?php if($pdf_link!='') { echo 'titleWithPDF'; } ?>">
                            <div class="postTitle"><a href="<?php echo get_permalink() ?>" title="<?php echo $post->post_title; ?>"><?php echo $post->post_title; ?></a></div>
                            <?php if($pdf_link!='') {?>
                                <div class="pressPDFLinkWapper"><a href="<?php echo $pdf_link; ?>" title="Download PDF" download><i class="far fa-file-pdf"></i></a></div>
                            <?php } ?>
                            </div>
                            <div class="post-meta-data cf">
                                <div class="authorData">
                                    

                                    <?php include('blocks/author-image.php') ?>

<div class="postAuthorName"><?php echo get_the_author_meta( 'display_name', $post->post_author ); ?></div>
                                </div>
                                <div class="postDate"><?php echo get_the_date() ?></div>
                            </div>
                        </div>



                    </div>    
                </div>    
                <!-- postBlock ends -->
                         
            <?php }
        } ?>


               
                    
    
           
              

                    </div>
                    <!-- bloglistWrapper ends -->
            </div>
            </div>
            <!--blogListSection ends-->
        </div>
        <!-- Main wrapper ends-->

<?php get_footer(); ?>