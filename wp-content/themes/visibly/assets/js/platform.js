$(window).on('load', function () {
	$('.no-touch #fullpage').fullpage({
		scrollingSpeed: 650,
	//	autoScrolling:false,
	easingcss3:'linear',
		css3:false,
		onLeave: function(anchorLink, index, direction){
			console.log('index',index, 'direction',direction)
			$('.section').eq(index-1).addClass('selected');
			if(index==4 && direction=='up'){
					$('body').removeClass('fixed-plane');
			}

			if(index==7 && direction=='up'){
				$('body').addClass('fixed-plane').removeClass('show-plane2');
		}
			
		},
		afterLoad: function(anchorLink, index,direction){

			if(index>4){
				$('body').addClass('fixed-plane');
			}else{
				$('body').removeClass('fixed-plane');
			}

			if(index>7){
				$('body').removeClass('fixed-plane').addClass('show-plane2');
			}
			else{
				$('body').addClass('drop-plane-top').removeClass('show-plane2');
			}
		}
	});

// 	$("body").removeClass("preload");
// 	$("html, body").animate({ scrollTop: 0 }, 100)
// $('.platform-title-wrapper').addClass('active');
// 	$('.create-message-section').addClass('active');

// 	// Scroll down from create,curate,message
// 	var step1to2=9000;
// 	setTimeout(function(){
// 		$('.platformMainWrapper').removeClass('platform-step1').addClass('platform-step2');
// 		$("html, body").animate({ scrollTop: '400px' }, 800)
// 	},step1to2);


// 	// Visible moderate section
// 	var moderate_active = step1to2+1000;
// 	setTimeout(function(){
// 		$('.moderate-message-section').addClass('active')
// 	},moderate_active);

// 	// Scroll down from moderate section
// 	var step2to3 = moderate_active + 7000;
// 	setTimeout(function(){
// 		$('.platformMainWrapper').removeClass('platform-step2').addClass('platform-step3');
// 		$("html, body").animate({ scrollTop: '800px' }, 800)
// 	},step2to3);

// 	// Visible Thanks!! chatbox
// 	var thanks_message_active = step2to3 + 1000;
// 	setTimeout(function(){
// 		$('.thanks-message-section').addClass('active');
// 	},thanks_message_active);

// 	// Scroll down from Thanks!! chatbox
// 	var step3to4 = thanks_message_active + 3000;
// 	setTimeout(function(){
// 		$('.platformMainWrapper').removeClass('platform-step3').addClass('platform-step4');
// 		$("html, body").animate({ scrollTop: '1200px' }, 800)
// 	},step3to4);

// 	// Visible publish section
// 	var publish_share_active = step3to4 + 1000;
// 	setTimeout(function(){
// 		$('.publish-message-section').addClass('active');
// 	},publish_share_active);
	
// 	// Scroll down from publish section
// 	var step4to5 = publish_share_active + 5000;
// 	setTimeout(function(){
// 		$('body').addClass('plane-sticky');
// 		$('.platformMainWrapper').removeClass('platform-step4').addClass('platform-step5');
// 		$("html, body").animate({ scrollTop: '1500px' }, 800)
// 	},step4to5);

// 	// Visible about photo section
// 	var about_photo_block_active = step4to5 + 1000;
// 	setTimeout(function(){
// 		$('.about-photo-block').addClass('active');
// 	},about_photo_block_active);

// 	// Scroll down from about photo section
// 	var step5to6 = about_photo_block_active + 5000;
// 	setTimeout(function(){
// 		$('.platformMainWrapper').removeClass('platform-step5').addClass('platform-step6');
// 		$("html, body").animate({ scrollTop: '2100px' }, 800)
// 	},step5to6);

// 	// Visible feedback section
// 	var feed_section_active = step5to6 + 1000;
// 	setTimeout(function(){
// 		$('.feedback-section').addClass('active');
// 	},feed_section_active);

// 	//Scroll down from feedback section
// 	var step6to7 = feed_section_active + 10000
// 	setTimeout(function(){
// 		$('.platformMainWrapper').removeClass('platform-step6').addClass('platform-step7');
// 		$("html, body").animate({ scrollTop: '2300px' }, 800)
// 	},step6to7);

// 	// Visible measure section
// 	var measure_section_active = step6to7;
// 	setTimeout(function(){
// 		$('.measure-section').addClass('active');
// 	},measure_section_active);

// 	// Scroll down from measure section
// 	var step7to8 = measure_section_active + 4000;
// 	setTimeout(function(){
// 		$('.platformMainWrapper').removeClass('platform-step7').addClass('platform-step8');
// 		$("html, body").animate({ scrollTop: '2500px' }, 800)
// 	},step7to8);

// 	// Visible platform social section
// 	var platform_social_section_active = step7to8 + 2000;
// 	setTimeout(function(){
// 		$('.platform-social-wrapper').addClass('active');
// 	},platform_social_section_active);

// 	// Visible no sweat section
// 	var platform_no_sweat_block_section_active = platform_social_section_active + 4000;
// 	setTimeout(function(){
// 		$('.no-sweat-block').addClass('active');
// 	},platform_no_sweat_block_section_active);
	
// 	// Visible no sweat text
// 	var platform_no_sweat_text_block_section_active = platform_no_sweat_block_section_active + 4000;
// 	setTimeout(function(){
// 		$('.platform-no-sweat-text-block').addClass('active');
// 	},platform_no_sweat_text_block_section_active);

});