function headerSticky(){

}
function setHomeFeatureBlockMargin(){
    $('.homeFeatureSection').each(function(){
        var container = $(this).find('.container');
        var homefeatureBlockMargin = $(window).height()-container.height()-386;
        if(homefeatureBlockMargin>0){
            homefeatureBlockMargin = homefeatureBlockMargin/2;
        }else{
            homefeatureBlockMargin = 0;
        }
        container.css('margin-top',homefeatureBlockMargin);
    });
    
}

function ValidateEmailId(inputText) 
{
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var inputValue = $(inputText).val(); 
    if(inputValue.match(mailformat))
    {
        return true;
    }
    //inputText.val('');
        return (false);
}


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var country = getUrlParameter('country');

function customContactFormCheckbox(){
    var contact_form = $("#contact_section .contact_form");
    if($(contact_form).length > 0){

        $(contact_form).find(".checkbox_field input[type=checkbox]").wrap("<label class='custom_container'></label>");
        $(contact_form).find(".checkbox_field .custom_container").append('<span class="checkmark"></span>');
    }

}

function blogCustomSelect(){
    var custom_select = $("#blog_listing_page .articles_section .custom_select, .blogListSection .articles_section .custom_select");
    if(custom_select.length > 0){
        $(custom_select).find(".custom_select_trigger").click(function(){
            $(this).toggleClass("show_options");
            $(this).next().stop().slideToggle();
        });


        $(custom_select).find(".custom_option_box .custom_option").click(function(){
         
           
            $(this).closest(".custom_option_box").slideUp();
            $(".custom_option_box .custom_option").removeClass("selected");
            $(".custom_select_trigger").removeClass('show_options');
            $(this).addClass("selected");
        
            $('.blog_main_wrap .article').unwrap();

           
            	

           
            var selected_option_attr = $(this).attr("data-value");
            var selected_option = $(this).text();

            $(this).closest(".category_div").find(".current_cat").text(selected_option);
                
            
           
            var all_articles = $("#blog_listing_page .article_list .article_row .article, .blogListSection .article_list .article_row .article");
            
           
            $(all_articles).stop().fadeOut();
            $(all_articles).removeClass('selected');

            $(all_articles).each(function(){
            
               
                if($(this).hasClass(selected_option_attr)){
                  console.log($(this))
                    $(this).stop().fadeIn();
                    $(this).addClass('selected');
                   
                }
            });

                console.log($(this));
               var blogselectdetail = "#blog_listing_page .article.select-all.selected";
               console.log(blogselectdetail);
               for(var i = 0; i < $(blogselectdetail).length; i+=2) {
                   $(blogselectdetail).slice(i, i+2).wrapAll("<div class='blog_main_wrap'></div>");
               } 
           
        });
    }
}




/* Add animation accroding to element position */
var textanimFlag=true;
function addActive(){

    $('.animate_parent').each(function(){

        if($(this).hasClass('feedback-section')){
            if($(window).scrollTop() + $(window).height() / 4 > $(this).offset().top-100){
                $(this).addClass('active');
            }
        }else
        if($(this).hasClass('sky-dive-section')){
            if($(window).scrollTop() + $(window).height() / 4 > $(this).position().top+300){
                $(this).addClass('active');
            }
        }else{
            if($(window).scrollTop() + $(window).height() / 4 > $(this).position().top){
                $(this).addClass('active');
            }
        }

    })
}

function checkInput (element){
    var regName = /^[a-zA-Z ]*$/;
    $(element).removeClass('error-input');
    var x = $(element).val();
    if (x == null || x == "") {
     // $(element).addClass('error-input');
      return true;
    } else {
      if(!regName.test(x)){
          console.log('element')
        $(element).addClass('error-input');
        return false;
      }else{
        return true;
      }
    }
  }
  function checkPhone (element){
    var phoneno = /^\d{10}$/;
    $(element).removeClass('error-input');
    var x = $(element).val();
    if (x == null || x == "") {
        $(element).addClass('error-input');
        return false;
    } else {
      if(!phoneno.test(x)){
        $(element).addClass('error-input');
        return false;
      }
    }
    return true;
  }

function ValidateEmail(inputText)
{
    emailValidFlag = ValidateEmailId($(inputText));
    if(emailValidFlag==false)
    {
        $(inputText).closest('.emailform-wrapper').addClass('error');
        setTimeout(function(){
            $('.emailform-wrapper').removeClass('error success');
        },5000);
        $(inputText).closest('.emailform-wrapper').find('.email-popup-text').html('You have entered an invalid email address!');
        $('.signupForm').removeClass('loading');
        inputText.focus();
        return false;
    }
    else {
        return true;
    }
}




var TxtType = function(el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = parseInt(period, 10) || 2000;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
};

function menuHover(){
$('.submenu_parent_li').hover(function(){
    $('.menu_overlay').addClass('show');
},function(){
    $('.menu_overlay').removeClass('show');
})
}

function hideCurrentFeature(){
    var url = window.location.href;
    $('.page-template-template1-features-detail .feature_li,  .page-template-usescase-template1  .feature_li').each(function(){
        var link1 = $(this).find('a').attr('href');
        var link2 = $(this).find('a').attr('href')+'/';
        if(link1!=url && link2!=url){
            $(this).addClass('show');
        }
    })
}

function animateValue(id, start, end, duration) {
    var range = end - start;
    var current = start;
    var increment = end > start? 1 : -1;
    var stepTime = Math.abs(Math.floor(duration / range));
    var obj = document.getElementById(id);
    var timer = setInterval(function() {
        current += increment;
        //obj.innerHTML = current;
        $('#variation-box, #green-box').html(current);
        if (current >= end && increment==1) {
            clearInterval(timer);
        }else if(current <= end && increment==-1){
            clearInterval(timer);
        }
    }, stepTime);
}
var startCount = true;
function addFocusClassOnScroll(element){
    var windowTop = $(window).scrollTop();
    var section = $(element);
    if(section.length>0){
        if(windowTop>section.position().top-$(window).height()/2){
            section.addClass('focused');
            // if(element == '#commmunicate_section'){
            //     setTimeout(function(){
            //         section.addClass('focused1');
            //     },8500)
            // }
            if(startCount){
                animateValue("green-box", 7.5, 2.5, 5000);
                startCount = false;
            }
        }
    }
}


var planId;
$(document).ready(function () {
    var blogdetail = "#blog_listing_page .article.select-all ";
for(var i = 0; i < $(blogdetail).length; i+=2) {
    $(blogdetail).slice(i, i+2).wrapAll("<div class='blog_main_wrap'></div>");
} 
    // Blog Page Custom Select Box
    blogCustomSelect();

    // Footer Contact form Custom Select Box

    customContactFormCheckbox();

    // Add new page
    $('.create').click(function(){
        $.ajax({
            method: "POST",
            url: 'http://localhost/visibly/wp-json/wp/v2/pages',
            data: {
                title: 'Your Desired Page Title',
            //  content: '[my_sample_shortcode]',
                type: 'page',
                status: 'publish',
            },
            beforeSend: function ( xhr ) {
                xhr.setRequestHeader( 'X-WP-Nonce', MS_Ajax.nonce );
            },
            success : function( response ) {
                // Save the page ID in case you need it for something
                var myNewPageID = response.id;
        
            }
        });
    });
    
      // List  page
    
    $('.edit').click(function(){
        const editHttp = new XMLHttpRequest();
        const url='http://localhost/visibly/wp-json/wp/v2/pages/3008';
        editHttp.open("GET", url);
        editHttp.send();
        
        editHttp.onreadystatechange = (e) => {
        console.log(editHttp.responseText)
        }
        
    })

    // Update page
    $('.save').click(function(){
        $.ajax({
            method: "POST",
            url: 'http://localhost/visibly/wp-json/wp/v2/pages/3008',
            data: {
                title: 'Your Desired Page Title',
            //  content: '[my_sample_shortcode]',
            'fields' : { 'hero_title':'update main title 123'},
            type: 'page',
            status: 'publish',
            },
            beforeSend: function ( xhr ) {
                xhr.setRequestHeader( 'X-WP-Nonce', MS_Ajax.nonce );
            },
            success : function( response ) {
                // Save the page ID in case you need it for something
                var myNewPageID = response.id;
        
            }
        });

        })    
    
    

    setHomeFeatureBlockMargin();

    $('.subscribe-popup-close-icon').click(function(){
        $(".subscribeFormMainWrapper").addClass('hide');
        localStorage.setItem('subscribePopupState','shown');
        
    });

    if(localStorage.getItem('subscribePopupState') == 'shown'){
        $('.subscribeFormMainWrapper').addClass('hide');
    }else{
        $('.subscribeFormMainWrapper').removeClass('hide');
    }

    $('.pageWrapper table').wrap('<div class="table-wrap"></div>');
    
    $(".featureMainWrapper .featureTabBtnWrapper").sticky({ topSpacing: 0, className: 'sticky' });
    $(".blogTabbingSectionInner").sticky({ topSpacing: 0, className: 'sticky', bottomSpacing:$('#footer').outerHeight() + 400 });
    $(".platform-plane-image").sticky({ topSpacing: -550, className: 'sticky', bottomSpacing:$('#footer').outerHeight() + 400  });
    $(".blogTabbingSectionInner").parent().height($(".blogTabbingSectionInner").outerHeight());
    $('.country-dropdown').closest('.rminput').addClass('country-dropdown-wrapper');
    $('.country-dropdown').closest('.rminput').prepend('<i class="fa fa-caret-down" />')
    if($('.employeeBuildInfluenceColWrapper').length>0){
        var employeeBuildOwl = $('.employeeBuildInfluenceColWrapper');
        employeeBuildOwl.owlCarousel({
            mouseDrag:false,
            touchDrag:false,
            dots:false,
            autoplay:true,
            loop:true,
            smartSpeed:800,
            autoplayTimeout:3000,
            responsive:{
                0:{
                    items:1
                },
                568:{
                    items:2
                },
                1024:{
                    items:3
                },
                1300:{
                    items:3
                }
            }
        });
    }
    if($('.employeeBuildSlider').length>0){
        var employeeBuildTextOwl =  $('.employeeBuildSlider');
        employeeBuildTextOwl.owlCarousel({
            mouseDrag:false,
            touchDrag:false,
            dots:false,
            autoplay:true,
            loop:true,
            smartSpeed:800,
            autoplayTimeout:3000,
            items:1
        });
    }
$('.employeeBuildIncustomNextBtn').click(function() {
    employeeBuildOwl.trigger('next.owl.carousel');
    employeeBuildTextOwl.trigger('next.owl.carousel');
})
// Go to the previous item
$('.employeeBuildIncustomPrevBtn').click(function() {
    employeeBuildOwl.trigger('prev.owl.carousel');
    employeeBuildTextOwl.trigger('prev.owl.carousel');
})

if($('.featureCampaingImageSlider').length>0){
    $('.featureCampaingImageSlider').owlCarousel({
        nav:false,
        dots:true,
        autoplay:true,
        smartSpeed:800,
        autoplayHoverPause:true,
        autoplayTimeout:3000,
        loop:true,
        items:1
    });
}
$('.menu-handle-wrapper').click(function(){
    $('body').toggleClass('show-menu');
});
$('.close-menu').click(function(){
    $('body').removeClass('show-menu');
});


var elements = $('.typewrite');
TxtType.prototype.tick = function () {
    var i = this.loopNum % this.toRotate.length;
    var fullTxt = this.toRotate[i];
  
    if (this.isDeleting) {
      this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
      this.txt = fullTxt.substring(0, this.txt.length + 1);
    }
  
    this.el.innerHTML = '<span class="wrap">' + this.txt + '</span>';
  
    var that = this;
    var delta = 200 - Math.random() * 100;
  
    if (this.isDeleting) {delta /= 2;}
  
    if (!this.isDeleting && this.txt === fullTxt) {
      delta = this.period;
      this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
      this.isDeleting = false;
      this.loopNum++;
      delta = 500;
    }
    setTimeout(function () {
        that.tick();
      }, delta);
    };
elements.each(function(){
    var toRotate = $(this).attr('data-type');
    var period = $(this).attr('data-period');
    // if (toRotate) {
    //     new TxtType(this, JSON.parse(toRotate), period);
    // }
})
$('.subscribePopupOverlay, .close-subscribe-popup').click(function(){
    $('.subscribePopupOverlay, .subscribePopupWrapper').addClass('hide');
});
$('.featureTabBtn').click(function(){
    $('.featureTabBtn-col').siblings('.featureTabBtn-col').removeClass('active');
    $('.featureTabWrapper').siblings('.featureTabWrapper').removeClass('active');
    $(this).closest('.featureTabBtn-col').addClass('active');
    var selectedId = '#'+$(this).closest('.featureTabBtn-col').attr('data-tab');
    $(selectedId).addClass('active');
    $("html, body").animate({scrollTop: $(selectedId).position().top-200}, 1000);
});

$('.faqTitle').click(function(){
    $(this).closest('.faqBlock').siblings('.faqBlock').removeClass('active');
    $(this).closest('.faqBlock').siblings('.faqBlock').find('.faqContent').slideUp();
    $(this).closest('.faqBlock').toggleClass('active');
    $(this).closest('.faqBlock').find('.faqContent').slideToggle(function(){if($(window).width()<768){
        $("html, body").animate({scrollTop: $(this).closest('.faqBlock').position().top-40}, 1000);
    }});

});

$('.basic-feature-link').click(function(e){
    e.preventDefault();
    $("html, body").animate({ scrollTop: $('.compare-plan-title').position().top - 100 }, "slow");
});

var ip = $('.signupApiData .ip').text();
var country = $('.signupApiData .country').text();
var generalPlan="Freemium";
/* Get detail when click on pricing page > get started button */


/* Send detail to api when click on email button */
$('.signupForm .sign-up-link').click(function(e){
    e.preventDefault();
    $('.signupForm').addClass('loading');
    var selected_email = $(this).closest('.signupForm').find('.email')[0];
    var planId = 'rpPDp';
    sendDataToAPI(selected_email,generalPlan,ip,country, planId, this);
});


function sendDataToAPI(emailInput,plan,ip,country, planId,button){
    $('.emailform-wrapper').removeClass('error success ');

    if(ValidateEmail(emailInput)){
        var email={
            "email":emailInput.value,
            'planId': planId
        };
        emailStringify = JSON.stringify(email);
        $.ajax({
            url: "https://api.visibly.io/api/v1/signup",
            type: "POST",
            data: emailStringify,
            contentType: "application/json",
            success: function (data) {
                var successText = '';
                $(button).closest('.emailform-wrapper').removeClass('loading show-signup');
                $(button).closest('.emailform-wrapper').addClass('success');
                $(button).closest('.emailform-wrapper').find('.email-submit').trigger('click');
                if(data.message!=''){
                    successText = data.message; 
                    $(button).closest('.emailform-wrapper').find('.email-popup-text').html(successText);
                }
                
            },
            error: function (data) {
                $('.signupForm').removeClass('loading');
                $(emailInput).closest('.emailform-wrapper').addClass('error');
                var errorText= '';
                console.log(data.responseJSON.code);
                if(data.responseJSON.error){
                    if(data.responseJSON.error.email){
                        errorText = data.responseJSON.error.email; 
                    }else if(data.responseJSON.error.domain){
                        errorText = data.responseJSON.error.domain; 
                    }
                }
                else if(data.responseJSON.code==500){
                    errorText = data.responseJSON.message; 
                }
                $(emailInput).closest('.emailform-wrapper').find('.email-popup-text').html(errorText);
                setTimeout(function(){
                    $('.emailform-wrapper').removeClass('error success');
                },5000);
            },
            
        });
    }

}


    $('.watchVideoLink').click(function(){
        $('.videoPopupWrapper').addClass('active');
        $('.videoPopupWrapper iframe').attr('src',$(this).attr('data-url'));
    });
    $('.videoOverlay, .closeVideoPopup').click(function(){
        $('.videoPopupWrapper').removeClass('active');
        $('.videoPopupWrapper iframe').attr('src','');
    });
    var demoInputs = '.demoMailForm .um-field input[type="text"], .demoMailForm .um-field input[type="email"], .demoMailForm .um-field input[type="number"], .demoMailForm .um-field textarea';
    var errorClass = 'error-input';
    setTimeout(function(){
        $('.um-field-error').fadeOut(1000)
    },5000)
    
    $('.rde-checksbumit').click(function(){
        var inputSubmitLevel1 = true;
        var inputSubmitLevel2 = false;
        $('.request-demo-hubspotform .name').val($("div[data-key='first_name'] input").val());
        $('.request-demo-hubspotform .email').val($("div[data-key='user_email'] input").val());
        $('.request-demo-hubspotform .positionInOrganisation').val($("div[data-key='position_in_organisation'] input").val());
        $('.request-demo-hubspotform .numberOfEmployees').val($("div[data-key='number_of_emp_dropdown'] select").val());
        $('.request-demo-hubspotform .country').val($("div[data-key='country'] select").val());
        $('.request-demo-hubspotform .contactNumber').val($("div[data-key='phone_number1'] input").val());

        $(demoInputs).each(function(){
            if(!$(this).hasClass('not-required')){
                if($(this).val()==''){
                    inputSubmitLevel1=false;
                    $(this).addClass(errorClass);
                }
            }
            if(inputSubmitLevel1){
                if($(this).attr('data-key')=='user_email'){
                    inputSubmitLevel2 = ValidateEmailId($(this));
                    if(inputSubmitLevel2==false){
                        $(this).addClass(errorClass);
                    }
                }
            }
        });

        var contactName =  checkInput('.india-signupform-wrapper .um-field-first_name input');
        var contactOrganisation = checkInput('.india-signupform-wrapper .um-field-position_in_organisation input');
        var contactPhone = checkPhone('.india-signupform-wrapper .um-field-phone_number1 input');
        console.log(contactName , contactOrganisation , contactPhone)
        
        if(inputSubmitLevel1 && inputSubmitLevel2 && contactName && contactOrganisation && contactPhone){
            console.log('trigger called');
            $('.request-demo-hubspotform .signup-submit').trigger('click');
            $('.demo-by-email-box form').submit();
        }
        
    });
    $("div[data-key='number_of_employees'] input").attr('min',0);
    $("div[data-key='number_of_employees'] input").blur(function(){
        var amount = $(this).val().replace(/\D/g,'');
        $(this).val(amount);
    })

    $(demoInputs).focus(function(){
        $(this).removeClass(errorClass)
    });
    $(demoInputs).blur(function(){
        if($(this).val()==''){
            $(this).addClass(errorClass);
        }
    });

    $('.gallery-item a').fancybox({
        caption : function( instance, item ) {
            var caption = $(this).closest('.gallery-icon').siblings('.wp-caption-text').text() || '';
            console.log($(this),caption, $(this).closest('.gallery-icon').siblings('.wp-caption-text'));
            if ( item.type === 'image' ) {
                caption = (caption.length ? caption : '') ;
            }

            return caption;
        }
    });
 
    $('.sub-menu').wrap('<div class="sub-menu-wrapper"/>');
    $('.sub-menu-wrapper').wrapInner('<div class="sub-menu-wrapper-inner">');
    $('.menu-item-has-children').append('<a href="javascript:void(0);" class="menu-toggle-handler"><svg viewBox="0 0 11 7" xmlns="http://www.w3.org/2000/svg"><path d="M5.371 3.95L1.707.293A1 1 0 0 0 .293 1.708l4.348 4.34a1 1 0 0 0 1.389.023l4.652-4.34A1 1 0 1 0 9.318.27L5.37 3.95z"></path></svg></a>')
    $('.menu-toggle-handler').click(function(){
        $(this).siblings('.sub-menu-wrapper').slideToggle();
        $(this).toggleClass('active');
    });

    var $set = $('.wsp-container').children();   
    var $set_next = 2; 
for(var i=0, len = $set.length; i < len; i+=$set_next){
    $set.slice(i, i+$set_next).wrapAll('<div class="sitemap-col"><div class="sitemap-col-inner"></div></div>');
} 

$(".integration-contact-form-popup").fancybox({
    maxWidth    : 800,
    maxHeight   : 600,
    fitToView   : false,
    width       : '70%',
    height      : '70%',
    autoSize    : false,
    closeClick  : false,
    openEffect  : 'none',
    closeEffect : 'none'
});
   menuHover(); 
   hideCurrentFeature();
   $('.nav-next').closest('.next_article').addClass('show');
});

$(window).load(function(){
    
    if($('.bloglistWrapper').length>0){
        $('.blogTabbingSection li a').click(function(e){
            e.preventDefault();
            $('.blogTabbingSection li a').removeClass('active');
            $(this).addClass('active');
        })
    }
})

$(window).scroll(function(){
    addFocusClassOnScroll('.employeeBuildInfluenceWrapper');
    addFocusClassOnScroll('#engage_section');
    addFocusClassOnScroll('#measure_section');
    addFocusClassOnScroll('#commmunicate_section');

});

$(window).resize(function(){
    $(".blogTabbingSectionInner").parent().height($(".blogTabbingSectionInner").outerHeight());
    setHomeFeatureBlockMargin();
});


