<style>
         * {
         padding: 0;
         margin: 0;
         font-family: "Lato", Helvetica, Arial, sans-serif !important;
         }
         body {
         background-color: #ededed;
         }
      </style>
      <table style="padding: 30px 0; width: 700px; margin: 0px auto;">
         <tbody>
            <tr>
               <td style="text-align: center; padding-bottom: 30px;"><a href="{site_url}"> <img class="alignright size-full wp-image-845" src="https://app.visibly.io/img/logo.png" alt="visibly-logo-1" width="180px" height="auto" /></a></td>
            </tr>
            <tr>
               <td style="border-bottom:3px solid #ff5660;">
                  <table style="margin: 0 auto; background: #fff; padding: 60px; width: 100%;">
                     <tbody>
                        <tr>
                           <td style="color: #1c1c1c;">
                              <h2 style="font-weight: 400; font-size: 33px; margin-bottom: 20px;margin-top: 42px;
                                 line-height: 43px;"">Welcome to {site_name}</h2>
                           </td>
                        </tr>
                        <tr>
                           <td style="color: #7b7b7b; font-size: 16px; font-weight: 300;">
                              <div style="margin-top: 0;
                                 line-height: 27px;
                                 color: #333333;
                                 font-size: 16px;
                                 font-weight: 300;
                                 text-align: left;"">Thank you for signing up!<span style="display: block;">Please click the following link to activate your account.</span></div>
                              <a class="m_3575264571938759704m_-6317637866279845492button" style="display: inline-block;
                                 margin-top:26px;
                                 width: 224px;
                                 min-height: 20px;
                                 padding: 10px;
                                 background-color: #ff595e;
                                 border-radius: 3px;
                                 color: #ffffff;
                                 font-size: 18px;
                                 line-height: 28px;
                                 text-align: center;
                                 text-decoration: none;
                                 font-weight: 500;" href="{account_activation_link}" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?hl=en-GB&amp;q=http://app.visibly.io/sign-up?e%3DY2hpcmFnQHZpc2libHkuaW8%3D%26t%3DSU1vVWlGOGpMRDFpMTVh&amp;source=gmail&amp;ust=1533720769606000&amp;usg=AFQjCNHNCRy18e5mTOhnW2qzOqZT3J9_Nw"> Activate your Account </a><br /><br /><br />
                              <p style="color: #333333;
                                 font-weight: 300;
                                 font-size: 16px;
                                 line-height: 28px;">Thanks,<br /><span style="color: #333333;font-weight:400;font-size: 16px;line-height: 27px;">Visibly Team</span></p>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
            
            <tr>
<td style="text-align: center; padding-top: 40px; color: #333333; font-weight: 400; font-size: 14px;">Made by Visibly Ltd <span style="color: #1c1c1c;">•</span> <a style="color: #256dfc; font-weight: 300; border-bottom: 1px solid #256dfc; text-decoration: none;" href="https://visibly.io/blog/" target="blank">Our Blog</a><br /><br /><a style="text-decoration: none; color: #333333;">107 High Street <span style="color: #1c1c1c;">•</span> Great Abington, Cambridge<span style="color: #1c1c1c;"> •</span> CB21 6AE </a> <br /><br /><span style="color: #1c1c1c;"> •</span> United Kingdom</td>
</tr>
<tr>
<td style="text-align: center; padding-top: 17px;"><a class="linkedin-icon" style="display: inline-block; margin-right: 5px; width: 30px; vertical-align: middle; height: 30px; border-radius: 37px; -webkit-border-radius: 37px; -o-border-radius: 37px; -ms-border-radius: 37px; -moz-border-radius: 37px; border: 1px solid #b3b3b3; color: #b3b3b3; text-align: center; line-height: 29px; font-size: 20px; -webkit-transition: all 0.5s ease; -moz-transition: all 0.5s ease; transition: all 0.5s ease;" title="Linkedin" href="https://www.linkedin.com/company/visiblyhq/"><img src="https://visibly.io/wp-content/uploads/2018/07/linkedin-32.png" width="14px;" /></a> <a class="facebook-icon" style="display: inline-block; width: 30px; vertical-align: middle; height: 30px; border-radius: 37px; -webkit-border-radius: 37px; -o-border-radius: 37px; -ms-border-radius: 37px; -moz-border-radius: 37px; border: 1px solid #b3b3b3; color: #b3b3b3; margin-right: 5px; text-align: center; line-height: 33px; font-size: 20px; -webkit-transition: all 0.5s ease; -moz-transition: all 0.5s ease; transition: all 0.5s ease;" title="Facebook" href="https://www.facebook.com/BeVisibly/"><img src="https://visibly.io/wp-content/uploads/2018/07/facebook.png" width="9px;" /></a> <a class="twitter-icon" style="display: inline-block; width: 30px; vertical-align: middle; height: 30px; border-radius: 37px; -webkit-border-radius: 37px; -o-border-radius: 37px; -ms-border-radius: 37px; -moz-border-radius: 37px; border: 1px solid #b3b3b3; color: #b3b3b3; text-align: center; line-height: 31px; font-size: 20px; -webkit-transition: all 0.5s ease; -moz-transition: all 0.5s ease; transition: all 0.5s ease;" title="Twitter" href="https://twitter.com/BeVisibly"><img src="https://visibly.io/wp-content/uploads/2018/07/twitter.png" width="14px;" /></a></td>
</tr>
         </tbody>
      </table>