<?php
/**
 * Template Name: Contact
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>
<?php while ( have_posts() ) : the_post(); ?>
<div class="contact-page form-page">
	<div class="container">
        
       <div class="contact-form-wrapper">   
       
       <div class="contact-form-inner form-inner">
       	<div id="contact_section">
       		<div class="inner_wrap">
       			<div class="contact_form">
       				<h1 class="form-main-title"><?php the_title(); ?></h1>
       				<div class="contact-subtitle form-sub-title">Call, email or chat anytime</div>
       				<div class="form-wrapper">
	       				<?php dynamic_sidebar('contact-section') ?>
	       				<div class="form-image-wrapper"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/contact-bg.jpg" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"></div>
	       			</div>
       			</div>
       		</div>
       	</div>
       </div>

</div>
<div class="contact-text-block">
			<div class="contact-text-title">Get in touch</div>
			<div class="contact-text-col-wrapper">
				<div class="contact-text-col">
					<div class="contact-text-col-inner">
						<div class="contact-text-col-title">Sales</div>
						<div class="contact-text-col-email"><a href="mailto:sales@visibly.io">sales@visibly.io</a></div>
						<div class="contact-text-col-number">+44-1223-931660</div>
					</div>
				</div>
				<div class="contact-text-col">
					<div class="contact-text-col-inner">
						<div class="contact-text-col-title">Press</div>
						<div class="contact-text-col-email"><a href="mailto:info@visibly.io">info@visibly.io</a></div>
						<div class="contact-text-col-number">+44-1223-931660</div>
					</div>
				</div>
				<div class="contact-text-col">
					<div class="contact-text-col-inner">
						<div class="contact-text-col-title">Support</div>
						<div class="contact-text-col-email"><a href="mailto:support@visibly.io">support@visibly.io</a></div>
					</div>
				</div>
			</div>
		</div>
</div>
<?php endwhile; ?>
<!-- <div class="section">
<div id="SurveyWidget"></div>
</div> -->

<!-- <script type="text/javascript">
var $widget_secret = "wJXYvc5n" 
var $feed = "1" 
var $feedback = "1"

</script>
<script type="text/javascript" src=" http://local.visibly.io:8080/js/SurveyWidget.js" ></script> -->
<?php get_footer(); ?>