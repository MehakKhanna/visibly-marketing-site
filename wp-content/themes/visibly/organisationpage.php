<?php
/**
 * Template Name: Organisation page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?>

<?php  $page_slug ='join-the-growing-number-of-organisations-using-visibly';
$partner_page_data = get_page_by_path($page_slug); ?>

<!-- Partner section starts -->
<div class="partner-section"> 
   <div class="partner-title-wrapper a-center container">
    <?php $editLink = get_edit_post_link($partner_page_data->ID); include('inc/edit-link.php'); ?>
        <?php $post_type_obj = get_post_type_object( 'organisations' );?>
        <h2 class="pageInnerTitle"><?php echo $partner_page_data->post_title; ?></h2>
        <p class="pageInnerText">
            <span class="free-trial-block">
                <?php echo get_post_meta($partner_page_data->ID, 'wpcf-organisation-sub-title-for-india', true); ?>
            </span>
            
    </p>
</div>
<div class="container">
    <div class="partner-list">

       <?php $args = array(
        'posts_per_page'   => -1,
        'orderby'          => 'order',
        'post_type'        => 'organisations',
        'post_status'      => 'publish',
        'order' => 'ASC',
        );
       $posts_array = get_posts( $args );

       if( sizeof($posts_array) != 0 )
       {    
        foreach ($posts_array as $post) { ?>
        <div class="partner">
            <?php $editLink = get_edit_post_link($post->ID); include('inc/edit-link.php'); ?>
            <a target="_blank" href="<?php echo get_post_meta($post->ID, 'wpcf-organisation-link', true); ?>" title="<?php echo $post->post_title; ?>">
                <img src="<?php echo get_post_meta($post->ID, 'wpcf-organisation-logo', true);?>" title="<?php echo $post->post_title; ?>" alt="<?php echo $post->post_title; ?>">
            </a>
        </div>

        <?php }
    } ?>

    </div>
</div>
</div>
    <!-- Partner section ends -->