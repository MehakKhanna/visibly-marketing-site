<?php
/**
 * Template Name: eBook
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();

?>
<h1 id="h1_title"><?php the_title(); ?></h1>

<!-- Main wrapper starts  -->
<div class='mainWrapper blogMainWrapper'>

    <!-- blogHeroSection starts -->


  


  <!--blogListSection starts-->
  <div class="blogListSection">
    <div id="new_grid">
        <div class="container">

            <!-- bloglistWrapper starts -->
            <div class="bloglistWrapper">
               

<!-- ARTICLE SECTION STARTS -->
                    <section class="articles_section">
                         <div class="title_section text-center">
                                <h3 class="title_txt"><span class="line_txt">All ebooks</span></h3>
                            </div>
                        <div class="article_list">
                        <div class="inner_wrap">

                            <div class="article_row flex_view">
                <?php

                $args1 = array(
                    'post_type'        => 'ebooks',
                    'post_status'      => 'publish',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'posts_per_page' => -1,
                );


                $posts_array = get_posts( $args1 );

                if( sizeof($posts_array) != 0 )
                {    
                    foreach ($posts_array as $post) { 
                        $categories = get_the_category();
                        $cls = '';

                        if ( ! empty( $categories ) ) {
                          foreach ( $categories as $cat ) {
                            $cls .= $cat->slug . ' ';
                        }
                    }

                    ?>


                                <div class="article select-all <?php echo $cls;?>">
                                     <?php
                                            $term_list = wp_get_post_terms($post->ID, 'category', ['fields' => 'all']);
                                            $primaryCategory='';
                                            foreach($term_list as $term) {
                                             if( get_post_meta($post->ID, '_yoast_wpseo_primary_category',true) == $term->term_id ) {
                                                $primaryCategory = $term->name;
                                            }
                                        }

                                        ?>
                                    <a target="_blank" href="<?php the_field('ebook_link'); ?>"  class="article_url flex_view middle">
                                      
                                        <span class="border sm-hide"></span>
                                        <div class="article_image_wrap">
                                            <div class="image">
                                               <?php  if ( has_post_thumbnail() ) {?>
                                                <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="wp-post-image">
                                                <?php 
                                            } ?>
                                        </div>
                                    </div>
                                    <div class="article_text_wrap">
                                        <h3 class="article_title"><?php echo $post->post_title; ?></h3>
                                        <div class="article_descr"><?php the_field('ebook_description') ?></div>

                                    </div>
                                  
                                </a>
                            </div>



                      


            <?php

        }
    } ?>
  </div>


                    </div>

                </div>
    </section>
            <!-- ARTICLE SECTION ENDS -->




    



</div>
<!-- bloglistWrapper ends -->
</div>
</div>
</div>
<!--blogListSection ends-->
</div>
<!-- Main wrapper ends-->







<?php get_footer(); ?>