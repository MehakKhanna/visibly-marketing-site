<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="mainWrapper aboutMainWrapper">
        <div class="no-page-section global-think-section">
            <div class="container cf">
				<div class="no-page-section-cover">
					<div class="heading-img">
				<h3>Err..err..error</h3>
				<h2>Page not found</h2>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/404_img.png" title="404" alt="404">
				</div>
				<div class="heading-button">
				<h3>No robots, only humans</h3>
				<div class="homebutton">
				<a href="<?php echo get_home_url() ?>">Go Home</a>
				</div>
                </div>

            </div>
		        </div>
		</div>
</div>
	

		

<?php get_footer();?>
