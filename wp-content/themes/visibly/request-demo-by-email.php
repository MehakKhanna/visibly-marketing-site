<?php
/**
 * Template Name: Request demo page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();
?>
<div class="form-page">
<div class="mainWrapper">
  <div class="demo-by-email-box global-think-section">
    <div class="container cf">
      <div class="demoBox">
        <div class="demoHeading">
          <?php while ( have_posts() ) : the_post(); 
          $titleAfterSubmit = get_post_meta($post->ID, 'wpcf-request-demo-title-after-submit-form', true); 
          $paragraphAfterSubmit = get_post_meta($post->ID, 'wpcf-request-demo-paragraph-after-submit-form', true); 
          $imageUrl = get_post_meta($post->ID, 'wpcf-request-demo-image-after-submit-form', true);  
          ?>


          <?php if(isset($_GET['message'])){ ?>
           <h1 class="form-main-title"><?php echo $titleAfterSubmit; ?></h1>
            


          <div class="demoMailForm">
            <div class="requestdemo-paragraph"> <?php echo $paragraphAfterSubmit; ?> </div>
          </div>
          <div class = "cup-of-tea-image"><img src = "<?php echo $imageUrl; ?>" alt="image-after-submit" alt="Request demo by email" title="Request demo by email" /></div>

          

          <?php }else{ ?> 
          
          <h1 class="form-main-title"><?php the_title(); ?></h1>
            <div class="form-sub-title">Please fill out the form below</div>
            <div class="form-wrapper">
          <div class="demoMailForm india-signupform-wrapper">
          <?php if(!is_user_logged_in()) { ?>
            <div class="india-signupform-wrapper-inner">  
              <?php the_content(); ?>
              <div class="rde-checksbumit-wrapper">
                <div class="rde-checksbumit">Submit</div>
              </div>
              <div class="demoFormBtn">
            <div class="book-demo-link-wrapper demoBtnBlock">
              <div class="emailFormBelowText cf">
                 <?php dynamic_sidebar('platform-icons-sidebar'); ?>
                 <?php dynamic_sidebar('login-text-sidebar'); ?>
                 
                <?php // echo dynamic_sidebar('platform-icon1'); ?>
                <?php //echo dynamic_sidebar('login-text'); ?>
              </div>
            </div>
          </div>
            </div>
           <?php } else { ?> 
           You are already registered.
           <?php } ?>
            </div>
            <div class="form-image-wrapper"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/contact-bg.jpg" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"></div>
          </div>
          <?php } 
          endwhile; ?>

      </div>
      
    </div>
      <div class="WaitTextBottom">
        <?php $editLink = get_admin_url().'widgets.php'; include('inc/edit-link.php'); ?>
        <?php dynamic_sidebar('can_not_wait_for_demo')?>
      </div>

    </div>
  </div>
</div>
  <div  class="request-demo-hubspotform disappear">
    <form id="request-demo-hubspot" action="<?php echo get_home_url() ?>/thanks-for-request-a-demo/">
      <h2>Request demo</h2>
      <div class="signupInputWrapper">
        <input class="name" type="text" placeholder="Name">
        <input class="email" type="email" placeholder="Email Address">
        <textarea class="positionInOrganisation" type="text" placeholder="Position in organisation"></textarea>
        <input class="numberOfEmployees" type="text" placeholder="Number of employees">
        <input class="country" type="text" placeholder="Country">
        <input class="contactNumber" type="text" placeholder="Contact number">
      </div>
      <div class="signupSubmit">
        <input type="submit" value="Get started" class="signup-submit">
        
      </div>
    </form>
  </div>
</div>
<?php get_footer(); ?>