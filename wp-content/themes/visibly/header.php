<?php
const freemiumCountries=['in','pk','bt','np','lk','bd','mv','af','mm'];
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <title><?php wp_title(''); ?></title>
 <link id="favicon" rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/assets/images/favicon.ico" sizes="16x16 32x32 48x48" type="image/ico" />
 <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,700,800,900" rel="stylesheet">
 <meta charset="<?php bloginfo( 'charset' ); ?>">
 <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
 <link rel="profile" href="https://gmpg.org/xfn/11">
 <!-- <meta name="description" content="<?php echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true);  ?>" > -->
<meta name="keywords" content="<?php echo get_post_meta($post->ID, 'wpcf-meta-keywords', true); ?>">
<meta name="google-site-verification" content="rvD3xq9PbPMNGTV4Obn91zcgQcJanDowWmBnOYEEuN4" />


<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-00000000-0', 'auto');
    ga('send', 'pageview');
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async  src="https://www.googletagmanager.com/gtag/js?id=UA-34071815-2"></script>
<script  >
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

gtag('config', 'UA-34071815-2');
</script> 

<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144845823-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144845823-4');
</script> -->


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id="></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '', { 'optimize_id': 'GTM-M36STX2'});
</script>

<?php wp_head(); ?>
<script  src='//platform-api.sharethis.com/js/sharethis.js#property=5b46fd603cd9e600119c7b1d&#038;product=null-share-buttons-wp'></script>
<?php $islive = include('inc/checkLocal.php');
if($islive){?>
    <script >
        if(window.location.protocol=='http:'){
            var url = 'https://'+window.location.host+window.location.pathname;
        }
    </script>
<?php } ?>
</head>

<body <?php body_class(); ?>>
    <div id="page" class="site">


        <!--Header starts-->
        <div class="headerWrapper">
            <header id="header" class="header">
                <div class="container cf">
                    <div id='logo'>
                        <?php if ( is_front_page() ) {?>

                        <?php }else{ ?>
                          <!--   <h1 id="h1_title"><?php the_title(); ?></h1> -->
                      <?php } ?>
                      <a href="<?php echo get_home_url() ?>" title='Visibly'>
                        <svg class="nav__image image" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 374.68 95.95">
                            <defs>
                                <style>
                                    .logo-cls-1 {
                                        fill: #000;
                                    }

                                    .logo-cls-2 {
                                        fill: #ffbf00;
                                    }

                                    .logo-cls-3 {
                                        fill: #ff595e;
                                    }
                                </style>
                            </defs>
                            <g data-name="Layer 2">
                                <g data-name="Layer 1">
                                    <path class="logo-cls-1" d="M147.92,23.56H164.1V73.12H147.92ZM156,0a8.71,8.71,0,0,0-6.57,2.63,9,9,0,0,0-2.53,6.47,9,9,0,0,0,2.53,6.47,9.53,9.53,0,0,0,13.15,0,9,9,0,0,0,2.53-6.47,9,9,0,0,0-2.53-6.47A8.71,8.71,0,0,0,156,0Zm48,46.32a23.57,23.57,0,0,0-5.11-3q-2.73-1.16-5-2-2.93-1.11-4.8-1.87A2.58,2.58,0,0,1,187.31,37a2.2,2.2,0,0,1,1.57-2.28,14.11,14.11,0,0,1,4.5-.56,26.17,26.17,0,0,1,6.37.76,35.13,35.13,0,0,1,5.76,2l2.22-10.62a26.5,26.5,0,0,0-7.38-2.88,39,39,0,0,0-8.5-.86q-8.8,0-14.26,3.84t-5.46,11.93A13.46,13.46,0,0,0,173.25,44a13.78,13.78,0,0,0,3.08,4.3,19.3,19.3,0,0,0,4.7,3.24A51.86,51.86,0,0,0,186.91,54q3.84,1.42,5.76,2.17a2.65,2.65,0,0,1,1.92,2.58,3,3,0,0,1-2.17,2.88,13.84,13.84,0,0,1-5.11.86,39.37,39.37,0,0,1-6.62-.66,32,32,0,0,1-6.93-2L171.53,71.1a37.27,37.27,0,0,0,8.09,2.28,50.62,50.62,0,0,0,8.5.76,32,32,0,0,0,8.7-1.11,20.38,20.38,0,0,0,6.83-3.29,14.73,14.73,0,0,0,4.45-5.41,16.73,16.73,0,0,0,1.57-7.38A11.47,11.47,0,0,0,208,50.72,16.64,16.64,0,0,0,204,46.32Zm12.37,26.8H232.6V23.56H216.42ZM224.51,0a8.71,8.71,0,0,0-6.57,2.63A9,9,0,0,0,215.4,9.1a9,9,0,0,0,2.53,6.47,9.53,9.53,0,0,0,13.15,0,9,9,0,0,0,2.53-6.47,9,9,0,0,0-2.53-6.47A8.71,8.71,0,0,0,224.51,0Zm66.4,37.72a32.79,32.79,0,0,1,1.52,10.21,29.18,29.18,0,0,1-1.77,10.16,22.79,22.79,0,0,1-5.31,8.34,25.26,25.26,0,0,1-8.9,5.61,34.69,34.69,0,0,1-12.54,2.07,49.83,49.83,0,0,1-11.58-1.31,44.81,44.81,0,0,1-10.06-3.64V1.72h16.18V27.41a17.33,17.33,0,0,1,5.11-3.49,18.13,18.13,0,0,1,7.53-1.37,20.28,20.28,0,0,1,8.8,1.87,19.32,19.32,0,0,1,6.73,5.26A24.52,24.52,0,0,1,290.91,37.72ZM276.24,48.14q0-6.07-2.38-9.66t-7.43-3.59a12.47,12.47,0,0,0-8,2.73V61.08a12,12,0,0,0,2.63.81,16,16,0,0,0,3.14.3,12.53,12.53,0,0,0,5.46-1.11,9.8,9.8,0,0,0,3.74-3,13.62,13.62,0,0,0,2.12-4.45A20.23,20.23,0,0,0,276.24,48.14ZM319,62q-2.33,0-3.13-1.06a4.82,4.82,0,0,1-.81-3V1.72H298.9v58a19.93,19.93,0,0,0,.71,5.31,12.6,12.6,0,0,0,2.33,4.6,11.32,11.32,0,0,0,4.35,3.24,16.68,16.68,0,0,0,6.78,1.21,39.12,39.12,0,0,0,5.31-.35,30.72,30.72,0,0,0,4.6-1l-.51-11.12A23.8,23.8,0,0,1,319,62ZM358.4,23.56,334.93,96h16.28l23.46-72.39ZM98.09,22H81.3L99.2,72.75l6.55-21Zm4.24,50.77h16.28L142.07.37H125.79ZM345,53.29l-7.67-29.73H320.51l17.9,50.77Z"></path>
                                    <path class="logo-cls-2" d="M17.21,22V49.66H40.52V72.71H67.85V22ZM57.73,62.58H50.64V39.52H27.33v-7.4h30.4Z"></path>
                                    <path class="logo-cls-3" d="M50.64,90.25V62.57H27.33V39.52H0V90.25ZM10.12,49.66h7.09V72.71H40.52v7.4H10.12Z"></path>
                                </g>
                            </g>
                        </svg>
                    </a>

                </div>
                <div class="headerRight">
                    <div class="headerRightInner clearfix">
                        <div class="menu-handle-wrapper">
                            <div class="menu-handle-line"></div>
                            <div class="menu-handle-line"></div>
                            <div class="menu-handle-line"></div>
                        </div>
                        <div class="mainNav">
                            <div class="menuNavShapeWrapper">
                                <div class="menuNavShape menuNavShape1"></div>
                                <div class="menuNavShape menuNavShape2"></div>
                                <div class="menuNavShape menuNavShape3"></div>
                            </div>
                            <div class="manuNavWrapper clearfix">
                                <div class="mobileMenuTitle cf">
                                    <div class="mobile-menu-logo-wrapper">
                                        <div class="mobile-menu-logo">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="nav__image image" viewBox="0 0 374 95">
                                                <defs>
                                                    <style>
                                                        .mobile-menu-logo-wrapper .cls-1 {
                                                            fill: #fff;
                                                        }

                                                        .mobile-menu-logo-wrapper .cls-2 {
                                                            fill: #ffbf00;
                                                        }

                                                        .mobile-menu-logo-wrapper .cls-3 {
                                                            fill: #ff595e;
                                                        }

                                                        .close-menu .close {
                                                            fill: #fff;
                                                        }
                                                    </style>
                                                </defs>
                                                <g data-name="Layer 2">
                                                    <g data-name="Layer 1">
                                                        <path class="cls-2" d="M17.21,22V49.66H40.52V72.71H67.85V22ZM57.73,62.58H50.64V39.52H27.33v-7.4h30.4Z" />
                                                        <path class="cls-3" d="M50.64,90.25V62.57H27.33V39.52H0V90.25ZM10.12,49.66h7.09V72.71H40.52v7.4H10.12Z" />
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                    <a href="#" class="close-menu" title="Close">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="-255 347 100 100" width="24" aria-label="Close Navigation" class="svg-replaced"
                                        shape-rendering="geometricPrecision">
                                        <path class="close" d="M-160.4 434.2l-37.2-37.2 37.1-37.1-7-7-37.1 37.1-37.1-37.1-7 7 37.1 37.1-37.2 37.2 7.1 7 37.1-37.2 37.2 37.2"></path>
                                    </svg>
                                </a>
                            </div>
                            <div class="mobile-menu">
                                <?php wp_nav_menu( array( 'menu'=>'mobile-menu' ) ); ?>
                                <ul class="start-link-wrapper">
                                    <?php dynamic_sidebar('free-trial-link'); ?>
                                </ul>        
                            </div>
                            <div class="desktop-menu">
                                <a target="_blank" class="login-link" href="https://app.visibly.io/login" tabindex="0">Login</a>
                                <ul class="start-link-wrapper">
                                    <li><a href="<?php echo get_home_url() ?>/request-a-demo-by-mail/">Request Demo</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- Left Nav -->
                    <div class="left_nav desktop-menu">

                        <ul class="main_nav_ul clearfix">

                            <li class="main_nav_li platform_nav_li submenu_parent_li">
                                <div  class="nav_item no-cursor">Platform
                                    <span class="icon"><svg viewBox="0 0 11 7" xmlns="http://www.w3.org/2000/svg"><path d="M5.371 3.95L1.707.293A1 1 0 0 0 .293 1.708l4.348 4.34a1 1 0 0 0 1.389.023l4.652-4.34A1 1 0 1 0 9.318.27L5.37 3.95z"></path></svg> </span></div>

                                    <div class="submenu">
                                      <div class="submenu_inner">
                                        <div class="container">
                                            <div class="submenu_wrapper">
                                              <div class="submenu_title">HOW VISIBLY CAN HELP YOU</div>
                                              <div class="submenu_content how_visibly_can_help_menu">
                                                <ul class="clearfix">
                                                    <li class="green_li">
                                        <div class="submenu_li_inner">
                                            <a title="Listen" href="<?php echo get_home_url() ?>/listen">
                                              <div class="icon">
                                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                viewBox="0 0 508 508" style="enable-background:new 0 0 508 508;" xml:space="preserve">
                                                <g>
                                                  <g>
                                                    <path class="monitor_st0" d="M497.6,375.8l-32.7-32.7V64.6H0v310h160.5l-37.2,48.1c-5,5.9-4.1,20,10.4,20.8h197.7
                                                    c14.2-1.3,14.9-15.4,10.4-20.8l-37.2-48.1H399l49.9,49.9c21.5,19,42.2,6.5,48.7,0C504.2,418,517.3,397,497.6,375.8z M322.7,426.2
                                                    H142.3l40-51.7h100.4L322.7,426.2z M17.2,357.4v-0.1V81.8h430.5v244l-31.9-31.9l-6.1,6.1L398,288.3c14.9-19,23.8-42.9,23.8-68.8
                                                    c0-61.7-50.2-111.9-111.9-111.9S198,157.8,198,219.6s50.2,111.9,112,111.9c17,0,33-3.9,47.5-10.7l15.8,15.8l-6.1,6.1l14.7,14.7
                                                    L17.2,357.4L17.2,357.4z M245.4,228.3v-0.1l19-25.3l27.2,72.7l27-53.9l24.4,48.7l31.6-42.1h29.7c-4.4,48.2-45,86.1-94.3,86.1
                                                    s-89.9-37.9-94.3-86.1L245.4,228.3L245.4,228.3z M215.7,210.9c4.4-48.2,45-86.1,94.3-86.1s89.9,37.9,94.3,86.1H366l-20.1,26.7
                                                    L318.6,183l-24.7,49.4l-24.4-65.1l-32.7,43.6H215.7z M373,312c4.8-3.3,9.2-7,13.4-10.9l11.1,11.1l-12.1,12.2L373,312z
                                                    M485.5,412.3c-6.5,6.5-17.8,6.5-24.4,0l-69.6-69.7l24.4-24.4l69.6,69.7C495,398,488.7,409,485.5,412.3z"/>
                                                </g>
                                            </g>
                                            <g>
                                                                              <g>
                                                                                <path class="monitor_st0" d="M120.6,133.4v-25.8H68.9v25.8H43.1v51.7h25.8v25.8h51.7v-25.8h25.8v-51.7H120.6z M129.1,167.9h-25.8v25.8
                                                                                H86.1v-25.8H60.3v-17.2h25.8v-25.8h17.2v25.8h25.8V167.9z"/>
                                                                            </g>
                                                                        </g>
                                                                        <g>
                                                                          <g>
                                                                            <rect x="43.1" y="254" class="monitor_st0" width="137.8" height="17.2"/>
                                                                        </g>
                                                                    </g>
                                                                    <g>
                                                                      <g>
                                                                        <rect x="43.1" y="305.7" class="monitor_st0" width="77.5" height="17.2"/>
                                                                    </g>
                                                                </g>
                                                                <g>
                                                                  <g class="monitor_st1">
                                                                    <path class="monitor_st0" d="M497.6,375.7L464.9,343V64.5H0v310h160.5l-37.2,48.1c-5,5.9-4.1,20,10.4,20.8h197.7
                                                                    c14.2-1.3,14.9-15.4,10.4-20.8l-37.2-48.1H399l49.9,49.9c21.5,19,42.2,6.5,48.7,0C504.2,417.9,517.3,396.9,497.6,375.7z"/>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </div>
                                                    <div class="submenu-text">Listen</div>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="primary_li">
                                                <div class="submenu_li_inner">
                                                    <a href="<?php echo get_home_url() ?>/communicate">
                                                      <div class="icon">
                                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                        viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                        <path class="communicate_st0" d="M380.7,435.9c0,30.7,25,55.6,55.6,55.6s55.6-25,55.6-55.6c0-25.9-17.8-47.7-41.7-53.9v-72.2H283.4v-41.7h83.5
                                                        v-41.7c0-53.7-43.7-97.4-97.4-97.4s-97.4,43.7-97.4,97.4v41.7h83.5v41.7H87.7V382c-24,6.2-41.7,28-41.7,53.9
                                                        c0,30.7,25,55.6,55.6,55.6s55.6-25,55.6-55.6c0-25.9-17.8-47.7-41.7-53.9v-44.4h140V382c-24,6.2-41.7,28-41.7,53.9
                                                        c0,30.7,25,55.6,55.6,55.6s55.6-25,55.6-55.6c0-25.9-17.8-47.7-41.7-53.9v-44.4h139.1V382C398.5,388.2,380.7,410,380.7,435.9z"/>
                                                        <path class="communicate_st0" d="M325.1,73.4c0-30.5-24.7-56.6-55.6-56.6c-31,0-55.6,26.1-55.6,56.6c0,30.7,25,55.6,55.6,55.6
                                                        S325.1,104,325.1,73.4z"/>
                                                        <path class="communicate_st1" d="M380.7,436.4c0,30.7,25,55.6,55.6,55.6s55.6-25,55.6-55.6c0-25.9-17.8-47.7-41.7-53.9v-72.2H283.4v-41.7h83.5
                                                        v-41.7c0-53.7-43.7-97.4-97.4-97.4s-97.4,43.7-97.4,97.4v41.7h83.5v41.7H87.7v72.2c-24,6.2-41.7,28-41.7,53.9
                                                        C46,467,71,492,101.6,492s55.6-25,55.6-55.6c0-25.9-17.8-47.7-41.7-53.9v-44.4h140v44.4c-24,6.2-41.7,28-41.7,53.9
                                                        c0,30.7,25,55.6,55.6,55.6s55.6-25,55.6-55.6c0-25.9-17.8-47.7-41.7-53.9v-44.4h139.1v44.4C398.5,388.7,380.7,410.5,380.7,436.4z
                                                        M199.9,226.8c0-33.6,23.9-61.7,55.6-68.1v54.2h27.8v-54.2c31.7,6.5,55.6,34.6,55.6,68.1v13.9H199.9V226.8z M129.5,436.4
                                                        c0,15.3-12.5,27.8-27.8,27.8s-27.8-12.5-27.8-27.8c0-15.3,12.5-27.8,27.8-27.8S129.5,421,129.5,436.4z M297.3,436.4
                                                        c0,15.3-12.5,27.8-27.8,27.8s-27.8-12.5-27.8-27.8c0-15.3,12.5-27.8,27.8-27.8S297.3,421,297.3,436.4z M464.2,436.4
                                                        c0,15.3-12.5,27.8-27.8,27.8c-15.3,0-27.8-12.5-27.8-27.8c0-15.3,12.5-27.8,27.8-27.8C451.7,408.5,464.2,421,464.2,436.4z"/>
                                                        <path class="communicate_st1" d="M325.1,73.8c0-30.5-24.7-56.6-55.6-56.6c-31,0-55.6,26.1-55.6,56.6c0,30.7,25,55.6,55.6,55.6
                                                        S325.1,104.5,325.1,73.8z M269.5,101.6c-15.3,0-27.8-12.5-27.8-27.8c0-15.6,12.7-28.7,27.8-28.7c15.1,0,27.8,13.2,27.8,28.7
                                                        C297.3,89.2,284.8,101.6,269.5,101.6z"/>
                                                    </svg>

                                                </div>
                                                <div class="submenu-text">Communicate</div>
                                            </a>
                                        </div>
                                    </li>
                                                  <li class="theme_li">
                                                    <div class="submenu_li_inner">
                                                        <a title="Engage" href="<?php echo get_home_url() ?>/engage">
                                                          <div class="icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
<style type="text/css">
    .st0{opacity:0.3;fill:#256EFF;}
    .st1{fill:none;}
    .st2{fill:#256EFF;}
</style>
<path class="st0" d="M15.3,13.7c-0.1,0.1-0.2,0.2-0.2,0.3H8.9c-0.1-0.1-0.1-0.2-0.2-0.3C7.4,12.5,6.8,10.8,7.1,9  C7.4,7.1,9,5.5,11,5.1C11.3,5,11.7,5,12,5c2.8,0,5,2.2,5,5C17,11.4,16.4,12.8,15.3,13.7z"/>
<g>
    <rect class="st1" width="24" height="24"/>
</g>
<g>
    <g>
        <path class="st2" d="M12,3c-0.5,0-0.9,0-1.4,0.1c-2.8,0.5-5,2.8-5.5,5.5c-0.5,2.6,0.5,5,2.2,6.6C7.8,15.6,8,16.1,8,16.7V19    c0,1.1,0.9,2,2,2h0.3c0.4,0.6,1,1,1.7,1s1.4-0.4,1.7-1H14c1.1,0,2-0.9,2-2v-2.3c0-0.5,0.2-1.1,0.6-1.5c1.5-1.3,2.4-3.2,2.4-5.2    C19,6.1,15.9,3,12,3z M14,17h-4v-1h4V17z M10,19v-1h4v1H10z M15.3,13.7c-0.1,0.1-0.2,0.2-0.2,0.3H8.9c-0.1-0.1-0.1-0.2-0.2-0.3    C7.4,12.5,6.8,10.8,7.1,9c0.4-1.9,2-3.6,3.9-3.9C11.3,5,11.7,5,12,5c2.8,0,5,2.2,5,5C17,11.4,16.4,12.8,15.3,13.7z"/>
    </g>
    <g>
        <rect x="11.5" y="11" class="st2" width="1" height="3"/>
        <rect x="10.6" y="8.8" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -4.0275 10.8527)" class="st2" width="1" height="3"/>
        <rect x="11.4" y="9.8" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -3.4991 12.1487)" class="st2" width="3" height="1"/>
    </g>
</g>
</svg>
                                                        </div>
                                                        <div class="submenu-text">Engage</div>
                                                    </a>
                                                </div>
                                            </li>
                                            

        <!-- <li class="secondary_li">
            <div class="submenu_li_inner">
                <a title="Advocate" href="<?php echo get_home_url() ?>/features/advocacy">
                  <div class="icon">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                    <path class="advocate_st0" d="M0,0h24v24H0V0z"/>
                    <path class="advocate_st1" d="M18,16.1c-0.8,0-1.4,0.3-2,0.8l-7.1-4.2C9,12.5,9,12.2,9,12s0-0.5-0.1-0.7L16,7.2C16.5,7.7,17.2,8,18,8
                    c1.7,0,3-1.3,3-3s-1.3-3-3-3s-3,1.3-3,3c0,0.2,0,0.5,0.1,0.7L8,9.8C7.5,9.3,6.8,9,6,9c-1.7,0-3,1.3-3,3s1.3,3,3,3
                    c0.8,0,1.5-0.3,2-0.8l7.1,4.2c-0.1,0.2-0.1,0.4-0.1,0.6c0,1.6,1.3,2.9,2.9,2.9s2.9-1.3,2.9-2.9S19.6,16.1,18,16.1z"/>
                    <path class="advocate_st2" d="M18,16.1c-0.8,0-1.4,0.3-2,0.8l-7.1-4.2C9,12.5,9,12.3,9,12s0-0.5-0.1-0.7L16,7.2C16.5,7.7,17.2,8,18,8
                    c1.7,0,3-1.3,3-3s-1.3-3-3-3s-3,1.3-3,3c0,0.2,0,0.5,0.1,0.7L8,9.9C7.5,9.4,6.8,9,6,9c-1.7,0-3,1.3-3,3s1.3,3,3,3
                    c0.8,0,1.5-0.3,2-0.8l7.1,4.2c-0.1,0.2-0.1,0.4-0.1,0.6c0,1.6,1.3,2.9,2.9,2.9s2.9-1.3,2.9-2.9S19.6,16.1,18,16.1z M18,4
                    c0.5,0,1,0.4,1,1s-0.5,1-1,1s-1-0.4-1-1S17.5,4,18,4z M6,13c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S6.6,13,6,13z M18,20.1
                    c-0.5,0-1-0.5-1-1s0.5-1,1-1s1,0.5,1,1S18.5,20.1,18,20.1z"/>
                </svg>
            </div>
            <div class="submenu-text">Advocate</div>
        </a>
    </div>
</li> -->
</ul>

</div>
</div>
</div>
<div class="submenu_bottom_section">
  <div class="container">
    <p><strong>Blog:</strong>Employee experience and the future of work <a class="link_with_arrow" href="<?php echo get_home_url(); ?>/employee-experience-the-future-of-work/" title="Read now">Read now 
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
      viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
      <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
      <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
  </svg></a></p>
</div>
</div>
</div>
</div>
</li>

<li class="main_nav_li why_visibly_nav_li"><a href="<?php echo get_home_url() ?>/why-visibly" class="nav_item">Why Visibly</a></li>

<li class="main_nav_li commnunity_li submenu_parent_li">
    <a href="javascript:void(0);" class="nav_item">Community
        <span class="icon"><svg viewBox="0 0 11 7" xmlns="http://www.w3.org/2000/svg"><path d="M5.371 3.95L1.707.293A1 1 0 0 0 .293 1.708l4.348 4.34a1 1 0 0 0 1.389.023l4.652-4.34A1 1 0 1 0 9.318.27L5.37 3.95z"></path></svg> </span>
    </a>
    <div class="submenu">
      <div class="submenu_inner">
        <div class="container">
            <div class="submenu_wrapper">
              <div class="submenu_title">HOW VISIBLY CAN HELP YOU</div>
              <div class="submenu_content">
                <ul class="clearfix">
                  <li class="theme_li">
                    <div class="submenu_li_inner">
                        <a title="Podcasts" href="<?php echo get_home_url() ?>/podcasts">
                          <div class="icon">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <path class="podcast-st0" d="M487,264.5c0-62.4-24.3-121-68.4-165.1S315.9,31,253.5,31s-121,24.3-165.1,68.4S20,202.1,20,264.5
                                c0,95.5,58.5,181.4,146.5,216.8v3.1c0,7.6,6.1,13.7,13.7,13.7h146.6c7.6,0,13.7-6.1,13.7-13.7v-3.1C428.5,445.9,487,360,487,264.5z
                                M300.3,337.8c24.1-15.5,40.2-42.5,40.2-73.3c0-47.9-39-87-87-87s-87,39-87,87c0,30.7,16,57.8,40.2,73.3c-13.2,8.4-24,20.4-31,34.4
                                c-34.3-24.8-55.1-64.8-55.1-107.7c0-73.3,59.6-132.9,132.9-132.9s132.9,59.6,132.9,132.9c0,42.9-20.8,82.9-55.1,107.7
                                C324.2,358.1,313.5,346.2,300.3,337.8z M193.9,264.5c0-32.9,26.7-59.6,59.6-59.6s59.6,26.7,59.6,59.6s-26.7,59.6-59.6,59.6
                                S193.9,297.4,193.9,264.5z M313.1,470.6H193.9V411c0-32.9,26.7-59.6,59.6-59.6s59.6,26.7,59.6,59.6V470.6z M340.5,451.5V411
                                c0-3.9-0.3-7.7-0.8-11.5c45.9-29.3,74-80.2,74-135.1c0-88.3-71.9-160.2-160.2-160.2s-160.2,72-160.2,160.3
                                c0,54.9,28.1,105.7,74,135.1c-0.5,3.8-0.8,7.6-0.8,11.5v40.4c-72-33.5-119.2-106.3-119.2-187c0-113.7,92.5-206.1,206.1-206.1
                                s206.1,92.5,206.1,206.1C459.6,345.1,412.5,417.9,340.5,451.5z"></path>
                                <path class="podcast-st1" d="M323.2,492.5H189.5v-63.6c0-35.1,30-63.6,66.8-63.6s66.8,28.5,66.8,63.6v63.6H323.2z M153.7,472.1
                                C72.9,436.3,20,358.5,20,272.5c0-121.3,103.7-220,231.2-220s231.2,98.7,231.2,220c0,86-52.9,163.8-133.7,199.6"></path>
                            </svg>
                        </div>
                        <div class="submenu-text">Podcasts</div>
                    </a>
                </div>
            </li>
            <li class="primary_li">
                <div class="submenu_li_inner">
                    <a title="Blog" href="<?php echo get_home_url() ?>/blog">
                      <div class="icon">
                         <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511 511" style="enable-background:new 0 0 511 511;" xml:space="preserve">
                          <path class="blog-st0" d="M435.2,28c-5.4-5.4-14.2-5.4-19.6,0L308.8,134.8H66.6c-22.9,0-41.6,18.7-41.6,41.6v194
                          c0,22.9,18.7,41.6,41.6,41.6h14.8v71.1c0,5.6,3.4,10.7,8.6,12.8c5.3,2.2,11.3,0.8,15.1-3.1l79.1-80.8h161.4
                          c22.9,0,41.6-18.6,41.6-41.6V213.2L494,106.4c5.4-5.4,5.4-14.2,0-19.6L435.2,28z"></path>
                          <path class="blog-st1" d="M436.1,28c-5.4-5.4-14.2-5.4-19.6,0L309.7,134.8H67.5c-22.9,0-41.6,18.7-41.6,41.6v194
                          c0,22.9,18.7,41.6,41.6,41.6h14.8v71.1c0,5.6,3.4,10.7,8.6,12.8c5.3,2.2,11.3,0.8,15.1-3.1l79.1-80.8h161.4
                          c22.9,0,41.6-18.6,41.6-41.6V213.2l106.8-106.8c5.4-5.4,5.4-14.2,0-19.6L436.1,28z M308.7,253.4l-39.2-39.2L387.1,96.6l39.2,39.2
                          L308.7,253.4z M254.8,238.6l29.5,29.5l-50.6,21.1L254.8,238.6z M360.4,370.4c0,7.6-6.2,13.9-13.9,13.9H179.3c-3.7,0-7.3,1.5-9.9,4.2
                          L110,449.2v-51c0-7.7-6.2-13.9-13.9-13.9H67.5c-7.6,0-13.9-6.2-13.9-13.9v-194c0-7.6,6.2-13.9,13.9-13.9H282l-41.9,41.9
                          c-1.2,1.2-2.3,2.7-3.1,4.6l-38.4,92.1h-74.8c-7.7,0-13.9,6.2-13.9,13.9c0,7.7,6.2,13.8,13.9,13.8h84.1l0,0c1.8,0,3.7-0.4,5.4-1.1
                          l100.5-41.9c1.7-0.7,3.4-1.8,4.6-3.1l41.9-41.9v129.6H360.4z M445.9,116.2L406.7,77l19.6-19.6l39.2,39.2L445.9,116.2z"></path>
                      </svg>

                  </div>
                  <div class="submenu-text">Blog</div>
              </a>
          </div>
      </li>
      <li class="green_li">
        <div class="submenu_li_inner">
            <a title="Events" href="<?php echo get_home_url() ?>/events">
              <div class="icon">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
                <path class="event-st0" d="M0,0h24v24H0V0z"/>
                <path class="event-st1" d="M19,3h-1V1h-2v2H8V1H6v2H5C3.9,3,3,3.9,3,5l0,14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z
                M19,19H5V8h14V19z M7,10h5v5H7V10z"/>
                <path class="event-st2" d="M19,3h-1V1h-2v2H8V1H6v2H5C3.9,3,3,3.9,3,5l0,14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z"/>
            </svg>
        </div>
        <div class="submenu-text">Events</div>
    </a>
</div>
</li>
<li class="secondary_li">
    <div class="submenu_li_inner">
        <a title="ebooks" class="no-case" href="<?php echo get_home_url() ?>/ebooks/">
          <div class="icon">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
            <path class="eBooks-st0" d="M414.8,32.1C407,24.3,396.7,20,385.7,20l-270.9,0.4c-2,0-4,0-6,0c-13.7-0.1-27.8-0.2-39.6,4.8
            C55,31.3,47.2,44,46,62.9c0,0.1,0,0.3,0,0.4v356.1c0,0.1,0,0.2,0,0.3c0.7,16.7,13.6,34.7,39.9,34.7h128.6l9.2,11.9l0.1,0.1
            c8.9,10.8,21,16.9,33.5,16.9c0.4,0,0.8,0,1.3,0c12.1-0.4,23.4-6.6,30.9-17.1l9.2-11.8H386c22.5,0,40.8-18.5,40.8-41.2v-59.5
            l0.1-292.4C426.9,50.2,422.6,39.9,414.8,32.1z"/>
            <path class="eBooks-st1" d="M177.2,190.3H244c3.8,0,6.8-3.1,6.8-6.8c0-3.8-3.1-6.8-6.8-6.8h-66.8c-1.9,0-3.5-1.6-3.5-3.5V114
            c0-1.9,1.6-3.5,3.5-3.5h159.7c1.9,0,3.5,1.6,3.5,3.5v59.1c0,1.9-1.6,3.5-3.5,3.5h-61.5c-3.8,0-6.8,3.1-6.8,6.8
            c0,3.8,3.1,6.8,6.8,6.8h61.5c9.5,0,17.2-7.7,17.2-17.2v-59c0-9.5-7.7-17.2-17.2-17.2H177.2c-9.5,0-17.2,7.7-17.2,17.2v59.1
            C160,182.6,167.7,190.3,177.2,190.3z"/>
            <path class="eBooks-st2" d="M414.8,42.1C407,34.3,396.7,30,385.7,30l-270.9,0.4c-2,0-4,0-6,0c-13.7-0.1-27.8-0.2-39.6,4.8
            C55,41.3,47.2,54,46,72.9c0,0.1,0,0.3,0,0.4v356.1c0,0.1,0,0.2,0,0.3c0.7,16.7,13.6,34.7,39.9,34.7h128.6l9.2,11.9l0.1,0.1
            c8.9,10.8,21,16.9,33.5,16.9c0.4,0,0.8,0,1.3,0c12.1-0.4,23.4-6.6,30.9-17.1l9.2-11.8H386c22.5,0,40.8-18.5,40.8-41.2v-59.5
            l0.1-292.4C426.9,60.2,422.6,49.9,414.8,42.1z M108,44v178.3c0,3.8,3.1,6.8,6.8,6.8s6.8-3.1,6.8-6.8V44l264-0.3
            c7.3,0,14.2,2.9,19.5,8.1c5.2,5.2,8.1,12.1,8.1,19.4l-0.1,292.4c0,15.2-12.4,27.5-27.5,27.5h-37.5c0.7-6,0-11.8-2.4-16.9
            c-2.9-6.4-11.1-17.2-33.3-18.3c-0.1,0-0.2,0-0.3,0h-7.5v-66.1c0-9.2-7.8-16.7-17.5-16.7h-60.8c-9.6,0-17.5,7.5-17.5,16.7v66.1h-7.5
            c-0.1,0-0.2,0-0.3,0c-22.2,1.1-30.4,11.9-33.3,18.3c-2.3,5.1-3.1,10.9-2.4,16.9h-43.7V253.6c0-3.8-3.1-6.8-6.8-6.8
            c-3.8,0-6.8,3.1-6.8,6.8v137.5H86c-11.1,0-19.8,3.3-26.2,8.3V73.5C61.6,45.3,79.3,43.8,108,44z M203.9,450.6h-118
            c-18.6,0-25.7-11.5-26.2-21.5v-2.4c0.6-11,8.9-22,26.2-22h83.6c1.5,3,3.4,6,5.6,8.9L203.9,450.6L203.9,450.6z M278.8,467.8
            c-0.1,0.1-0.1,0.1-0.2,0.2c-5.1,7.2-12.4,11.3-20.4,11.5c-8.6,0.3-17.3-4-23.7-11.8l-48.6-62.5c-2.3-3-4.1-6.1-5.3-9.3
            c-0.1-0.3-0.2-0.6-0.3-0.9c-2-5.5-2.1-11-0.2-15.2c3.6-7.8,13.6-9.9,21.4-10.3h14.2c3.8,0,6.8-3.1,6.8-6.8v-72.9c0-1.6,1.8-3,3.8-3
            h60.8c2,0,3.8,1.4,3.8,3v72.9c0,3.8,3.1,6.8,6.8,6.8h14.1c7.8,0.4,17.8,2.5,21.4,10.4c1.9,4.2,1.8,9.7-0.2,15.2
            c-0.1,0.3-0.2,0.5-0.3,0.8c-1.2,3.1-3,6.3-5.3,9.3L278.8,467.8z M386,450.6h-76.6l0,0l28.8-37c2.2-2.9,4.1-5.8,5.6-8.9h41.7
            c10.6,0,20.2-4,27.5-10.6v29C413.1,438.3,401,450.6,386,450.6z"/>
        </svg>
    </div>
    <div class="submenu-text">eBooks</div>
</a>
</div>
</li>
</ul>

</div>
</div>
</div>
<div class="submenu_bottom_section">
  <div class="container">
    <p><strong>eBook:</strong>How to create alignment through an employee-led culture <a class="link_with_arrow" href="https://www.visibly.io/2020-ebook" title="Download now">Download now 
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
      viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
      <path class="round_arrow_st0" d="M106.9,199.1l51.8-51.9l-51.8-51.9l16-16l67.9,67.9L122.9,215L106.9,199.1z"/>
      <circle class="round_arrow_st1" cx="143.4" cy="146.3" r="117.7"/>
  </svg></a></p>
</div>
</div>
</div>
</div>
</li>
</ul>
<div class="menu_overlay"></div>

</div>


<!-- MainNav ends -->
</div>
</div>
</div>
</header>
</div>
<!--Header ends-->

<div class="site-content-contain">
    <div id="content" class="site-content">

