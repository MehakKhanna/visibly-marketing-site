<?php
/**
 * Template Name:New Homepage
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header();

    while ( have_posts() ) : the_post();
    if(current_user_can('administrator')) { ?> 
    <div class="create">create</div>
    <h1 contentEditable="true"><?php the_field('hero_title') ?></h1><div class="edit">edit</div>
    <div class="save">save</div>
    <?php }else{ ?>
        <h1 ><?php the_field('hero_title') ?></h1>
    <?}
     ?>
    
      <?php the_content();
   endwhile; 

    // include('organisationpage.php')
    
    ?>

<?php get_footer(); ?>

