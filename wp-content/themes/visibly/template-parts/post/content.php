<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post-meta-data cf">
                                <div class="authorData">
                                    

                                    <?php 
                                    //echo $_SERVER['DOCUMENT_ROOT'];
                                    //echo get_template_directory_uri();
                                    $path = $_SERVER['DOCUMENT_ROOT'].parse_url( get_stylesheet_directory_uri(), PHP_URL_PATH );
                                    include($path."/blocks/author-image.php") ?>

			<div class="postAuthorName"><?php echo get_the_author_meta( 'display_name', $post->post_author ); ?></div>
                                </div>
                                <?php 
                                $pdf_link = get_post_meta($post->ID, 'wpcf-press-upload-pdf-file', true); 
                                 
                                if($pdf_link!='') {?>
                                <div class="pressPDFLinkWapper"><a href="<?php echo $pdf_link; ?>" title="Download PDF" download><i class="far fa-file-pdf"></i> Download PDF</a></div>
                            <?php } ?>
                                <div class="postDate"><div class="post-date"><i class="far fa-clock"></i> <?php echo get_the_date() ?></div></div>
	</div>
							
	

	<div class="entry-content">
		<?php
		/* translators: %s: Name of current post */
		the_content( sprintf(
			__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
			get_the_title()
		) );?>
		<div class="social-icons-wrapper cf">
		<?php echo sharethis_inline_buttons(); ?>
	</div>
		<?php wp_link_pages( array(
			'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
			'after'       => '</div>',
			'link_before' => '<span class="page-number">',
			'link_after'  => '</span>',
		) );
		?>
	</div><!-- .entry-content -->

	

</article><!-- #post-## -->
