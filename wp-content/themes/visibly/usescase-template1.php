<?php
/*
 * Template Name: Usecase detail
 */
get_header();


 ?>

<div class="mainWrapper usescases-detail-page">
		<main id="main" class="site-main" role="main">
        <h1 class="usescases-detail-title"><?php the_title() ?></h1>
			<?php
			while ( have_posts() ) : the_post();
                
				the_content(); ?>
				

<!-- Features overview section starts -->
		<section class="features_overview">
			<div class="inner_wrap">
				<div class="container">

					<div class="section_title text-center">
						<h3 class="title"><span class="square"><span class="square_two"></span>Industry overview</span></h3>
					</div>
					
						<?php include($rootPath.'/inc/why-visibly-features.php');?> 

				</div>
			</div>
		</section>
		<!--  Features overview section ends -->
	
		<?php endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	
</div><!-- .wrap -->

<?php get_footer();
