<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
// define('DB_NAME', 'dbvisibly');
// define('DB_USER', 'root');
// define('DB_PASSWORD', 'root');
// define('DB_HOST', 'localhost');


//this is local database which use in office
define('DB_NAME', 'visibly_local');
define('DB_USER', 'visibly_user');
define('DB_PASSWORD', 'Visibly@123');
define('DB_HOST', '192.168.1.71:3307');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'F;?xze*p(;Yba9aNtQp`Rl&AF2|:oC4ywHsT;WI0]9~x]wkSB,8#rl+QcNge`u_%');
define('SECURE_AUTH_KEY',  '^m5W7vWjij;Nwt^Cyib6{O@E@qcVP1H3* CARw8D+>.;mN{{3x`X7D>I,m>vIluD');
define('LOGGED_IN_KEY',    '-`n-?;ixBD=t6t;kgdfbdL}Vrt3a.WJL*]nD#uH8}i=-LuG@JGsSvccrD=8L0GkF');
define('NONCE_KEY',        'EqPdD|F1.?IUVac-)QH<ffl.7#Wn^)#`zGFFaRb/M~NaN:BnTauB?S<xZ^sxJ*s_');
define('AUTH_SALT',        'H^k{A?rm~xW-esQsjn,b!V!d0vJ8I$bF!cFl>(9xT0^ew5q)#to>O:v]v,}p2&s5');
define('SECURE_AUTH_SALT', 'fk!Yr&:3_Enxa~G@f%Yd.C{i]u CbX*_Ah:qVH_=1Q!6oc0rt*9yiIqMwuK(XA/s');
define('LOGGED_IN_SALT',   'N=R-`gjH&)vqVWU+?8HOH@;`j-!X=RPdmF^_Om8~At458GLu-X1U@{W8 `on p5&');
define('NONCE_SALT',       'M`MONArm>gC~SH%k4so{L&lj$8OROm6k1Bdja:XKRX&bx/,LDg<3Cs|ir[YL0X32');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
