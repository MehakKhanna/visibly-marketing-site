$(document).ready(function(){
  $('#surveyStepTotal').html($('.survey-slide').length);
  function check_tr(value){
    return value;
  }
    $('.survey-slider').slick({
        dots:true,
        swipe:false,
        swipeToSlide:false,
        infinite:false,
        fnCanGoNext: function(instance, currentSlide){
          // console.log(currentSlide) previous project
          var currentSlide = instance.$slides.eq(currentSlide);
          var tr = currentSlide.find('tbody').find("tr");
          var option1 = currentSlide.find('tbody').find('tr').find('td:nth-child(2)').find('input[type=radio]:checked').size();
          var option2 = currentSlide.find('tbody').find('tr').find('td:nth-child(3)').find('input[type=radio]:checked').size();
          var option3 = currentSlide.find('tbody').find('tr').find('td:nth-child(4)').find('input[type=radio]:checked').size();
          var option4 = currentSlide.find('tbody').find('tr').find('td:nth-child(5)').find('input[type=radio]:checked').size();
          var option5 = currentSlide.find('tbody').find('tr').find('td:nth-child(6)').find('input[type=radio]:checked').size();

          
          
          var radioValueArray = [];
          $(tr).each(function(){
            if($(this).find('input[type=radio]:checked').size()==0){
              radioValueArray.push(false)
            }else{
              radioValueArray.push(true)
            }
          });
          if($('.main-wrapper').hasClass('manager-survey')){
            if(radioValueArray.every(check_tr) && option1==1 && option2==1 && option3==1 && option4==1 && option5==1){
              return true;
            }
            else{
              $('.error-message').addClass('show');
              setTimeout(function(){
                $('.error-message').removeClass('show');
              },5000)
            }
          }
          else{
            if(radioValueArray.every(check_tr)){
              return true;
            }
            else{
              $('.error-message').addClass('show');
              setTimeout(function(){
                $('.error-message').removeClass('show');
              },5000)
            }
          }
      },
      
        customPaging: function(slick,index) {
          
  return '<span class="slick-number-wrapper"><span class="slick-number">' + (index + 1) + '</span><span class="slick-icon"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z"/></svg></span></span>';
}
    });


    $('.survey-slider').on('beforeChange', function(event, slick, currentSlide, next){
      
      $('.survey-slider .slick-dots > li').removeClass('completed-tab')
      for(var j = 0; j<next; j++){
        $('.survey-slider .slick-dots > li').eq(j).addClass('completed-tab');
    }
      $('#surveyStepCount').html(next+1);
   //   document.querySelectorAll('#tourStepCount')[0].innerHTML=next+1;
     // document.querySelectorAll('#tourStepText')[0].innerHTML=TabsNames[next];
        lastSlide = $('.survey-slide').length-1;
      if(next==lastSlide){
        $('#finish-tour-image').addClass('show-image');
        $('.slider-next-button').addClass('show');
        $('.step-content .content-title-header').addClass('hide');
      }else{
        $('#finish-tour-image').removeClass('show-image');
        $('.slider-next-button').removeClass('show');
        $('.step-content .content-title-header').removeClass('hide');
      }
    });

    $('.finish-survey-btn').click(function(){
      var tr = $('.slick-active').find('tbody').find("tr");
      var radioValueArray = [];
      $(tr).each(function(){
        if($(this).find('input[type=radio]:checked').size()==0){
          radioValueArray.push(false)
        }else{
          radioValueArray.push(true)
        }
      });
      if(radioValueArray.every(check_tr)){
        $('body').addClass('thank-you-page');
      }
      else{
        $('.error-message').addClass('show');
        setTimeout(function(){
          $('.error-message').removeClass('show');
        },5000)
      }
    })

  });